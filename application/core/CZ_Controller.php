<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class CZ_Controller extends CI_Controller
{
	function __construct()
	{
	    parent::__construct();

	}
	public function enkrip($key)
	{
		return substr(base64_encode(base64_encode(base64_encode($key))),0,-2);
	}
	public function dekrip($key)
	{
		return base64_decode(base64_decode(base64_decode($key.'==')));
	}
	public function convToObj($data=array())
	{
		$result = json_decode(json_encode($data), FALSE);
		return $result;
	}
	public function convDateTime($dt,$st=1){
		
		if ($st ==1 ) { $out = date('d F Y H:i:s', strtotime($dt)); }
		elseif($st==2){ $out = date('d M Y H:i:s', strtotime($dt)); }
		elseif($st==3){ $out = date('d-M-Y H:i:s', strtotime($dt)); }
		elseif($st==4){ $out = date('d m Y H:i:s', strtotime($dt)); }
		elseif($st==5){ $out = date('d-m-Y H:i:s', strtotime($dt)); }
		elseif($st==6){ $out = date('d/M/Y H:i:s', strtotime($dt)); }
		elseif($st==7){ $out = date('d/m/Y H:i:s', strtotime($dt)); }
		return $out;
	}
	public function convDate($dt,$st=1){
		if ($st ==1 ) { $out = date('d F Y', strtotime($dt)); }
		elseif($st==2){ $out = date('d M Y', strtotime($dt)); }
		elseif($st==3){ $out = date('d-M-Y', strtotime($dt)); }
		elseif($st==4){ $out = date('d m Y', strtotime($dt)); }
		elseif($st==5){ $out = date('d-m-Y', strtotime($dt)); }
		elseif($st==6){ $out = date('d/M/Y', strtotime($dt)); }
		elseif($st==7){ $out = date('d/m/Y', strtotime($dt)); }
		return $out;
	}
	public function convTime($dt='')
	{
		$out = date('H:i:s', strtotime($dt));
		return $out;
	}
	public function arrHari($h=''){
		$hasil = '';
		$arr_hari = array("","Minggu","Senin","Selasa","Rabu","Kamis","Jum`at","Sabtu");
		$hasil = $arr_hari;
		if($h!='') $hasil = $arr_hari[$h];
		return $hasil;
	}
    public function req($param=''){
        if ($this->input->server('REQUEST_METHOD') == 'GET'){
            if(!empty($param)){
                return $this->input->get($param);//its a get
            }else{
                return $this->input->get();//its a get
            }
        }
        else if ($this->input->server('REQUEST_METHOD') == 'POST'){
            if(!empty($param)){
                return $this->input->post($param);//its a pos
            }else{
                return $this->input->post();//its a pos
            }
        }
    }

	public function arrBulan($b=''){
		$hasil = array();
		$arr_bulan_nama = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");
		$arr_bulan_angka = array("01","02","03","04","05","06","07","08","09","10","11","12");
		$hasil = $arr_bulan_nama;
		if($b != '') $hasil = $arr_bulan_nama[$b];
		return $hasil;
	}
	public function formatAngka($var){
		$out = number_format($var, 0, ',', '.');
		return $out;
	}
	public function detDiff($tgl1='',$tgl2='',$format='days')
	{
		$hasil = date_diff(date_create($tgl1),date_create($tgl2));
		return $hasil->$format;
	}
	public function countDays($year, $month, $ignore) {
	    $count = 0;
	    $counter = mktime(0, 0, 0, $month, 1, $year);
	    while (date("n", $counter) == $month) {
	        if (in_array(date("w", $counter), $ignore) == false) {
	            $count++;
	        }
	        $counter = strtotime("+1 day", $counter);
	    }
	    return $count;
	}
}
