<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_rajal_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
    }

    public function get_pasien_irj($reg="")
	{
		$this->sv->select("r.Regno, mp.Medrec, mp.Firstname, mp.Address, mp.Phone, pt.KDPoli, pt.NMPoli, r.Kategori, r.KdCbayar, tc.GroupUnit, mp.UmurThn, r.Regdate, r.Regdate");
		$this->sv->from("Register r");
		$this->sv->join("MasterPS mp","r.Medrec = mp.Medrec", "LEFT");
		$this->sv->join("POLItpp pt","pt.KDPoli = r.KdPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = r.KdCbayar", "LEFT");
		$this->sv->where("r.Regno", $reg);
		$ps = $this->sv->get();
		return $ps->row();
	}

    public function get_irj($reg="")
    {
        // SET ATRIBUT
        $fill = $head = $detail = $hasil['irj'] = [];
        $n0 = $n1 = 0;
        // GET HEAD TRANSAKSI
        $this->sv->select("hbj.Notran, hbj.Tanggal, hbj.Regno,  hbj.Regdate, hbj.Medrec, hbj.Firstname, hbj.KdPoli,
                            fd.NmDoc, pt.NMPoli, tc.NMCbayar, tj.NMJaminan, hbj.ValidUser");
        $this->sv->from("HeadBilIrj hbj");
        $this->sv->join("POLItpp pt", "hbj.KdPoli=pt.KDPoli", "LEFT");
        $this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbj.KdCbayar", "LEFT");
        $this->sv->join("TBLJaminan tj", "hbj.KdJaminan=tj.KDJaminan", "LEFT");
        $this->sv->join("FtDokter fd", 'fd.KdDoc = hbj.KdDoc', "LEFT");
        $this->sv->where('hbj.Regno', $reg);
        $head = $this->sv->get()->result();
        foreach ($head as $h) {
            $fill[] = $h->Notran;
        }
        // GET DATA DETAIL
        $this->sv->select("dbj.Notran, dbj.Regno, dbj.Tanggal, fpj.KdGroup, fpj.NMDetail, ftj.KDTarif, dbj.NmTarif, dbj.Qty,
                        dbj.JumlahBiaya, dbj.KdDoc, fd.NmDoc");
        $this->sv->from("DetailBilIrj dbj");
        $this->sv->join("fTarifRawatJalan ftj", "ftj.KDTarif = dbj.KdTarif", "LEFT");
        $this->sv->join("fPemeriksaanIRJ fpj", "ftj.KDDetail = fpj.KDDetail", "LEFT");
        $this->sv->join("FtDokter fd", 'fd.KdDoc = dbj.KdDoc', "LEFT");
        $this->sv->where('dbj.Regno', $reg);
        if(!empty($fill)) $this->sv->where_in('dbj.Notran',$fill);
        $detail = $this->sv->get()->result();
        // FILL DATA IN ONE CONTAINER
        foreach ($detail as $d) {
            $hasil['irj'][$n1]['Tanggal'] = $d->Tanggal;
            $hasil['irj'][$n1]['NmTarif'] = $d->NmTarif;
            $hasil['irj'][$n1]['NmDoc'] = $d->NmDoc;
            foreach ($head as $h) {
                if($h->Notran == $d->Notran) $hasil['irj'][$n1]['KdPoli'] = $h->KdPoli;
            }
            $hasil['irj'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
            $n1++;
        }
        return $this->cl->convToObj($hasil);
    }

    public function get_ugd($reg="")
	{
		// SET ATRIBUT
		$head = $detail = $fill = $hasil['ugd'] = [];
		$n0=$n1=0;
		// GET HEAD TRANSAKSI
		$this->sv->select("hbu.Notran, hbu.Tanggal, hbu.Regno, hbu.Regdate, hbu.Medrec,
						hbu.Firstname, tc.NMCbayar, tj.NMJaminan, hbu.ValidUser");
		$this->sv->from("HeadBilUgd hbu");
		$this->sv->join("POLItpp pt", "hbu.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbu.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbu.KdJaminan=tj.KDJaminan", "LEFT");
		$this->sv->where("hbu.Regno",$reg);
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI 
		$this->sv->select("dbu.Regno, dbu.Tanggal, fpu.KdGroup, fpu.NmDetail, 
							ftu.KDTarif, dbu.NmTarif, dbu.Qty, dbu.JumlahBiaya, dbu.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilUgd dbu");
		$this->sv->join("fTarifRawatDarurat ftu", "ftu.KDTarif = dbu.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIGD fpu", "ftu.KDDetail = fpu.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbu.KdDoc', "LEFT");
		$this->sv->where('dbu.Regno', $reg);
		if(!empty($fill)) $this->sv->where_in('dbu.Notran', $fill);
		$detail = $this->sv->get()->result();
			// FILL DATA IN ONE CONTAINER
		// echo '<pre>'; print_r($detail); exit();
		foreach ($detail as $d) {
			if($d->KDTarif == '010411' || $d->KDTarif == '010412'){
				$hasil['ugd'][$n1]['Tanggal'] = $d->Tanggal;
				$hasil['ugd'][$n1]['NmTarif'] = $d->NmTarif;
				$hasil['ugd'][$n1]['KdDoc'] = $d->KdDoc;
				$hasil['ugd'][$n1]['NmDoc'] = $d->NmDoc;
				$hasil['ugd'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
				$n1++;
			} else {
				$hasil['ugd'][$n1]['Tanggal'] = $d->Tanggal;
				$hasil['ugd'][$n1]['NmTarif'] = $d->NmTarif;
				// $hasil['ugd'][$n1]['NmDoc'] = $d->NmDoc;
				$hasil['ugd'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
				$n1++;
			}
		}

		return $this->cl->convToObj($hasil);

		// return $ugd->result();
	}
	public function get_apotek($reg="")
	{
		// set atgribut
		$raw = $fill = $hasil['apt'] = $hasil['di'] = [];
		$hasil['du'] = $hasil['do'] = $hasil['dh'] = [];
		$n0=$n1=$n2=$n3=$n4=$n5=0;
		// GET OBAT AOPTEK
		$this->sv->select("ha.Regno, ha.KdBangsal, ha.KdKelas, ha.KdCbayar, ha.Regdate, ha.BLCode, da.BLDate, da.NamaObat, da.JumlahHarga");
		$this->sv->from("HeadApotik ha");
		$this->sv->join("DetailApotik da","ha.BLCode = da.BLCode","LEFT");
		$this->sv->where("ha.Regno", $reg);
		$apt = $this->sv->get()->result();
		// FILL DATA TO CONTAINER
		foreach ($apt as $d) {
			$hasil['apt'][$n1]['Tanggal'] = $d->BLDate;
			$hasil['apt'][$n1]['KdBangsal'] = $d->KdBangsal;
			$hasil['apt'][$n1]['NamaObat'] = $d->NamaObat;
			$hasil['apt'][$n1]['JumlahHarga'] = $d->JumlahHarga;
			$n1++;
		}	
		return $this->cl->convToObj($hasil);
	}
	public function get_umuk($reg='')
	{
		$this->sv->where('Regno', $reg);
		$umuk = $this->sv->get('UangMuka');
		return $umuk->row();
	}

}


