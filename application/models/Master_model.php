<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model {
	
	protected $sv;
	function __construct(){
		parent::__construct();
		$this->sv = $this->load->database('server',true);
	}
	public function get_sex($id='')
	{
		if ($id!='') {
			$this->sv->where('KdSex',$id);
		}
		$jk = $this->sv->get('TBLSex');
		return $jk->result();
	}
	public function get_stat_kawin($id='')
	{
		if ($id!='') {
			$this->sv->where('KdKawin',$id);
		}
		$stat = $this->sv->get('TBLPerkawinan');
		return $stat->result();
	}
	public function get_kelurahan($id='')
	{
		if ($id!='') {
			$this->sv->where('KdKelurahan', $id);
		}
		$stat = $this->sv->get('TBLKelurahan');
		return $stat->result();
	}
	public function get_kecamatan($id='')
	{
		if ($id!='') {
			$this->sv->where('KdKecamatan', $id);
		}
		$stat = $this->sv->get('TBLKecamatan');
		return $stat->result();
	}
	public function get_kabupaten($id='')
	{
		if ($id!='') {
			$this->sv->where('KdKabupaten', $id);
		}
		$stat = $this->sv->get('TBLKabupaten');
		return $stat->result();
	}
	public function get_provinsi($id='')
	{
		if ($id!='') {
			$this->sv->where('KdPropinsi', $id);
		}
		$stat = $this->sv->get('TBLPropinsi');
		return $stat->result();
	}
	public function get_ktg_pasien($id='')
	{
		if ($id!='') {
			$this->sv->where('KdKategori', $id);
		}
		$stat = $this->sv->get('TBLKategoriPsn');
		return $stat->result();
	}
	public function get_suku($id='')
	{
		if ($id!='') {
			$this->sv->where('KdSuku', $id);
		}
		$stat = $this->sv->get('TBLSuku');
		return $stat->result();
	}
	public function get_agama($id='')
	{
		if ($id!='') {
			$this->sv->where('KdAgama', $id);
		}
		$stat = $this->sv->get('TBLAgama');
		return $stat->result();
	}
	public function get_pendidikan($id='')
	{
		if ($id!='') {
			$this->sv->where('KdDidik', $id);
		}
		$stat = $this->sv->get('TBLPendidikan');
		return $stat->result();
	}
	public function get_poli($id='')
	{
		if ($id!='') {
			$this->sv->where('KDPoli', $id);
		}
		$stat = $this->sv->order_by('NMPoli','ASC')->get('POLItpp');
		return $stat->result();
	}
	public function get_unit_ktg($id='')
	{
		if ($id!='') {
			$this->sv->where('KdUnit', $id);
		}
		$stat = $this->sv->get('TBLUnitKategori');
		return $stat->result();	
	}
	public function get_group_unit($id='')
	{
		if ($id!='') {
			$this->sv->where('Kategori', $id);
		}
		$stat = $this->sv->get('TBLGroupUnit');
		return $stat->result();	
	}
	public function get_dokter($id='')
	{
		if ($id!='') {
			$this->sv->where('KdDoc', $id);
		}
		$stat = $this->sv->get('FtDokter');
		return $stat->result();	
	}
	public function get_kategoribayar($id='')
	{
		if ($id!='') {
			$this->sv->where('KDCbayar', $id);
		}
		$stat = $this->sv->get('TBLcarabayar');
		return $stat->result();	
	}
	public function get_dokter_poli($poli='')
	{
		if ($poli!='') {
			// $this->sv->where('KdDoc', $id);
			$this->sv->where('KdPoli', $poli);
		}
		$stat = $this->sv->get('FtDokter');
		return $stat->result();	
	}
}

/* End of file master_model.php */
/* Location: ./application/models/master_model.php */