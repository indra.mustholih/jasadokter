<?php
// mst_groupjasaid
// 1 Klinik Spesialis
// 2 IGD
// 3 Klinik Gigi dan muluts spesialis
// 4 laboratorium
// 5 Radiologi
// 6 Fisioterapi
// 7 Hemodialisa
// 8 Farmasi


defined('BASEPATH') OR exit('No direct script access allowed');

class Spesialis_model extends CI_Model {

  protected $sv;

  // variable utk datatable serverside
	var $tbl = 'trans_spesialis';
	var $column_order = array(null,'a.mst_jasapelayananid','a.jumlah');
  var $column_search = array('a.jumlah',"a.keterangan");
	var $order = array(''=> '');
  

  //private variable untuk  memanggl nama table dan id nya
  private $table = 'trans_spesialis';
  private $id = 'trans_spesialisid';

	public function __construct()
	{
		parent::__construct();
    $this->sv = $this->load->database('server', true);
    $this->load->library('cl');
  }

  public function listing(){
    $this->sv->select("
       b.nama_jasa as nama_jasa, a.jumlah as jumlah, keterangan
    ");
    $this->sv->from('trans_spesialis a');
    $this->sv->join("mst_jasapelayanan b", "a.mst_jasapelayananid=b.mst_jasapelayananid", "JOIN");
    $this->sv->order_by('trans_spesialisid','DESC');
		$query = $this->sv->get();
    return $query->result_array();

  }
  
  public function tambah($data){
    $this->sv->insert($this->tbl,$data);
  }
  
  public function edit($trans_spesialisid){
    $this->sv->select("
      b.nama_jasa as nama_jasa, a.jumlah as jumlah, keterangan, a.mst_jasapelayananid
    ");
    $this->sv->from(' trans_spesialis a ');
    $this->sv->join("mst_jasapelayanan b", "a.mst_jasapelayananid=b.mst_jasapelayananid", "JOIN");
		$this->sv->where('trans_spesialisid',$trans_spesialisid);
		$query = $this->sv->get();
		return $query->row_array();
  }

  public function editproses($trans_spesialisid, $data){
    $this->sv->where('trans_spesialisid',$trans_spesialisid);
		$this->sv->update('trans_spesialis',$data);
  }

  public function jasapelayananGetAll(){
    $this->sv->select("*");
    $this->sv->from('mst_jasapelayanan');
    $this->sv->where('mst_groupjasaid', '1');
    $this->sv->order_by('mst_jasapelayananid','DESC');
    $query = $this->sv->get();
    return $query->result();
  }
  public function delete($data){
      $this->sv->where($data);
      $this->sv->delete($this->tbl);
  }














  //datatables serverside
  private function _get_datatables_query() {
    
    $this->sv->select("b.nama_jasa as nama_jasa, a.jumlah as jumlah, keterangan, a.trans_spesialisid, a.mst_jasapelayananid");
    $this->sv->from('
    
      trans_spesialis a
    
    ');
    $this->sv->join("mst_jasapelayanan b", "a.mst_jasapelayananid=b.mst_jasapelayananid", "JOIN");

    $i = 0;

    foreach ($this->column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->sv->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->sv->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->sv->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->sv->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->sv->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->sv->order_by(key($order), $order[key($order)]);
    }
  }

function get_datatables()
{
  $this->_get_datatables_query();
  if($_POST['length'] != -1)
  $this->sv->limit($_POST['length'], $_POST['start']);
  $query = $this->sv->get();
  return $query->result();
}

// function get_datatables_verify()
// {
//   $this->_get_datatables_query();
//   if($_POST['length'] != -1)
//   $this->db->limit($_POST['length'], $_POST['start']);
//   $this->db->where("akses_level","kecamatan");
//   $query = $this->db->get();
//   return $query->result();
// }

function count_filtered()
{
  $this->_get_datatables_query();
  $query = $this->sv->get();
  return $query->num_rows();
}

public function count_all()
{
  $this->sv->from($this->tbl);
  return $this->sv->count_all_results();
}


}