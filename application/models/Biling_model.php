<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		$this->sv = $this->load->database('server', true);
	}
	public function get_pasien($reg="")
	{
		$this->sv->select("fi.Regno, mp.Medrec, mp.Firstname, mp.Address,
			mp.Phone, pt.KDPoli, pt.NMPoli, fi.KdBangsal, tb.NmBangsal, fi.KdKelas, tk.NMKelas, tk.ByTarif, fi.KdCbayar, tc.GroupUnit,
			mp.UmurThn, fi.Regdate, fp.Tanggal");
		$this->sv->from("FPPRI fi");
		$this->sv->join("MasterPS mp","fi.Medrec = mp.Medrec", "LEFT");
		$this->sv->join("TBLBangsal tb","tb.KdBangsal = fi.KdBangsal", "LEFT");
		$this->sv->join("POLItpp pt","pt.KDPoli = fi.KdPoli", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = fi.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = fi.KdCbayar", "LEFT");
		$this->sv->join("FPulang fp","fp.Regno = fi.Regno", "LEFT");
		$this->sv->where("fi.Regno", $reg);
		$ps = $this->sv->get();
		return $ps->first();
	}
	public function get_apotek($reg="")
	{
		$this->sv->select("fi.Regno, fi.KdKelas, fi.KdCbayar, fi.Regdate, 
			ha.BLCode, da.BLDate, da.NamaObat, da.JumlahHarga");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadApotik ha","fi.Regno = ha.Regno","LEFT");
		$this->sv->join("DetailApotik da","ha.BLCode = da.BLCode","LEFT");
		$this->sv->where("fi.Regno", $reg);
		$apt = $this->sv->get();
		return $apt->result();
	}
	public function get_aku($reg="")
	{
		$this->sv->select("fi.Regno, fi.Medrec, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
			hba.NoTran, dba.Tanggal, hba.KdCbayar, hba.TotalBiaya, dba.KdTarif, dba.NmTarif, dba.Qty,
			dba.JumlahBiaya, dba.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilAku hba", "fi.Regno = hba.Regno", "LEFT");
		$this->sv->join("DetailBilAku dba", "hba.NoTran = dba.NoTran", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dba.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$ak = $this->sv->get();
		return $ak->result();
	}
	public function get_ibs($reg="")
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
			bo.NoTran, bo.Tanggal, fi.KdCbayar, fpb.KdGroup, fgb.NmGroup, fpb.NMDetail, ftb.KDTarif, bo.JumlahBiaya");
		$this->sv->from("FPPRI fi");
		$this->sv->join("BilOperasi bo", "fi.Regno = bo.Regno", "LEFT");
		$this->sv->join("fTarifBedahSentral ftb", "bo.KodeTarif = ftb.KDTarif", "LEFT");
		$this->sv->join("fPemeriksaanIBS fpb", "ftb.KDDetail = fpb.KDDetail", "LEFT");
		$this->sv->join("fGroupIBS fgb", "fpb.KdGroup = fgb.KdGroup", "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$ibs = $this->sv->get();
		return $ibs->result();
	}
	public function get_fis($reg="")
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
							hbf.NoTran, dbf.Tanggal, hbf.KdCbayar, hbf.TotalBiaya,
							fpe.KdGroup, fge.NmGroup ,fpe.NMDetail, fte.KDTarif, dbf.NmTarif, dbf.Qty,
							dbf.JumlahBiaya, dbf.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilFis hbf", "fi.Regno = hbf.Regno", "LEFT");
		$this->sv->join("DetailBilFis dbf", "hbf.NoTran = dbf.NoTran", "LEFT");
		$this->sv->join("fTarifElektromedik fte", "fte.KDTarif = dbf.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanEle fpe", "fte.KDDetail = fpe.KDDetail", "LEFT");
		$this->sv->join("fGroupEle fge", "fpe.KdGroup = fge.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbf.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$fi = $this->sv->get();
		return $fi->result();
	}
	public function get_hd($reg="")
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
				hbd.NoTran, dbd.Tanggal, hbd.KdCbayar, hbd.TotalBiaya,
				fph.KdGroup, fgh.NmGroup ,fph.NMDetail, fth.KDTarif, dbd.NmTarif,
				dbd.Qty, dbd.JumlahBiaya, dbd.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilHD hbd", "fi.Regno = hbd.Regno", "LEFT");
		$this->sv->join("DetailBilHD dbd", "hbd.NoTran = dbd.NoTran", "LEFT");
		$this->sv->join("fTarifHemodialisa fth", "fth.KDTarif = dbd.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanHemo fph", "fth.KDDetail = fph.KDDetail", "LEFT");
		$this->sv->join("fGroupHemo fgh", "fph.KdGroup = fgh.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbd.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$hd = $this->sv->get();
		return $hd->result();
	}
	public function get_irj($reg="")
	{
		$this->sv->select("fi.Regno, fi.Firstname, hbj.KdPoli, pt.NMPoli, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
						dbj.Tanggal, fpj.KdGroup, fgj.NmGroup, fpj.NMDetail, ftj.KDTarif, dbj.NmTarif, dbj.Qty,
						dbj.JumlahBiaya, dbj.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilIrj hbj", "fi.Regno = hbj.Regno", "LEFT");
		$this->sv->join("DetailBilIrj dbj", "hbj.Notran = dbj.Notran", "LEFT");
		$this->sv->join("POLItpp pt", "pt.KdPoli = hbj.KdPoli", "LEFT");
		$this->sv->join("fTarifRawatJalan ftj", "ftj.KDTarif = dbj.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIRJ fpj", "ftj.KDDetail = fpj.KDDetail", "LEFT");
		$this->sv->join("fGroupIRJ fgj", "fpj.KdGroup = fgj.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbj.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$rj = $this->sv->get();
		return $rj->result();
	}
	public function get_irna($reg="")
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
							dbi.Tanggal, fpi.KdGroup, fgi.NmGroup, fpi.NmDetail, fti.KDTarif, dbi.NmTarif, dbi.Qty,
							dbi.JumlahBiaya, dbi.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilIrna hbi", "fi.Regno = hbi.Regno", "LEFT");
		$this->sv->join("DetailBilIrna dbi", "hbi.NoTran = dbi.NoTran", "LEFT");
		$this->sv->join("fTarifRawatInap fti", "fti.KDTarif = dbi.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIRNA fpi", "fti.KDDetail = fpi.KDDetail", "LEFT");
		$this->sv->join("fGroupIRNA fgi", "fpi.KdGroup = fgi.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbi.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$ri = $this->sv->get();
		return $ri->result();
	}
	public function get_lab($reg="")
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
						dbl.Tanggal, fpl.KdGroup, fgl.NmGroup, fpl.NmDetail, ftl.KDTarif, dbl.NmTarif,
						dbl.JumlahBiaya, dbl.KdDokter, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilLab hbl", "fi.Regno = hbl.Regno", "LEFT");
		$this->sv->join("DetailBilLab dbl", "hbl.NoTran = dbl.NoTran", "LEFT");
		$this->sv->join("fTarifLaboratorium ftl", "ftl.KDTarif = dbl.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanLab fpl", "ftl.KDDetail = fpl.KDDetail", "LEFT");
		$this->sv->join("fGroupLab fgl", "fpl.KdGroup = fgl.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbl.KdDokter', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$lab = $this->sv->get();
		return $lab->result();
	}
	public function get_rad($reg="")
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
							dbr.Tanggal, fpr.KdGroup, fgr.NmGroup, fpr.NmDetail, ftr.KDTarif, dbr.NmTarif, dbr.Qty,
							dbr.JumlahBiaya, dbr.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilRad hbr", "fi.Regno = hbr.Regno", "LEFT");
		$this->sv->join("DetailBilRad dbr", "hbr.NoTran = dbr.NoTran", "LEFT");
		$this->sv->join("fTarifRadiologi ftr", "ftr.KDTarif = dbr.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanRad fpr", "ftr.KDDetail = fpr.KDDetail", "LEFT");
		$this->sv->join("fGroupRad fgr", "fpr.KdGroup = fgr.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbr.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$rad = $this->sv->get();
		return $rad->result();
	}
	public function get_ugd($reg="")
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
							dbu.Tanggal, fpu.KdGroup, fgu.NmGroup, fpu.NmDetail, ftu.KDTarif, dbu.NmTarif, dbu.Qty,
							dbu.JumlahBiaya, dbu.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilUgd hbu", "fi.Regno = hbu.Regno", "LEFT");
		$this->sv->join("DetailBilUgd dbu", "hbu.NoTran = dbu.NoTran", "LEFT");
		$this->sv->join("fTarifRawatDarurat ftu", "ftu.KDTarif = dbu.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIGD fpu", "ftu.KDDetail = fpu.KDDetail", "LEFT");
		$this->sv->join("fGroupIGD fgu", "fpu.KdGroup = fgu.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbu.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$ugd = $this->sv->get();
		return $ugd->result();
	}
}

/* End of file biling_model.php */
/* Location: ./application/models/biling_model.php */