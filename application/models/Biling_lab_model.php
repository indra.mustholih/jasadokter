<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_lab_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}

	public function get_lab($ktg='',$tgl_awal='',$tgl_akhir='')
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['lab'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbl.Notran, hbl.Tanggal, hbl.NoLab, hbl.Regno, hbl.Regdate,
			hbl.Medrec, hbl.Firstname, hbl.NmDoc, pt.NMPoli, tk.NMKelas,
			tc.NMCbayar,  hbl.Jam, hbl.Sex, hbl.Umurthn, hbl.UmurBln, hbl.UmurHari,
			hbl.KdBangsal, tb.NmBangsal, hbl.KdKelas, hbl.KdPoli, hbl.kdCbayar,
			hbl.Kategori, hbl.KdDoc, hbl.NmDoc, hbl.KdDokter, hbl.TglSelesai,
			hbl.JamSelesai, hbl.nStatus, hbl.nJenis, hbl.TotalBiaya, hbl.ValidUser");
		$this->sv->from("HeadBilLab hbl");
		$this->sv->join("POLItpp pt", "hbl.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLBangsal tb","hbl.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas tk","hbl.KdKelas = tk.KDKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbl.KdCbayar", "LEFT");
		if($ktg){$this->sv->where("hbl.Kategori",$ktg);}
		if($tgl_awal){$this->sv->where("hbl.Tanggal >= ",$tgl_awal);}
		if($tgl_akhir){$this->sv->where("hbl.Tanggal <= ",$tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("dbl.Notran, dbl.Tanggal, fpl.KdGroup, dbl.KdTarif, dbl.NmTarif, dbl.Sarana, dbl.Pelayanan, dbl.JumlahBiaya, dbl.nCover,
				dbl.KdDokter, fd.NmDoc");
		$this->sv->from("DetailBilLab dbl");
		$this->sv->join("fTarifLaboratorium ftl", "ftl.KDTarif = dbl.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanLab fpl", "ftl.KDDetail = fpl.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", "fd.KdDoc = dbl.KdDokter", "LEFT");
		if(!empty($fill)) $this->sv->where_in('dbl.Notran', $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['lab'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['lab'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['Notran'] = $h->Notran;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['NmBangsal'] = $h->NmBangsal;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['NMKelas'] = $h->NMKelas;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['Regno'] = $h->Regno;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['Firstname'] = $h->Firstname;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['KdPoli'] = $h->KdPoli;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['NMPoli'] = $h->NMPoli;
			}
			$hasil['lab'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}

		return $this->cl->convToObj($hasil['lab']);
	}
}