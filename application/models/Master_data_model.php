<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_data_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		$this->sv = $this->load->database('server', true);
	}

    public function get_tarif_bpjs_irna($key='',$value='')
    {
        $this->sv->from('tarif_pelayanan_bpjs_irna');
        if($key!='' && $value!=''){$this->sv->where($key,$value);}
        $this->sv->order_by('IdTarif','DESC');
        $data = $this->sv->get()->row();
        return $data;
    }

    public function up_tarif_bpjs_irna($id='',$data='')
    {
        $up = null;
        if($id!=''){
            $this->sv->where('IdTarif',$id);
            $up = $this->sv->update('tarif_pelayanan_bpjs_irna',$data);
        }else{
            $up = $this->sv->insert('tarif_pelayanan_bpjs_irna',$data);
        }
        return $up;
    }

    public function get_sisa_bayar($key='',$value='')
    {
        $this->sv->select("ds.id, ds.kd_doc, fd.NmDoc as nm_doc, ds.sisa_bayar");
        $this->sv->from("data_sisa_bayar_dokter ds");
        $this->sv->join("FtDokter fd", "ds.kd_doc = fd.KdDoc");
        if($key!=''&&$value!=''){$this->sv->where($key,$value);}
        $data = $this->sv->get();
        return $data->result();
    }
    public function up_sisa_bayar($id='',$data=array())
    {
        if($id!=''){
            $this->sv->where('id',$id);
            $up = $this->sv->update('data_sisa_bayar_dokter',$data);
        }else{
            $up = $this->sv->insert('data_sisa_bayar_dokter',$data);
        }
        return $up;
    }

    public function del_sisa_bayar($id='')
    {
        $this->sv->where('id',$id);
        $del = $this->sv->delete('data_sisa_bayar_dokter');
        return $del;
    }
}

/* End of file biling_model.php */
/* Location: ./application/models/biling_model.php */