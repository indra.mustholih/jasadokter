<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_ranap_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}
	public function get_pasien($reg="")
	{
		$this->sv->select("fi.Regno, mp.Medrec, mp.Firstname, mp.Address,
			mp.Phone, pt.KDPoli, pt.NMPoli, fi.KdBangsal, tb.NmBangsal, fi.KdKelas, tk.NMKelas, fm.KdBangsal1 as KdBangsalPindah, tbp.NmBangsal as BangsalPindah, fm.KdKelas1 as KdKelasPindah, tkp.NMKelas as KelasPindah, 
				fm.Lama as LamaPindah, tk.ByTarif, fi.Kategori, tkp.ByTarif as TarifPindah, fi.KdCbayar, tc.GroupUnit,
			mp.UmurThn, fi.Regdate, fp.Tanggal");
		$this->sv->from("FPPRI fi");
		$this->sv->join("FPindah fm","fi.Regno = fm.Regno", "LEFT");
		$this->sv->join("MasterPS mp","fi.Medrec = mp.Medrec", "LEFT");
		$this->sv->join("TBLBangsal tb","tb.KdBangsal = fi.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = fi.KdKelas", "LEFT");
		$this->sv->join("POLItpp pt","pt.KDPoli = fi.KdPoli", "LEFT");
		$this->sv->join("TBLBangsal tbp","tbp.KdBangsal = fm.KdBangsal1", "LEFT");
		$this->sv->join("TBLKelas tkp","tkp.KDKelas = fm.KdKelas1", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = fi.KdCbayar", "LEFT");
		$this->sv->join("FPulang fp","fp.Regno = fi.Regno", "LEFT");
		$this->sv->where("fi.Regno", $reg);
		$ps = $this->sv->get();
		return $ps->first();
	}
	public function get_apotek($reg="")
	{
		// set atgribut
		$raw = $fill = $hasil['apt'] = $hasil['di'] = [];
		$hasil['du'] = $hasil['do'] = $hasil['dh'] = [];
		$n0=$n1=$n2=$n3=$n4=$n5=0;
		// GET OBAT AOPTEK
		$this->sv->select("ha.Regno, ha.KdBangsal, ha.KdKelas, ha.KdCbayar, ha.Regdate, 
							ha.BLCode, da.BLDate, da.NamaObat, da.JumlahHarga");
		$this->sv->from("HeadApotik ha");
		$this->sv->join("DetailApotik da","ha.BLCode = da.BLCode","LEFT");
		$this->sv->where("ha.Regno", $reg);
		$apt = $this->sv->get()->result();
		// FILL DATA TO CONTAINER
		foreach ($apt as $d) {
			$hasil['apt'][$n1]['Tanggal'] = $d->BLDate;
			$hasil['apt'][$n1]['KdBangsal'] = $d->KdBangsal;
			$hasil['apt'][$n1]['NamaObat'] = $d->NamaObat;
			$hasil['apt'][$n1]['JumlahHarga'] = $d->JumlahHarga;
			$n1++;
		}
		// // GET OBAT DEPO IRNA
		// $this->sv->select("hdi.BLCode, hdi.BLDate, hdi.Jam, hdi.Regno, hdi.Regdate, hdi.Medrec, hdi.Firstname,
		// 					hdi.KdBangsal, hdi.KdKelas, hdi.KdPoli, hdi.KdDoc, hdi.KdCBayar, hdi.KdJaminan, 
		// 					ddi.KodeObat, ddi.NamaObat, ddi.Harga, ddi.Qty, ddi.JumlahHarga");
		// $this->sv->from("dbRSDEPO.dbo.HeadDepoIrna hdi");
		// $this->sv->join("dbRSDEPO.dbo.DetailDepoIrna ddi", "hdi.BLCode = ddi.BLCode","LEFT");
		// $this->sv->where("hdi.Regno", $reg);
		// $di = $this->sv->get()->result();
		// // FILL DATA TO CONTAINER
		// foreach ($di as $d) {
		// 	$hasil['di'][$n2]['Tanggal'] = $d->BLDate;
		// 	$hasil['di'][$n2]['KdBangsal'] = $d->KdBangsal;
		// 	$hasil['di'][$n2]['NamaObat'] = $d->NamaObat;
		// 	$hasil['di'][$n2]['JumlahHarga'] = $d->JumlahHarga;
		// 	$n2++;
		// }
		// // GET OBAT DEPO IGD
		// $this->sv->select("hdu.BLCode, hdu.BLDate, hdu.Jam, hdu.Regno, hdu.Regdate, hdu.Medrec, hdu.Firstname,
		// 					hdu.KdBangsal, hdu.KdKelas, hdu.KdPoli, hdu.KdDoc, hdu.KdCBayar, hdu.KdJaminan, 
		// 					ddu.KodeObat, ddu.NamaObat, ddu.Harga, ddu.Qty, ddu.JumlahHarga");
		// $this->sv->from("dbRSDEPO.dbo.HeadDepoIGD hdu");
		// $this->sv->join("dbRSDEPO.dbo.DetailDepoIGD ddu", "hdu.BLCode = ddu.BLCode","LEFT");
		// $this->sv->where("hdu.Regno", $reg);
		// $du = $this->sv->get()->result();
		// // FILL DATA TO CONTAINER
		// foreach ($du as $d) {
		// 	$hasil['du'][$n3]['Tanggal'] = $d->BLDate;
		// 	$hasil['du'][$n3]['KdBangsal'] = $d->KdBangsal;
		// 	$hasil['du'][$n3]['NamaObat'] = $d->NamaObat;
		// 	$hasil['du'][$n3]['JumlahHarga'] = $d->JumlahHarga;
		// 	$n3++;
		// }
		// // GET OBAT DEPO OK
		// $this->sv->select("hdo.BLCode, hdo.BLDate, hdo.Jam, hdo.Regno, hdo.Regdate, hdo.Medrec, hdo.Firstname,
		// 					hdo.KdBangsal, hdo.KdKelas, hdo.KdPoli, hdo.KdDoc, hdo.KdCBayar, hdo.KdJaminan, 
		// 					ddo.KodeObat, ddo.NamaObat, ddo.Harga, ddo.Qty, ddo.JumlahHarga");
		// $this->sv->from("dbRSDEPO.dbo.HeadDepoOK hdo");
		// $this->sv->join("dbRSDEPO.dbo.DetailDepoOK ddo ", "hdo.BLCode = ddo.BLCode","LEFT");
		// $this->sv->where("hdo.Regno", $reg);
		// $do = $this->sv->get()->result();
		// // FILL DATA TO CONTAINER
		// foreach ($do as $d) {
		// 	$hasil['do'][$n4]['Tanggal'] = $d->BLDate;
		// 	$hasil['do'][$n4]['KdBangsal'] = $d->KdBangsal;
		// 	$hasil['do'][$n4]['NamaObat'] = $d->NamaObat;
		// 	$hasil['do'][$n4]['JumlahHarga'] = $d->JumlahHarga;
		// 	$n4++;
		// }
		// GET OBAT DEPO HD
		// $this->sv->select("hdh.BLCode, hdh.BLDate, hdh.Jam, hdh.Regno, hdh.Regdate, hdh.Medrec, hdh.Firstname,
		// 				hdh.KdBangsal, hdh.KdKelas, hdh.KdPoli, hdh.KdDoc, hdh.KdCBayar, hdh.KdJaminan, 
		// 				ddh.KodeObat, ddh.NamaObat, ddh.Harga, ddh.Qty, ddh.JumlahHarga");
		// $this->sv->from("dbRSDEPO.dbo.HeadDepoHemo hdh");
		// $this->sv->join("dbRSDEPO.dbo.DetailDepoHemo ddh", "hdh.BLCode = ddh.BLCode","LEFT");
		// $this->sv->where("hdh.Regno", $reg);
		// $do = $this->sv->get()->result();	
		return $this->cl->convToObj($hasil);
	}
	/* public function get_aku($reg="")
	{
		$this->sv->select("fi.Regno, fi.Medrec, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
			hba.NoTran, dba.Tanggal, hba.KdCbayar, hba.TotalBiaya, dba.KdTarif, dba.NmTarif, dba.Qty,
			dba.JumlahBiaya, dba.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilAku hba", "fi.Regno = hba.Regno", "LEFT");
		$this->sv->join("DetailBilAku dba", "hba.NoTran = dba.NoTran", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dba.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$ak = $this->sv->order_by('dba.Tanggal', 'ASC')->get();
		return $ak->result();
	}*/
	public function get_ibs($reg="")
	{
		// SET ATRIBUT
		$fill = $head = $detail = $hasil['opr'] = [];
		$n0 = $n1 = 0;
		// GET HEADER TRNSAKSI 
		$this->sv->select("bo.Notran, bo.Tanggal, bo.Regno, bo.Regdate, bo.Medrec, bo.Firstname,
							tb.KdBangsal, tb.NmBangsal, tk.NMKelas, tc.NMCbayar, tj.NMJaminan");
		$this->sv->from("BilOperasi bo");
		$this->sv->join("TBLBangsal tb","tb.KdBangsal = bo.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = bo.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = bo.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "bo.KdJaminan=tj.KDJaminan", "LEFT");
		$this->sv->where("bo.Regno",$reg);
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("Notran, Regno, Tanggal, Medrec, Regdate, Firstname, KdBangsal, KdKelas, KdCbayar, KdJaminan,
					Kategori, KdDrOpr, KdDrRawat, KdAsDrOpr, KdDrAnestesi, KdDrPenata, KdDrAnak, KdDrPendamping, KdBidan,
					KodeTarif, NamaTindakan, DetailCode, DetailName, ByRrawat, ByAlatRS, ByDrOpr, ByAsDrOpr, ByDrAnestesi,
					ByDrPenata, ByDrAnak, ByDrPendamping, ByBidan, ByAsBidan, BySewaAlat, ByRroom, ByObat, JumlahBiaya,
					CitoDrOperator, CitoAsDrOperator, CitoDrAnestesi, CitoAsDrAnestesi, CitoDrAnak, CitoDrPendamping");
		$this->sv->from("BilOperasi");
		$this->sv->where("Regno",$reg);
		if(!empty($fill)) $this->sv->where_in("Notran",$fill);
		$detail = $this->sv->get()->result();
		// FILL IH ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['opr'][$n1]['Notran'] = $d->Notran;
			$hasil['opr'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['opr'][$n1]['NamaTindakan'] = $d->NamaTindakan;
			foreach ($head as $h) {
				if($h->KdBangsal == $d->KdBangsal){
					$hasil['opr'][$n1]['KdBangsal'] = $h->KdBangsal;
					$hasil['opr'][$n1]['Firstname'] = $h->Firstname;
					$hasil['opr'][$n1]['NmBangsal'] = $h->NmBangsal;
				}
			}
			$hasil['opr'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$hasil['dtl'][$n1]['Notran'] = $d->Notran;
			$hasil['dtl'][$n1]['Biaya_Rawat'] = $d->ByRrawat;
			$hasil['dtl'][$n1]['Biaya_Alat_RS'] = $d->ByAlatRS;
			$hasil['dtl'][$n1]['Biaya_Dr_Operasi'] = $d->ByDrOpr;
			$hasil['dtl'][$n1]['Biaya_Asist_Dr_Operasi'] = $d->ByAsDrOpr;
			$hasil['dtl'][$n1]['Biaya_Dr_Anestesi'] = $d->ByDrAnestesi;
			$hasil['dtl'][$n1]['Biaya_Dr_Penata'] = $d->ByDrPenata;
			$hasil['dtl'][$n1]['Biaya_Dr_Anak'] = $d->ByDrAnak;
			$hasil['dtl'][$n1]['Biaya_Bidan'] = $d->ByBidan;
			$hasil['dtl'][$n1]['Biaya_Asist_Bidan'] = $d->ByAsBidan;
			$hasil['dtl'][$n1]['Biaya_Sewa_Alat'] = $d->BySewaAlat;
			$hasil['dtl'][$n1]['Biaya_Room'] = $d->ByRroom;
			$hasil['dtl'][$n1]['Biaya_Obat'] = $d->ByObat;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	}
	public function get_fis($reg="")
	{
		// SET ATRIBUT
		$fill = $head = $detail = $hasil['fis'] = [];
		$n0 = $n1 = 0;
		// GET HEAD TRANSAKSI
		$this->sv->select("hbf.Notran, hbf.Regno, hbf.Tanggal, hbf.Jam, hbf.Medrec, hbf.Regdate, hbf.Firstname, hbf.Sex,
				hbf.umurThn, hbf.UmurBln, hbf.UmurHari, hbf.KdBangsal, hbf.KdKelas, hbf.KdPoli, hbf.KdDoc, hbf.KdCbayar, hbf.KdJaminan,
				hbf.Kategori, hbf.TotalBiaya, pt.NMPoli, tk.NMKelas, tc.NMCbayar, tj.NMJaminan, hbf.ValidUser");
		$this->sv->from("HeadBilFis hbf");
		$this->sv->join("POLItpp pt", "hbf.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = hbf.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbf.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbf.KdJaminan=tj.KDJaminan", "LEFT");
		$this->sv->where('hbf.Regno', $reg);
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select(" dbf.Notran, dbf.Regno, dbf.Tanggal, fpe.KdGroup, fpe.NMDetail, fte.KDTarif, dbf.NmTarif, dbf.Qty,
							dbf.JumlahBiaya, dbf.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilFis dbf");
		$this->sv->join("fTarifElektromedik fte", "fte.KDTarif = dbf.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanEle fpe", "fte.KDDetail = fpe.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbf.KdDoc', "LEFT");
		$this->sv->where('dbf.Regno', $reg);
		if(!empty($fill)) $this->sv->where_in('dbf.Notran', $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['fis'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['fis'][$n1]['NmTarif'] = $d->NmTarif;
			$hasil['fis'][$n1]['NmDoc'] = $d->NmDoc;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['fis'][$n1]['KdBangsal'] = $h->KdBangsal;
			}
			$hasil['fis'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	}
	public function get_hd($reg="")
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['hd'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbr.Notran, hbr.Tanggal, hbr.Regno, hbr.Regdate, hbr.KdBangsal,
						hbr.Medrec, hbr.Firstname, hbr.Shift, pt.NMPoli,
						tc.NMCbayar, tj.NMJaminan, hbr.ValidUser");
		$this->sv->from("HeadBilHD hbr");
		$this->sv->join("TBLBangsal tb","hbr.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("POLItpp pt", "hbr.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbr.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbr.KdJaminan=tj.KDJaminan", "LEFT");
		$this->sv->where('hbr.Regno', $reg);
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("dbr.Notran, dbr.Regno, dbr.Tanggal, fpr.KdGroup, fpr.NmDetail, ftr.KDTarif, dbr.NmTarif, dbr.Qty,
							dbr.JumlahBiaya, dbr.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilHD dbr");
		$this->sv->join("fTarifHemodialisa ftr", "ftr.KDTarif = dbr.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanHemo fpr", "ftr.KDDetail = fpr.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbr.KdDoc', "LEFT");
		$this->sv->where('dbr.Regno', $reg);
		if(!empty($fill)) $this->sv->where_in("dbr.Notran", $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['hd'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['hd'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['hd'][$n1]['KdBangsal'] = $h->KdBangsal;
			}
			$hasil['hd'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	} 
	public function get_irj($reg="")
	{
		// SET ATRIBUT
		$fill = $head = $detail = $hasil['irj'] = [];
		$n0 = $n1 = 0;
		// GET HEAD TRANSAKSI
		$this->sv->select("hbj.Notran, hbj.Tanggal, hbj.Regno,  hbj.Regdate, hbj.Medrec, hbj.Firstname, hbj.KdPoli,
							fd.NmDoc, pt.NMPoli, tc.NMCbayar, tj.NMJaminan, hbj.ValidUser");
		$this->sv->from("HeadBilIrj hbj");
		$this->sv->join("POLItpp pt", "hbj.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbj.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbj.KdJaminan=tj.KDJaminan", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = hbj.KdDoc', "LEFT");
		$this->sv->where('hbj.Regno', $reg);
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DATA DETAIL
		$this->sv->select("dbj.Notran, dbj.Regno, dbj.Tanggal, fpj.KdGroup, fpj.NMDetail, ftj.KDTarif, dbj.NmTarif, dbj.Qty,
						dbj.JumlahBiaya, dbj.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilIrj dbj");
		$this->sv->join("fTarifRawatJalan ftj", "ftj.KDTarif = dbj.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIRJ fpj", "ftj.KDDetail = fpj.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbj.KdDoc', "LEFT");
		$this->sv->where('dbj.Regno', $reg);
		if(!empty($fill)) $this->sv->where_in('dbj.Notran',$fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['irj'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['irj'][$n1]['NmTarif'] = $d->NmTarif;
			$hasil['irj'][$n1]['NmDoc'] = $d->NmDoc;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['irj'][$n1]['KdPoli'] = $h->KdPoli;
			}
			$hasil['irj'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	}
	public function get_irna($reg="")
	{
		// SET ATRIBUT
		$raw = $fill = $hasil['adm'] = $hasil['visite'] = $hasil['askep'] = [];
		$hasil['jaskep'] = $hasil['adpj'] = $hasil['bak'] = $hasil['hmd'] = [];
		$n0=$n1=$n2=$n3=$n4=$n5=$n6=$n7=0;
		// GET HEADER BILING RAWAT INAP
		$this->sv->select("Notran, Tanggal, Regno, Regdate, Medrec, Firstname, KdBangsal, KdKelas,
							KdCbayar, KdJaminan, Kategori, TotalBiaya, Verifikasi, VCode");
		$this->sv->from("HeadBilIrna hbi");
		$this->sv->where("Regno",$reg);
		// if(!empty($fill)) $this->sv->where_in('Notran',$fill);
		// $this->sv->order_by('Tanggal','ASC');
		$head = $this->sv->get()->result();
		foreach ($head as $r) {
			$fill[] = $r->Notran;
		}
		// GET DETAIL BILING RAWAT INAP
		$this->sv->select("dbi.Notran, dbi.Tanggal, fpi.KdGroup, dbi.KdTarif, dbi.NmTarif, dbi.Qty, dbi.Sarana, dbi.Pelayanan, dbi.Disc, dbi.JumlahBiaya, dbi.KdDoc, fd.NmDoc, dbi.Sts");
		$this->sv->from("DetailBilIrna dbi");
		$this->sv->join("fTarifRawatInap fti", "dbi.KdTarif = fti.KDTarif");
		$this->sv->join("fPemeriksaanIRNA fpi", "fti.KDDetail = fpi.KDDetail");
		$this->sv->join("FtDokter fd", "fd.KdDoc = dbi.KdDoc", "LEFT");
		$this->sv->where("dbi.Regno",$reg);
		if(!empty($fill)) $this->sv->where_in('dbi.Notran',$fill);
		// $this->sv->order_by('dbi.Tanggal','ASC');
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTGAINER
		foreach ($detail as $d) {
			// VISITE DOKTER
			if($d->KdGroup == '01' || $d->KdGroup == '15'){
				$hasil['visite'][$n1]['Tanggal'] = $d->Tanggal;
				$hasil['visite'][$n1]['NmTarif'] = $d->NmTarif;
				$hasil['visite'][$n1]['NmDoc'] = $d->NmDoc;
				foreach ($head as $h) {
					if($h->Notran == $d->Notran) $hasil['visite'][$n1]['KdBangsal'] = $h->KdBangsal;
				}
				$hasil['visite'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
				$n1++;
			}
			// ASUHAN KEPERAWATAN
			if($d->KdGroup == '02' || strtolower($d->NmTarif) == 'askep'){
				$hasil['askep'][$n2]['Tanggal'] = $d->Tanggal;
				$hasil['askep'][$n2]['NmTarif'] = $d->NmTarif;
				$hasil['askep'][$n2]['NmDoc'] = $d->NmDoc;
				foreach ($head as $h) {
					if($h->Notran == $d->Notran) $hasil['askep'][$n2]['KdBangsal'] = $h->KdBangsal;
				}
				$hasil['askep'][$n2]['JumlahBiaya'] = $d->JumlahBiaya;
				$n2++;
			}
			// ADMINISTRASI 
			if($d->KdGroup == '21'){
				$hasil['adm'][$n3]['Tanggal'] = $d->Tanggal;
				$hasil['adm'][$n3]['NmTarif'] = $d->NmTarif;
				$hasil['adm'][$n3]['NmDoc'] = $d->NmDoc;
				foreach ($head as $h) {
					if($h->Notran == $d->Notran) $hasil['adm'][$n3]['KdBangsal'] = $h->KdBangsal;
				}
				$hasil['adm'][$n3]['JumlahBiaya'] = $d->JumlahBiaya;
				$n3++;
			}
			// BIAYA BAHAN ATAU ALAT KESEHATAN
			if($d->KdGroup == '05' || $d->KdGroup == '04'  || $d->KdGroup == '07'){
				$hasil['bak'][$n4]['Tanggal'] = $d->Tanggal;
				$hasil['bak'][$n4]['NmTarif'] = $d->NmTarif;
				$hasil['bak'][$n4]['NmDoc'] = $d->NmDoc;
				foreach ($head as $h) {
					if($h->Notran == $d->Notran) $hasil['bak'][$n4]['KdBangsal'] = $h->KdBangsal;
				}
				$hasil['bak'][$n4]['JumlahBiaya'] = $d->JumlahBiaya;
				$n4++;
			}
			// HEMODIAL / KEMO
			if($d->KdGroup == '12'){
				$hasil['hmd'][$n5]['Tanggal'] = $d->Tanggal;
				$hasil['hmd'][$n5]['NmTarif'] = $d->NmTarif;
				$hasil['hmd'][$n5]['NmDoc'] = $d->NmDoc;
				foreach ($head as $h) {
					if($h->Notran == $d->Notran) $hasil['hmd'][$n5]['KdBangsal'] = $h->KdBangsal;
				}
				$hasil['hmd'][$n5]['JumlahBiaya'] = $d->JumlahBiaya;
				$n5++;
			}
			// AMBULAN DAN PEMULASARAN JENAZAH
			if($d->KdGroup == '18' || $d->KdGroup == '25'){
				$hasil['adpj'][$n6]['Tanggal'] = $d->Tanggal;
				$hasil['adpj'][$n6]['NmTarif'] = $d->NmTarif;
				$hasil['adpj'][$n6]['NmDoc'] = $d->NmDoc;
				foreach ($head as $h) {
					if($h->Notran == $d->Notran) $hasil['adpj'][$n6]['KdBangsal'] = $h->KdBangsal;
				}
				$hasil['adpj'][$n6]['JumlahBiaya'] = $d->JumlahBiaya;
				$n6++;
			}
			// JASA KEPAWATAN
			if($d->KdGroup != '01' && $d->KdGroup != '15' && $d->KdGroup != '02' && $d->KdGroup != '21' && $d->KdGroup != '05' && $d->KdGroup != '04' && $d->KdGroup != '18' && $d->KdGroup != '12' && $d->KdGroup != '25'){
				$hasil['jaskep'][$n7]['Tanggal'] = $d->Tanggal;
				$hasil['jaskep'][$n7]['NmTarif'] = $d->NmTarif;
				$hasil['jaskep'][$n7]['NmDoc'] = $d->NmDoc;
				foreach ($head as $h) {
					if($h->Notran == $d->Notran) $hasil['jaskep'][$n7]['KdBangsal'] = $h->KdBangsal;
				}
				$hasil['jaskep'][$n7]['JumlahBiaya'] = $d->JumlahBiaya;
				$n7++;
			}
			// ASUHAN GIZI
			// if($d->KdGroup == '14'){
			// 	$hasil['asgizi']['Tanggal'] = $d->Tanggal;
			// 	$hasil['asgizi']['NmTarif'] = $d->NmTarif;
			// 	foreach ($head as $h) {
			// 		if($h->Notran == $d->Notran) $hasil['asgizi']['KdBangsal'] = $h->KdBangsal;
			// 	}
			// 	$hasil['asgizi']['JumlahBiaya'] = $d->JumlahBiaya;
			// }


		}
		return $this->cl->convToObj($hasil);
	}
	public function get_lab($reg="")
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['lab'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbl.Notran, hbl.Tanggal, hbl.NoLab, hbl.Regno, hbl.Regdate,
			hbl.Medrec, hbl.Firstname, hbl.NmDoc, pt.NMPoli, tk.NMKelas,
			tc.NMCbayar,  hbl.Jam, hbl.Sex, hbl.Umurthn, hbl.UmurBln, hbl.UmurHari,
			hbl.KdBangsal, hbl.KdKelas, hbl.KdPoli, hbl.kdCbayar, hbl.Kategori, hbl.KdDoc, hbl.NmDoc, hbl.KdDokter, hbl.TglSelesai,
			hbl.JamSelesai, hbl.nStatus, hbl.nJenis, hbl.TotalBiaya, hbl.ValidUser");
		$this->sv->from("HeadBilLab hbl");
		$this->sv->join("POLItpp pt", "hbl.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = hbl.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbl.KdCbayar", "LEFT");
		$this->sv->where("hbl.Regno",$reg);
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("dbl.Notran, dbl.Tanggal, fpl.KdGroup, dbl.KdTarif, dbl.NmTarif, dbl.Sarana, dbl.Pelayanan, dbl.JumlahBiaya, dbl.nCover,
				dbl.KdDokter, fd.NmDoc");
		$this->sv->from("DetailBilLab dbl");
		$this->sv->join("fTarifLaboratorium ftl", "ftl.KDTarif = dbl.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanLab fpl", "ftl.KDDetail = fpl.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", "fd.KdDoc = dbl.KdDokter", "LEFT");
		$this->sv->where("dbl.Regno",$reg);
		if(!empty($fill)) $this->sv->where_in('dbl.Notran', $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['lab'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['lab'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['KdBangsal'] = $h->KdBangsal;
			}
			$hasil['lab'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}

		return $this->cl->convToObj($hasil);
	}
	public function get_rad($reg="")
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['rad'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbr.Notran, hbr.Tanggal, hbr.Regno, hbr.Regdate, hbr.KdBangsal,
						hbr.Medrec, hbr.Firstname, hbr.Shift, pt.NMPoli,
						tc.NMCbayar, tj.NMJaminan, hbr.ValidUser");
		$this->sv->from("HeadBilRad hbr");
		$this->sv->join("TBLBangsal tb","hbr.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("POLItpp pt", "hbr.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbr.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbr.KdJaminan=tj.KDJaminan", "LEFT");
		$this->sv->where('hbr.Regno', $reg);
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("dbr.Notran, dbr.Regno, dbr.Tanggal, fpr.KdGroup, fpr.NmDetail, ftr.KDTarif, dbr.NmTarif, dbr.Qty,
							dbr.JumlahBiaya, dbr.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilRad dbr");
		$this->sv->join("fTarifRadiologi ftr", "ftr.KDTarif = dbr.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanRad fpr", "ftr.KDDetail = fpr.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbr.KdDoc', "LEFT");
		$this->sv->where('dbr.Regno', $reg);
		if(!empty($fill)) $this->sv->where_in("dbr.Notran", $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['rad'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['rad'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['KdBangsal'] = $h->KdBangsal;
			}
			$hasil['rad'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	}
	public function get_ugd($reg="")
	{
		// SET ATRIBUT
		$head = $detail = $fill = $hasil['ugd'] = [];
		$n0=$n1=0;
		// GET HEAD TRANSAKSI
		$this->sv->select("hbu.Notran, hbu.Tanggal, hbu.Regno, hbu.Regdate, hbu.Medrec,
						hbu.Firstname, tc.NMCbayar, tj.NMJaminan, hbu.ValidUser");
		$this->sv->from("HeadBilUgd hbu");
		$this->sv->join("POLItpp pt", "hbu.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbu.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbu.KdJaminan=tj.KDJaminan", "LEFT");
		$this->sv->where("hbu.Regno",$reg);
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI 
		$this->sv->select("dbu.Regno, dbu.Tanggal, fpu.KdGroup, fpu.NmDetail, 
							ftu.KDTarif, dbu.NmTarif, dbu.Qty, dbu.JumlahBiaya, dbu.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilUgd dbu");
		$this->sv->join("fTarifRawatDarurat ftu", "ftu.KDTarif = dbu.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIGD fpu", "ftu.KDDetail = fpu.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbu.KdDoc', "LEFT");
		$this->sv->where('dbu.Regno', $reg);
		if(!empty($fill)) $this->sv->where_in('dbu.Notran', $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['ugd'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['ugd'][$n1]['NmTarif'] = $d->NmTarif;
			$hasil['ugd'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil);

		// return $ugd->result();
	}
	public function get_umuk($reg='')
	{
		$this->sv->where('Regno', $reg);
		$umuk = $this->sv->get('UangMuka');
		return $umuk->row();
	}
}

/* End of file biling_model.php */
/* Location: ./application/models/biling_model.php */