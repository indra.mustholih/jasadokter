<?php
// komp biaya
// 1 Rawat Jalan
// 2 Rawat Inap


defined('BASEPATH') OR exit('No direct script access allowed');

class JasaPelayanan_model extends CI_Model {

  protected $sv;

  // variable utk datatable serverside
	var $tbl = 'komp_jasa';
	var $column_order = array(null,'nama_jasa');
  var $column_search = array('nama_jasa',"");
	var $order = array(''=> '');
  

  //private variable untuk  memanggl nama table dan id nya
  private $table = 'komp_jasa';
  private $id = 'komp_jasaid';

	public function __construct()
	{
		parent::__construct();
    $this->sv = $this->load->database('server', true);
    $this->load->library('cl');
  }
  public function listing(){
    $this->sv->select("
      a.komp_jasaid,
      a.komp_biayaid,
      a.nama_jasa,
      a.keterangan,
      b.nama_komp,
      b.komp_biaya


    ");
    $this->sv->from('komp_jasa a');
    $this->sv->join("komp_biaya b", "a.komp_biayaid=b.komp_biayaid", "JOIN");
    $this->sv->order_by('nama_jasa','ASC');
		$query = $this->sv->get();
    return $query->result_array();

  }

  public function add($data){
    $this->sv->insert($this->tbl,$data);
  }

  public function edit($komp_jasaid){
    $this->sv->select("
      a.komp_jasaid,
      a.komp_biayaid,
      a.nama_jasa,
      a.keterangan,
      b.nama_komp,
      b.komp_biaya

    ");
    $this->sv->from(' komp_jasa a ');
    $this->sv->join("komp_biaya b", "a.komp_biayaid=b.komp_biayaid", "JOIN");
		$this->sv->where('komp_jasaid',$komp_jasaid);
		$query = $this->sv->get();
		return $query->row_array();
  }
  public function editproses($komp_jasaid, $data){
    $this->sv->where('komp_jasaid',$komp_jasaid);
		$this->sv->update('komp_jasa',$data);
  }

  public function showAllKompBiaya(){
    $this->sv->select("*");
    $this->sv->from('komp_biaya');
		$query = $this->sv->get();
		return $query->result_array();
  }
  //function delete data
  public function delete($data){
    $this->sv->where($data);
    $this->sv->delete($this->tbl);
  } 




  //datatables serverside
  private function _get_datatables_query() {
    
    $this->sv->select("
      a.komp_jasaid,
      a.komp_biayaid,
      a.nama_jasa,
      a.keterangan,
      b.nama_komp,
      b.komp_biaya

    ");
    $this->sv->from('komp_jasa a ');
    $this->sv->join("komp_biaya b", "a.komp_biayaid=b.komp_biayaid", "JOIN");

    $i = 0;

    foreach ($this->column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->sv->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->sv->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->sv->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->sv->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->sv->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->sv->order_by(key($order), $order[key($order)]);
    }
  }

function get_datatables()
{
  $this->_get_datatables_query();
  if($_POST['length'] != -1)
  $this->sv->limit($_POST['length'], $_POST['start']);
  $query = $this->sv->get();
  return $query->result();
}

// function get_datatables_verify()
// {
//   $this->_get_datatables_query();
//   if($_POST['length'] != -1)
//   $this->db->limit($_POST['length'], $_POST['start']);
//   $this->db->where("akses_level","kecamatan");
//   $query = $this->db->get();
//   return $query->result();
// }

function count_filtered()
{
  $this->_get_datatables_query();
  $query = $this->sv->get();
  return $query->num_rows();
}

public function count_all()
{
  $this->sv->from($this->tbl);
  return $this->sv->count_all_results();
}

// public function get_by_id($id)
//   {
//       $this->db->from($this->table);
//       $this->db->where('id_user',$id);
//       $query = $this->db->get();

//       return $query->row();
//   }
  
}