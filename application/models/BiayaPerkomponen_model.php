<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BiayaPerkomponen_model extends CI_Model {

  protected $sv;

  // variable utk datatable serverside
	var $tbl = 'komp_perbiaya';
	var $column_order = array(null,'komp_biayaid');
  var $column_search = array('');
	var $order = array(''=> '');
  

  //private variable untuk  memanggl nama table dan id nya
  private $table = 'komp_perbiaya';
  private $id = 'komp_perbiayaid';

	public function __construct()
	{
		parent::__construct();
    $this->sv = $this->load->database('server', true);
    $this->load->library('cl');
  }

  public function listing()
  {
    $this->sv->select("
        a.komp_perbiayaid, a.komp_biayaid, a.komp_jasaid, case when jenis_jasa = 1 then 'Medis' when jenis_jasa=2 then 'Dokter' else '' end as jenis_jasa,
        a.biaya_jasa, a.persen, b.nama_komp, c.nama_jasa, a.kd_tarif
    ");
    $this->sv->from('komp_perbiaya a');
    $this->sv->join("komp_biaya b", "a.komp_biayaid=b.komp_biayaid", "JOIN");
    $this->sv->join("komp_jasa c", "a.komp_jasaid=c.komp_jasaid", "JOIN");
    $this->sv->join("fTarifRawatJalan d", "a.kd_tarif=d.KDTarif", "LEFT JOIN");
    $this->sv->join("fPemeriksaanIRJ e", "a.KDDetail=e.KDDetail", "LEFT JOIN");
    $this->sv->order_by('komp_perbiayaid','ASC');
		$query = $this->sv->get();
    return $query->result_array();

  }

  public function showAllKompBiaya(){
    $this->sv->select("*");
    $this->sv->from('komp_biaya');
		$query = $this->sv->get();
		return $query->result_array();
  }

  public function showAllKompJasa(){
    $this->sv->select("*");
    $this->sv->from('komp_jasa');
		$query = $this->sv->get();
		return $query->result_array();
  }

  public function showAllKompTarifRawatJalan(){
    $this->sv->select("a.KDTarif, b.NMDetail, c.NmKategori");
    $this->sv->from('fTarifRawatJalan a');
    $this->sv->join("fPemeriksaanIRJ b", "a.KDDetail=b.KDDetail", "JOIN");
    $this->sv->join("TblKategoriPsn c", "c.KdKategori=a.Kategori", "JOIN");
		$query = $this->sv->get();
		return $query->result();
  }

  

  public function add($data){
    $this->sv->insert($this->tbl,$data);
  }

  public function edit($komp_perbiayaid){
    $this->sv->select("
      a.*

    ");
    $this->sv->from(' komp_perbiaya a ');
    // $this->sv->join("komp_biaya b", "a.komp_biayaid=b.komp_biayaid", "JOIN");
		$this->sv->where('komp_perbiayaid',$komp_perbiayaid);
		$query = $this->sv->get();
		return $query->row_array();
  }

  //function edit proses
  public function editproses($komp_perbiayaid, $data){
    $this->sv->where('komp_perbiayaid',$komp_perbiayaid);
		$this->sv->update('komp_perbiaya',$data);
  }

  //function delete data
  public function delete($data){
    $this->sv->where($data);
    $this->sv->delete($this->tbl);
  } 


  


  //function datatables
  private function _get_datatables_query() {
    
    $this->sv->select("a.komp_perbiayaid, a.komp_biayaid, a.komp_jasaid, case when jenis_jasa = 1 then 'Medis' when jenis_jasa=2 then 'Dokter' else '' end as jenis_jasa,
    a.biaya_jasa, a.persen, b.nama_komp, c.nama_jasa, a.kd_tarif");
    $this->sv->from('komp_perbiaya a');
    $this->sv->join("komp_biaya b", "a.komp_biayaid=b.komp_biayaid", "JOIN");
    $this->sv->join("komp_jasa c", "a.komp_jasaid=c.komp_jasaid", "JOIN");

    $i = 0;

    foreach ($this->column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->sv->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->sv->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->sv->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->sv->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->sv->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->sv->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables(){
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->sv->limit($_POST['length'], $_POST['start']);
    $query = $this->sv->get();
    return $query->result();
  }

// function get_datatables_verify()
// {
//   $this->_get_datatables_query();
//   if($_POST['length'] != -1)
//   $this->db->limit($_POST['length'], $_POST['start']);
//   $this->db->where("akses_level","kecamatan");
//   $query = $this->db->get();
//   return $query->result();
// }

  function count_filtered(){
    $this->_get_datatables_query();
    $query = $this->sv->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->sv->from($this->tbl);
    return $this->sv->count_all_results();
  }

}