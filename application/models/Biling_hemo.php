<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_hemo extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}

	public function get_hdtes($ktg='',$kddoc='',$tgl_awal='',$tgl_akhir='')
	{
		// $this->sv->select("fi.Regno, fi.Firstname, tkp.KdKategori, tkp.NmKategori, RIGHT(fth.KdKelas,2) as KdBangsal, tb.NmBangsal,
		// 					dbd.Tanggal, fph.KdGroup, fgh.NmGroup, fph.NmDetail,
		// 					fth.KDTarif, dbd.NmTarif,dbd.Qty, dbd.JumlahBiaya, dbd.KdDoc, fd.NmDoc");
		// $this->sv->from("FPPRI fi");
		// $this->sv->join("FPindah fp", "fi.Regno = fp.Regno", "LEFT");
		// $this->sv->join("TBLKategoriPsn tkp", "fi.Kategori = tkp.KdKategori", "LEFT");
		// $this->sv->join("HeadBilHD hbd", "fi.Regno = hbd.Regno", "LEFT");
		// $this->sv->join("DetailBilHD dbd", "hbd.NoTran = dbd.NoTran", "LEFT");
		// $this->sv->join("fTarifHemodialisa fth", "fth.KDTarif = dbd.KdTarif", "LEFT");
		// $this->sv->join("fPemeriksaanHemo fph", "fth.KDDetail = fph.KDDetail", "LEFT");
		// $this->sv->join("fGroupHemo fgh", "fph.KdGroup = fgh.KdGroup", "LEFT");
		// $this->sv->join("FtDokter fd", 'fd.KdDoc = dbd.KdDoc', "LEFT");
		// $this->sv->join("TBLBangsal tb", 'RIGHT(fth.KdKelas,2) = tb.KdBangsal', "LEFT");
		// $this->sv->where('dbd.KdDoc', $KdDoc);
		// $this->sv->where('tkp.KdKategori', $ktg);
		// if ($tgl_awal!='') { $this->sv->where('dbd.Tanggal >=', $tgl_awal); }
		// if ($tgl_akhir!='') { $this->sv->where('dbd.Tanggal <=', $tgl_akhir); }
		// $hd = $this->sv->order_by('dbd.Tanggal','DESC')->get();
		// return $hd->result();

		$head = $detail = $fill = $hasil['hd'] = [];
		$n0=$n1=0;

		$this->sv->select("hbh.NoTran, hbh.Tanggal, hbh.Regno, hbh.RegDate,
			hbh.Medrec, hbh.Firstname, hbh.KdDoc, pt.NMPoli, tk.NMKelas, tc.NMCbayar,
			hbh.Sex, hbh.Umurthn, hbh.UmurBln, hbh.UmurHari,
			hbh.KdBangsal, tb.NmBangsal, hbh.KdKelas, hbh.KdPoli, hbh.kdCbayar, hbh.Kategori, 
			hbh.TotalBiaya, hbh.ValidUser");
		$this->sv->from("HeadBilHD hbh");
		$this->sv->join("POLItpp pt", "hbh.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLBangsal tb","hbh.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = hbh.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbh.KdCbayar", "LEFT");
		if($ktg!=''){$this->sv->where("hbh.Kategori",$ktg);}
		if($kddoc!=''){$this->sv->where("hbh.KdDoc",$kddoc);}
		if($tgl_awal!=''){$this->sv->where("hbh.Tanggal >=",$tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where("hbh.Tanggal <=",$tgl_akhir);}
		$head = $this->sv->get()->result();
		// return $head;
		if (empty($head)) {
			return 'Kosong';
		} 
		foreach ($head as $h) {
			$fill[] = $h->NoTran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("dhb.NoTran, dhb.Tanggal, fpl.KdGroup, dhb.KdTarif, dhb.NmTarif, dhb.Sarana, dhb.Pelayanan, dhb.JumlahBiaya, dhb.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilHD dhb");
		$this->sv->join("fTarifHemodialisa ftl", "ftl.KDTarif = dhb.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanHemo fpl", "ftl.KDDetail = fpl.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", "fd.KdDoc = dhb.KdDoc", "LEFT");
		if(!empty($fill)) $this->sv->where_in('dhb.NoTran', $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['hd'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['hd'][$n1]['NmTarif'] = $d->NmTarif;
			$hasil['hd'][$n1]['NmDoc'] = $d->NmDoc;
			foreach ($head as $h) {
				if($h->NoTran == $d->NoTran) $hasil['hd'][$n1]['NoTran'] = $h->NoTran;
				if($h->NoTran == $d->NoTran) $hasil['hd'][$n1]['Regno'] = $h->Regno;
				if($h->NoTran == $d->NoTran) $hasil['hd'][$n1]['Firstname'] = $h->Firstname;
				if($h->NoTran == $d->NoTran) $hasil['hd'][$n1]['KdPoli'] = $h->KdPoli;
				if($h->NoTran == $d->NoTran) $hasil['hd'][$n1]['NMPoli'] = $h->NMPoli;
				if($h->NoTran == $d->NoTran) $hasil['hd'][$n1]['NmBangsal'] = $h->NmBangsal;
			}
			$hasil['hd'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;

		}

		return $this->cl->convToObj($hasil['hd']);

		// $this->sv->select('headhd.NoTran, headhd.Tanggal, headhd.Regno, headhd.Medrec, headhd.Firstname');
		// $this->sv->from("HeadBilHD headhd");
		// $this->sv->join("POLItpp poli", "headhd.KdPoli=poli.KDPoli", "LEFT");
		// $this->sv->join("TBLBangsal bangsal","headhd.KdBangsal = bangsal.KdBangsal", "LEFT");
		// $this->sv->join("TBLKelas kelas", "headhd.KdKelas = kelas.KDKelas", "LEFT");
		// $this->sv->join("TBLcarabayar bayar","headhd.KdCbayar = bayar.KDCbayar", "LEFT");
		// if($ktg!=''){$this->sv->where("headhd.Kategori",$ktg);}
		// if($kddoc!=''){$this->sv->where("headhd.KdDoc",$kddoc);}
		// if($tgl_awal!=''){$this->sv->where("headhd.Tanggal >=",$tgl_awal);}
		// if($tgl_akhir!=''){$this->sv->where("headhd.Tanggal <=",$tgl_akhir);}
		// $head = ;
		// return $this->sv->get()->result();
	}
}