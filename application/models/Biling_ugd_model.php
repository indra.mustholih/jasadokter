<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_ugd_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}
	public function get_ugd($tgl_awal='',$tgl_akhir='')
	{
		// SET ATRIBUT
		$head = $detail = $fill = $hasil['ugd'] = [];
		$n0=$n1=0;
		// GET HEAD TRANSAKSI
		$this->sv->select("hbu.Notran, hbu.Tanggal, hbu.Regno, hbu.Regdate, hbu.Medrec,
						hbu.Firstname, pt.KDPoli, pt.NMPoli, tc.NMCbayar, tj.NMJaminan, hbu.ValidUser");
		$this->sv->from("HeadBilUgd hbu");
		$this->sv->join("POLItpp pt", "hbu.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbu.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbu.KdJaminan=tj.KDJaminan", "LEFT");
		if($tgl_awal){$this->sv->where("hbu.Tanggal",$tgl_awal);}
		if($tgl_akhir){$this->sv->where("hbu.Tanggal",$tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI 
		$this->sv->select("dbu.Notran, dbu.Regno, dbu.Tanggal, fpu.KdGroup, fpu.NmDetail, 
							ftu.KDTarif, dbu.NmTarif, dbu.Qty, dbu.JumlahBiaya, dbu.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilUgd dbu");
		$this->sv->join("fTarifRawatDarurat ftu", "ftu.KDTarif = dbu.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIGD fpu", "ftu.KDDetail = fpu.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbu.KdDoc', "LEFT");
		if(!empty($fill)) $this->sv->where_in('dbu.Notran', $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['ugd'][$n1]['Notran'] = $d->Notran;
			$hasil['ugd'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['ugd'][$n1]['NmTarif'] = $d->NmTarif;
			$hasil['ugd'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			foreach ($head as $h) {
				if ($h->Notran == $d->Notran) {
					$hasil['ugd'][$n1]['Firstname'] = $h->Firstname;
					$hasil['ugd'][$n1]['KDPoli'] = $h->KDPoli;
					$hasil['ugd'][$n1]['NMPoli'] = $h->NMPoli;
				}
			}
			$n1++;
		}
		return $this->cl->convToObj($hasil);

		// return $ugd->result();
	}
}