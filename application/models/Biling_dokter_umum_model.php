<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_dokter_umum_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}

    public function get_data_irj($ktg='',$kd_doc='',$tgl_awal='',$tgl_akhir='',$txtSearch='')
    {
			
        $data = array();
        $up = $this->sv->query("EXEC spw_UpDataJasaDokter_IRJ @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir'")->result();
        $data['head'] = $this->sv->query("EXEC spw_GetDataJasaDokter_IRJ @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @status = '1', @dataSet = '1', @textSrc = '$txtSearch'")->result();
        $data['detail'] = $this->sv->query("EXEC spw_GetDataJasaDokter_IRJ_New @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @status = '1', @dataSet = '2', @textSrc = '$txtSearch'")->result();
        return $this->cl->convToObj($data);
	}

    public function get_data_ugd($ktg='',$kd_doc='',$tgl_awal='',$tgl_akhir='',$txtSearch='')
    {
        $data = array();
        $up = $this->sv->query("EXEC spw_UpDataJasaDokter_UGD @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir'")->result();
        $data['head'] = $this->sv->query("EXEC spw_GetDataJasaDokter_UGD @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @status = '1', @dataSet = '1', @textSrc = '$txtSearch'")->result();
        $data['detail'] = $this->sv->query("EXEC spw_GetDataJasaDokter_UGD_New @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @status = '1', @dataSet = '2', @textSrc = '$txtSearch'")->result();
        return $this->cl->convToObj($data);
    }

    public function get_data_irna($ktg='',$kd_doc='',$tgl_awal='',$tgl_akhir='',$txtSearch='')
    {
        $data = array();
        $up = $this->sv->query("EXEC spw_UpDataJasaDokter_IRNA @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir'")->result();
        $data['head'] = $this->sv->query("EXEC spw_GetDataJasaDokter_IRNA @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @status = '1', @dataSet = '1', @textSrc = '$txtSearch'")->result();
        $data['detail'] = $this->sv->query("EXEC spw_GetDataJasaDokter_IRNA @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @status = '1', @dataSet = '2', @textSrc = '$txtSearch'")->result();
        return $this->cl->convToObj($data);
    }

    public function get_data_ok($ktg='',$kd_doc='',$tgl_awal='',$tgl_akhir='',$txtSearch='')
    {
        $data = array();
        $this->sv->query("EXEC spw_UpDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @dataSet = 1")->result();
        $this->sv->query("EXEC spw_UpDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @dataSet = 2")->result();
        $this->sv->query("EXEC spw_UpDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @dataSet = 3")->result();
        $this->sv->query("EXEC spw_UpDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @dataSet = 4")->result();
        $this->sv->query("EXEC spw_UpDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @dataSet = 5")->result();
        $this->sv->query("EXEC spw_UpDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @dataSet = 6")->result();
        $this->sv->query("EXEC spw_UpDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @dataSet = 7")->result();
        $this->sv->query("EXEC spw_UpDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @dataSet = 8")->result();

        $data['head'] = $this->sv->query("EXEC spw_GetDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @status = '1', @dataSet = '1', @textSrc = '$txtSearch'")->result();
        $data['detail'] = $this->sv->query("EXEC spw_GetDataJasaDokter_OK @kategori = '$ktg',  @kd_doc = '$kd_doc', @tgl_awal = '$tgl_awal', @tgl_akhir = '$tgl_akhir', @status = '1', @dataSet = '2', @textSrc = '$txtSearch'")->result();
        return $this->cl->convToObj($data);

    }


	public function get_visite($ktg='',$KdDoc='',$tgl_awal='',$tgl_akhir='')
	{
		// $this->sv->select("fi.Regno, fi.Firstname, tkp.KdKategori, tkp.NmKategori, fl.Tanggal as TglPulang,
		// 					dbi.Tanggal, fpi.KdGroup, fgi.NmGroup, fpi.NmDetail, RIGHT(fti.KdKelas,2) as KdBangsal, tb.NmBangsal,
		// 					fti.KdKelas as KdKelas, fti.KDTarif, dbi.NmTarif, dbi.Qty,
		// 					dbi.JumlahBiaya, dbi.KdDoc, fd.NmDoc");
		$this->sv->select("fi.Regno, fi.Firstname, tkp.KdKategori, tkp.NmKategori, fl.Tanggal as TglPulang,
							dbi.Tanggal, fpi.KdGroup, fpi.NmDetail, RIGHT(fti.KdKelas,2) as KdBangsal, tb.NmBangsal,
							fti.KdKelas as KdKelas, fti.KDTarif, dbi.NmTarif, dbi.Qty,
							dbi.JumlahBiaya, dbi.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("FPindah fp", "fi.Regno = fp.Regno", "LEFT");
		$this->sv->join("FPulang fl", "fi.Regno = fl.Regno", "LEFT");
		$this->sv->join("TBLKategoriPsn tkp", "fi.Kategori = tkp.KdKategori", "LEFT");
		// $this->sv->join("HeadBilIrna hbi", "fi.Regno = hbi.Regno", "LEFT");
		// $this->sv->join("DetailBilIrna dbi", "hbi.NoTran = dbi.NoTran", "LEFT");
		$this->sv->join("DetailBilIrna dbi", "fi.Regno = dbi.Regno", "LEFT");
		$this->sv->join("fTarifRawatInap fti", "fti.KDTarif = dbi.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIRNA fpi", "fti.KDDetail = fpi.KDDetail", "LEFT");
		// $this->sv->join("fGroupIRNA fgi", "fpi.KdGroup = fgi.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbi.KdDoc', "LEFT");
		$this->sv->join("TBLBangsal tb", 'RIGHT(fti.KdKelas,2) = tb.KdBangsal', "LEFT");
		$this->sv->where('fpi.KdGroup', '15');
		$this->sv->where('tkp.KdKategori', $ktg);
		$this->sv->where('dbi.KdDoc', $KdDoc);
		if ($tgl_awal!='') { $this->sv->where('fl.Tanggal >=', $tgl_awal); }
		if ($tgl_akhir!='') { $this->sv->where('fl.Tanggal <=', $tgl_akhir); }
		// if ($tgl_awal!='') { $this->sv->where('dbi.Tanggal >=', $tgl_awal); }
		// if ($tgl_akhir!='') { $this->sv->where('dbi.Tanggal <=', $tgl_akhir); }
		$ri = $this->sv->order_by('fl.Tanggal','DESC')->get();
		return $ri->result();
	}

	public function get_rajal($ktg='',$KdDoc='',$tgl_awal='',$tgl_akhir='')
	{
		// $this->sv->select("	fi.Regno, fi.Firstname, dbj.KdPoli, pt.NMPoli, tkp.KdKategori, tkp.NmKategori, fi.KdDocRS, fi.KdDocRawat,
		// 					dbj.Tanggal, fpj.KdGroup, fgj.NmGroup, fpj.NMDetail, ftj.KDTarif, dbj.NmTarif,
		// 					dbj.Qty, dbj.JumlahBiaya, dbj.KdDoc, fd.NmDoc");
		$this->sv->select("fi.Regno, fi.Firstname, dbj.KdPoli, pt.NMPoli, tkp.KdKategori, tkp.NmKategori, fi.KdDocRS, fi.KdDocRawat,
							dbj.Tanggal, fpj.KdGroup, fpj.NMDetail, ftj.KDTarif, dbj.NmTarif,
							dbj.Qty, dbj.JumlahBiaya, dbj.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		// $this->sv->join("FPindah fp", "fi.Regno = fp.Regno", "LEFT");
		$this->sv->join("TBLKategoriPsn tkp", "fi.Kategori = tkp.KdKategori", "LEFT");
		// $this->sv->join("HeadBilIrj hbj", "fi.Regno = hbj.Regno", "LEFT");
		// $this->sv->join("DetailBilIrj dbj", "hbj.Notran = dbj.Notran", "LEFT");
		$this->sv->join("DetailBilIrj dbj", "fi.Regno = dbj.Regno", "LEFT");
		$this->sv->join("POLItpp pt", "pt.KdPoli = dbj.KdPoli", "LEFT");
		$this->sv->join("fTarifRawatJalan ftj", "ftj.KDTarif = dbj.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIRJ fpj", "ftj.KDDetail = fpj.KDDetail", "LEFT");
		// $this->sv->join("fGroupIRJ fgj", "fpj.KdGroup = fgj.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbj.KdDoc', "LEFT");
		$this->sv->where('dbj.KdDoc', $KdDoc);
		$this->sv->where('tkp.KdKategori', $ktg);
		if ($tgl_awal!='') { $this->sv->where('dbj.Tanggal >=', $tgl_awal); }
		if ($tgl_akhir!='') { $this->sv->where('dbj.Tanggal <=', $tgl_akhir); }
		$rj = $this->sv->order_by('dbj.Tanggal','DESC')->get();
		return $rj->result();
	}

	public function get_ibs($ktg='',$KdDoc='',$tgl_awal='',$tgl_akhir='')
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, bo.KdDrOpr, fd.NmDoc,
							RIGHT(ftb.KdKelas,2) as KdBangsal, tb.NmBangsal, bo.NamaTindakan,
							tkp.KdKategori, tkp.NmKategori, bo.NoTran, bo.Tanggal, fi.KdCbayar,
							fpb.KdGroup, fgb.NmGroup, fpb.NMDetail, ftb.KDTarif, bo.JumlahBiaya");
		$this->sv->from("FPPRI fi");
		$this->sv->join("FPindah fp", "fi.Regno = fp.Regno", "LEFT");
		$this->sv->join("TBLKategoriPsn tkp", "fi.Kategori = tkp.KdKategori", "LEFT");
		$this->sv->join("BilOperasi bo", "fi.Regno = bo.Regno", "LEFT");
		$this->sv->join("fTarifBedahSentral ftb", "bo.KodeTarif = ftb.KDTarif", "LEFT");
		$this->sv->join("fPemeriksaanIBS fpb", "ftb.KDDetail = fpb.KDDetail", "LEFT");
		$this->sv->join("fGroupIBS fgb", "fpb.KdGroup = fgb.KdGroup", "LEFT");
		$this->sv->join("TBLBangsal tb", 'RIGHT(ftb.KdKelas,2) = tb.KdBangsal', "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = bo.KdDrOpr', "LEFT");
		$this->sv->where('fd.KdDoc', $KdDoc);
		$this->sv->where('tkp.KdKategori', $ktg);
		if ($tgl_awal!='') { $this->sv->where('bo.Tanggal >=', $tgl_awal); }
		if ($tgl_akhir!='') { $this->sv->where('bo.Tanggal <=', $tgl_akhir); }
		$ibs = $this->sv->order_by('bo.Tanggal','DESC')->get();
		return $ibs->result();
	}

	public function get_labtes($ktg='',$KdDoc='',$tgl_awal='',$tgl_akhir='')
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
						RIGHT(ftl.KdKelas,2) as KdBangsal, tb.NmBangsal,
						tkp.KdKategori, tkp.NmKategori, dbl.Tanggal, fpl.KdGroup, fgl.NmGroup,
						fpl.NmDetail, ftl.KDTarif, dbl.NmTarif, dbl.JumlahBiaya, dbl.KdDokter, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("FPindah fp", "fi.Regno = fp.Regno", "LEFT");
		$this->sv->join("TBLKategoriPsn tkp", "fi.Kategori = tkp.KdKategori", "LEFT");
		$this->sv->join("HeadBilLab hbl", "fi.Regno = hbl.Regno", "LEFT");
		$this->sv->join("DetailBilLab dbl", "hbl.NoTran = dbl.NoTran", "LEFT");
		$this->sv->join("fTarifLaboratorium ftl", "ftl.KDTarif = dbl.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanLab fpl", "ftl.KDDetail = fpl.KDDetail", "LEFT");
		$this->sv->join("fGroupLab fgl", "fpl.KdGroup = fgl.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbl.KdDokter', "LEFT");
		$this->sv->join("TBLBangsal tb", 'RIGHT(ftl.KdKelas,2) = tb.KdBangsal', "LEFT");
		$this->sv->where('fd.KdDoc', $KdDoc);
		$this->sv->where('tkp.KdKategori', $ktg);
		if ($tgl_awal!='') { $this->sv->where('dbl.Tanggal >=', $tgl_awal); }
		if ($tgl_akhir!='') { $this->sv->where('dbl.Tanggal <=', $tgl_akhir); }
		$lab = $this->sv->order_by('dbl.Tanggal','ASC')->get();
		return $lab->result();
	}

	public function get_fis($ktg='',$KdDoc='',$tgl_awal='',$tgl_akhir='')
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
			 				tkp.KdKategori, tkp.NmKategori, hbf.NoTran, dbf.Tanggal, hbf.KdCbayar, hbf.TotalBiaya,
							fpe.KdGroup, fge.NmGroup ,fpe.NMDetail, fte.KDTarif, dbf.NmTarif, dbf.Qty,
							RIGHT(fte.KdKelas,2) as KdBangsal, tb.NmBangsal,dbf.JumlahBiaya, dbf.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("FPindah fp", "fi.Regno = fp.Regno", "LEFT");
		$this->sv->join("TBLKategoriPsn tkp", "fi.Kategori = tkp.KdKategori", "LEFT");
		$this->sv->join("HeadBilFis hbf", "fi.Regno = hbf.Regno", "LEFT");
		$this->sv->join("DetailBilFis dbf", "hbf.NoTran = dbf.NoTran", "LEFT");
		$this->sv->join("fTarifElektromedik fte", "fte.KDTarif = dbf.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanEle fpe", "fte.KDDetail = fpe.KDDetail", "LEFT");
		$this->sv->join("fGroupEle fge", "fpe.KdGroup = fge.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbf.KdDoc', "LEFT");
		$this->sv->join("TBLBangsal tb", 'RIGHT(fte.KdKelas,2) = tb.KdBangsal', "LEFT");
		$this->sv->where('fd.KdDoc', $KdDoc);
		$this->sv->where('tkp.KdKategori', $ktg);
		if ($tgl_awal!='') { $this->sv->where('dbf.Tanggal >=', $tgl_awal); }
		if ($tgl_akhir!='') { $this->sv->where('dbf.Tanggal <=', $tgl_akhir); }
		$fi = $this->sv->order_by('dbf.Tanggal','ASC')->get();
		return $fi->result();
	}

	public function get_rad_tes($ktg='',$KdDoc='',$tgl_awal='',$tgl_akhir='')
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
							RIGHT(ftr.KdKelas,2) as KdBangsal, tb.NmBangsal,
							tkp.KdKategori, tkp.NmKategori, dbr.Tanggal, fpr.KdGroup, fgr.NmGroup, fpr.NmDetail, 
							ftr.KDTarif, dbr.NmTarif, dbr.Qty, dbr.JumlahBiaya, dbr.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("FPindah fp", "fi.Regno = fp.Regno", "LEFT");
		$this->sv->join("TBLKategoriPsn tkp", "fi.Kategori = tkp.KdKategori", "LEFT");
		$this->sv->join("HeadBilRad hbr", "fi.Regno = hbr.Regno", "LEFT");
		$this->sv->join("DetailBilRad dbr", "hbr.NoTran = dbr.NoTran", "LEFT");
		$this->sv->join("fTarifRadiologi ftr", "ftr.KDTarif = dbr.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanRad fpr", "ftr.KDDetail = fpr.KDDetail", "LEFT");
		$this->sv->join("fGroupRad fgr", "fpr.KdGroup = fgr.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbr.KdDoc', "LEFT");
		$this->sv->join("TBLBangsal tb", 'RIGHT(ftr.KdKelas,2) = tb.KdBangsal', "LEFT");
		$this->sv->where('fd.KdDoc', $KdDoc);
		$this->sv->where('tkp.KdKategori', $ktg);
		if ($tgl_awal!='') { $this->sv->where('dbr.Tanggal >=', $tgl_awal); }
		if ($tgl_akhir!='') { $this->sv->where('dbr.Tanggal <=', $tgl_akhir); }
		$rad = $this->sv->order_by('dbr.Tanggal','ASC')->get();
		return $rad->result();
	}

	public function get_rad($ktg='',$KdDoc="",$tgl_awal='',$tgl_akhir='')
	{
		$head = $detail = $fill = $hasil['rad'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbr.Notran, hbr.Tanggal, hbr.Regno, hbr.Regdate, hbr.KdBangsal, tb.NmBangsal,
						hbr.Medrec, hbr.Firstname, hbr.Shift, pt.NMPoli, hbr.KdKelas, kls.NMKelas,
						tc.NMCbayar, tj.NMJaminan, hbr.ValidUser");
		$this->sv->from("HeadBilRad hbr");
		$this->sv->join("TBLBangsal tb","hbr.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas kls","hbr.KdKelas = kls.KDKelas", "LEFT");
		$this->sv->join("POLItpp pt", "hbr.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbr.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbr.KdJaminan=tj.KDJaminan", "LEFT");
//		$this->sv->where('hbr.Regno', $reg);
		if($ktg!=''){$this->sv->where('hbr.Kategori', $ktg);}
		if($KdDoc!=''){$this->sv->where('hbr.KdDocPem', $KdDoc);}
		if($tgl_awal!=''){$this->sv->where('hbr.Tanggal>=', $tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where('hbr.Tanggal<=', $tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
        if(!empty($fill)) {
            $this->sv->select("dbr.Notran, dbr.Regno, dbr.Tanggal, fpr.KdGroup, fpr.NmDetail, ftr.KDTarif, dbr.NmTarif, dbr.Qty,
							dbr.JumlahBiaya, dbr.KdDoc, fd.NmDoc");
            $this->sv->from("DetailBilRad dbr");
            $this->sv->join("fTarifRadiologi ftr", "ftr.KDTarif = dbr.KdTarif", "LEFT");
            $this->sv->join("fPemeriksaanRad fpr", "ftr.KDDetail = fpr.KDDetail", "LEFT");
            $this->sv->join("FtDokter fd", 'fd.KdDoc = dbr.KdDoc', "LEFT");
//		$this->sv->where('dbr.Regno', $reg);
            $this->sv->where_in("dbr.Notran", $fill);
            $detail = $this->sv->get()->result();
        }
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['rad'][$n1]['Regno']   = $d->Regno;
			$hasil['rad'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['rad'][$n1]['KdTarif'] = $d->KDTarif;
			$hasil['rad'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['Notran']  = $h->Notran;
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['KdBangsal'] = $h->KdBangsal;
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['NmBangsal'] = $h->NmBangsal;
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['NMPoli'] = $h->NMPoli;
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['NMKelas'] = $h->NMKelas;
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['Firstname'] = $h->Firstname;
			}
			$hasil['rad'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil['rad']);
	}

	public function get_lab($ktg='',$kddoc='',$tgl_awal='',$tgl_akhir='')
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['lab'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbl.Notran, hbl.Tanggal, hbl.NoLab, hbl.Regno, hbl.Regdate,
			hbl.Medrec, hbl.Firstname, hbl.NmDoc, pt.NMPoli, tk.NMKelas,
			tc.NMCbayar,  hbl.Jam, hbl.Sex, hbl.Umurthn, hbl.UmurBln, hbl.UmurHari,
			hbl.KdBangsal, tb.NmBangsal, hbl.KdKelas, hbl.KdPoli, hbl.kdCbayar, hbl.Kategori, hbl.KdDoc, 
			hbl.NmDoc, hbl.KdDokter, hbl.TglSelesai,
			hbl.JamSelesai, hbl.nStatus, hbl.nJenis, hbl.TotalBiaya, hbl.ValidUser");
		$this->sv->from("HeadBilLab hbl");
		$this->sv->join("POLItpp pt", "hbl.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLBangsal tb","hbl.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = hbl.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbl.KdCbayar", "LEFT");
		if($ktg){$this->sv->where("hbl.Kategori",$ktg);}
		if($kddoc){$this->sv->where("hbl.KdDokter",$kddoc);}
		if($tgl_awal){$this->sv->where("hbl.Tanggal>=",$tgl_awal);}
		if($tgl_akhir){$this->sv->where("hbl.Tanggal<=",$tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("dbl.Notran, dbl.Tanggal, fpl.KdGroup, dbl.KdTarif, dbl.NmTarif, dbl.Sarana, dbl.Pelayanan, dbl.JumlahBiaya, dbl.nCover,
				dbl.KdDokter, fd.NmDoc");
		$this->sv->from("DetailBilLab dbl");
		$this->sv->join("fTarifLaboratorium ftl", "ftl.KDTarif = dbl.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanLab fpl", "ftl.KDDetail = fpl.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", "fd.KdDoc = dbl.KdDokter", "LEFT");
		if(!empty($fill)) $this->sv->where_in('dbl.Notran', $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['lab'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['lab'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['Notran'] = $h->Notran;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['Regno'] = $h->Regno;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['Firstname'] = $h->Firstname;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['KdPoli'] = $h->KdPoli;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['NMPoli'] = $h->NMPoli;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['NmBangsal'] = $h->NmBangsal;
			}
			$hasil['lab'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}

		return $this->cl->convToObj($hasil['lab']);
	}

	public function get_ugd($ktg='',$KdDoc='',$tgl_awal='',$tgl_akhir='')
	{
		$this->sv->select("fi.Regno, fi.Firstname, fi.KdBangsal, fi.KdKelas, tb.NmBangsal, fi.KdDocRS, fi.KdDocRawat,
							tkp.KdKategori, tkp.NmKategori, dbu.Tanggal, fpu.KdGroup, fgu.NmGroup, fpu.NmDetail,
							ftu.KDTarif, dbu.NmTarif, dbu.Qty, dbu.JumlahBiaya, dbu.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("FPindah fp", "fi.Regno = fp.Regno", "LEFT");
		$this->sv->join("TBLKategoriPsn tkp", "fi.Kategori = tkp.KdKategori", "LEFT");
		$this->sv->join("HeadBilUgd hbu", "fi.Regno = hbu.Regno", "LEFT");
		$this->sv->join("DetailBilUgd dbu", "hbu.NoTran = dbu.NoTran", "LEFT");
		$this->sv->join("fTarifRawatDarurat ftu", "ftu.KDTarif = dbu.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIGD fpu", "ftu.KDDetail = fpu.KDDetail", "LEFT");
		$this->sv->join("fGroupIGD fgu", "fpu.KdGroup = fgu.KdGroup", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbu.KdDoc', "LEFT");
		$this->sv->join("TBLBangsal tb", 'fi.KdBangsal = tb.KdBangsal', "LEFT");
		$this->sv->where('fd.KdDoc', $KdDoc);
		$this->sv->where('tkp.KdKategori', $ktg);
		if ($tgl_awal!='') { $this->sv->where('dbu.Tanggal >=', $tgl_awal); }
		if ($tgl_akhir!='') { $this->sv->where('dbu.Tanggal <=', $tgl_akhir); }
		$ugd = $this->sv->order_by('dbu.Tanggal','ASC')->get();
		return $ugd->result();
	}

}