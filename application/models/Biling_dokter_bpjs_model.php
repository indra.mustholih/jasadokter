<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_dokter_bpjs_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}
	public function get_pasien($reg="")
	{
		$this->sv->select("fi.Regno, mp.Medrec, mp.Firstname, mp.Address,
			mp.Phone, pt.KDPoli, pt.NMPoli, fi.KdBangsal, tb.NmBangsal, fi.KdKelas, tk.NMKelas, fm.KdBangsal1 as KdBangsalPindah, tbp.NmBangsal as BangsalPindah, fm.KdKelas1 as KdKelasPindah, tkp.NMKelas as KelasPindah, 
				fm.Lama as LamaPindah, tk.ByTarif, fi.Kategori, tkp.ByTarif as TarifPindah, fi.KdCbayar, tc.GroupUnit,
			mp.UmurThn, fi.Regdate, fp.Tanggal");
		$this->sv->from("FPPRI fi");
		$this->sv->join("FPindah fm","fi.Regno = fm.Regno", "LEFT");
		$this->sv->join("MasterPS mp","fi.Medrec = mp.Medrec", "LEFT");
		$this->sv->join("TBLBangsal tb","tb.KdBangsal = fi.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = fi.KdKelas", "LEFT");
		$this->sv->join("POLItpp pt","pt.KDPoli = fi.KdPoli", "LEFT");
		$this->sv->join("TBLBangsal tbp","tbp.KdBangsal = fm.KdBangsal1", "LEFT");
		$this->sv->join("TBLKelas tkp","tkp.KDKelas = fm.KdKelas1", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = fi.KdCbayar", "LEFT");
		$this->sv->join("FPulang fp","fp.Regno = fi.Regno", "LEFT");
		$this->sv->where("fi.Regno", $reg);
		$ps = $this->sv->get();
		return $ps->first();
	}
    public function  get_tarif(){
	    $this->load->model('master_data_model','mdm');
	    $data = $this->mdm->get_tarif_bpjs_irna();
	    return $data;
    }
	public function get_irj($KdDoc="",$tgl_awal='',$tgl_akhir='')
    {
        // SET ATRIBUT
        $fill = $head = $detail = $hasil['irj'] = [];
        $n0 = $n1 = 0;
        // GET HEAD TRANSAKSI
        $this->sv->select("hbj.Notran, hbj.Tanggal, hbj.Regno,  hbj.Regdate, hbj.Medrec, hbj.Firstname, hbj.KdPoli,
							fd.NmDoc, pt.NMPoli, tc.NMCbayar, tj.NMJaminan, hbj.ValidUser");
        $this->sv->from("HeadBilIrj hbj");
        $this->sv->join("POLItpp pt", "hbj.KdPoli=pt.KDPoli", "LEFT");
        $this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbj.KdCbayar", "LEFT");
        $this->sv->join("TBLJaminan tj", "hbj.KdJaminan=tj.KDJaminan", "LEFT");
        $this->sv->join("FtDokter fd", 'fd.KdDoc = hbj.KdDoc', "LEFT");
//        $this->sv->where('hbj.Regno', $reg);
        if($KdDoc != ''){$this->sv->where('hbj.KdDoc', $KdDoc);}
        if($tgl_awal != ''){$this->sv->where('hbj.Tanggal>=', $tgl_awal);}
        if($tgl_akhir != ''){$this->sv->where('hbj.Tanggal<=', $tgl_akhir);}
        $head = $this->sv->get()->result();
        foreach ($head as $h) {
            $fill[] = $h->Notran;
        }
        // GET DATA DETAIL
        if(!empty($fill)) {
            $this->sv->select("dbj.Notran, dbj.Regno, dbj.Tanggal, fpj.KdGroup, fpj.NMDetail, ftj.KDTarif, dbj.NmTarif, dbj.Qty,
                            dbj.JumlahBiaya, dbj.KdDoc, fd.NmDoc");
            $this->sv->from("DetailBilIrj dbj");
            $this->sv->join("fTarifRawatJalan ftj", "ftj.KDTarif = dbj.KdTarif", "LEFT");
            $this->sv->join("fPemeriksaanIRJ fpj", "ftj.KDDetail = fpj.KDDetail", "LEFT");
            $this->sv->join("FtDokter fd", 'fd.KdDoc = dbj.KdDoc', "LEFT");
    //		$this->sv->where('dbj.Regno', $reg);
            $this->sv->where_in('dbj.Notran',$fill);
            $detail = $this->sv->get()->result();
        }
        // FILL DATA IN ONE CONTAINER
        foreach ($detail as $d) {
            $hasil['irj'][$n1]['Tanggal'] = $d->Tanggal;
            $hasil['irj'][$n1]['KdTarif'] = $d->KDTarif;
            $hasil['irj'][$n1]['NmTarif'] = $d->NmTarif;
            $hasil['irj'][$n1]['NmDoc'] = $d->NmDoc;
            foreach ($head as $h) {
                if($h->Notran == $d->Notran) $hasil['irj'][$n1]['Medrec'] = $h->Medrec;
                if($h->Notran == $d->Notran) $hasil['irj'][$n1]['Firstname'] = $h->Firstname;
                if($h->Notran == $d->Notran) $hasil['irj'][$n1]['KdPoli'] = $h->KdPoli;
                if($h->Notran == $d->Notran) $hasil['irj'][$n1]['NMPoli'] = $h->NMPoli;
            }
            $hasil['irj'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
            $n1++;
        }
        return $this->cl->convToObj($hasil['irj']);
    }
    public function get_umum($KdDoc="",$tgl_awal='',$tgl_akhir='')
    {
        // SET ATRIBUT
        $raw = $fill = $detail= $hasil['visite'] = [];
        $arr_icu = array('01005', '01006', '01007', '15005', '15006', '15007', '15008', '15009');
        $n0=$n1=$n2=$n3=$n4=$n5=$n6=$n7=0;
        // GET HEADER BILING RAWAT INAP
        $this->sv->select("hbi.Notran, hbi.Tanggal, hbi.Regno, hbi.Regdate, hbi.Medrec, hbi.Firstname, hbi.KdBangsal, tb.NmBangsal, hbi.KdKelas,
							hbi.KdCbayar, hbi.KdJaminan, hbi.Kategori, hbi.TotalBiaya, hbi.Verifikasi, hbi.VCode, fi.KdDocRawat");
        $this->sv->from("HeadBilIrna hbi");
        $this->sv->join("FPPRI fi", "hbi.Regno = fi.Regno");
        $this->sv->join("TBLBangsal tb", "hbi.KdBangsal = tb.KdBangsal");
        if($KdDoc!=''){$this->sv->where("fi.KdDocRs",$KdDoc);}
        if($tgl_awal!=''){$this->sv->where("hbi.Tanggal>=",$tgl_awal);}
        if($tgl_akhir!=''){$this->sv->where("hbi.Tanggal<=",$tgl_akhir);}
        // if(!empty($fill)) $this->sv->where_in('Notran',$fill);
        // $this->sv->order_by('Tanggal','ASC');
        $head = $this->sv->get()->result();
        foreach ($head as $r) {
            $fill[] = $r->Notran;
        }
        // GET DETAIL BILING RAWAT INAP
        if(!empty($fill)) {
            $this->sv->select("dbi.Notran, dbi.Tanggal, fpi.KdGroup, dbi.KdTarif, dbi.NmTarif, dbi.Qty, dbi.Sarana, dbi.Pelayanan, dbi.Disc, dbi.JumlahBiaya, dbi.KdDoc, fd.NmDoc, dbi.Sts");
            $this->sv->from("DetailBilIrna dbi");
            $this->sv->join("fTarifRawatInap fti", "dbi.KdTarif = fti.KDTarif");
            $this->sv->join("fPemeriksaanIRNA fpi", "fti.KDDetail = fpi.KDDetail");
            $this->sv->join("FtDokter fd", "fd.KdDoc = dbi.KdDoc", "LEFT");
//        $this->sv->where("dbi.Regno",$reg);
            $this->sv->where_in('dbi.Notran', $fill);
            // $this->sv->order_by('dbi.Tanggal','ASC');
            $detail = $this->sv->get()->result();
        }
        // FILL DATA IN ONE CONTGAINER
        foreach ($detail as $d) {
            // VISITE DOKTER
            if(substr($d->KdTarif,0,5) == '01001' || substr($d->KdTarif,0,5) == '15001'){
                foreach ($head as $h) {
                    if($h->Notran == $d->Notran) $hasil['visite'][$n1]['Medrec'] = $h->Medrec;
                    if($h->Notran == $d->Notran) $hasil['visite'][$n1]['Firstname'] = $h->Firstname;
                    if($h->Notran == $d->Notran) $hasil['visite'][$n1]['NmBangsal'] = $h->NmBangsal;
                }
                $hasil['visite'][$n1]['Tanggal'] = $d->Tanggal;
                $hasil['visite'][$n1]['KdTarif'] = $d->KdTarif;
                $hasil['visite'][$n1]['NmTarif'] = $d->NmTarif;
                $hasil['visite'][$n1]['NmDoc'] = $d->NmDoc;
                $hasil['visite'][$n1]['JumlahBiaya'] = isset($this->get_tarif()->visite_umum_visite)?$this->get_tarif()->visite_umum_visite:0;
                $n1++;
            }

        }
        return $this->cl->convToObj($hasil['visite']);
    }

    public function get_irna($KdDoc="",$tgl_awal='',$tgl_akhir='')
    {
        // SET ATRIBUT
        $raw = $fill = $detail= $hasil['visite'] = [];
        $arr_icu = array('01005', '01006', '01007', '15005', '15006', '15007', '15008', '15009');
        $n0=$n1=$n2=$n3=$n4=$n5=$n6=$n7=0;
        // GET HEADER BILING RAWAT INAP
        $this->sv->select("hbi.Notran, hbi.Tanggal, hbi.Regno, hbi.Regdate, hbi.Medrec, hbi.Firstname, hbi.KdBangsal, tb.NmBangsal, hbi.KdKelas,
							hbi.KdCbayar, hbi.KdJaminan, hbi.Kategori, hbi.TotalBiaya, hbi.Verifikasi, hbi.VCode, fi.KdDocRawat");
        $this->sv->from("HeadBilIrna hbi");
        $this->sv->join("FPPRI fi", "hbi.Regno = fi.Regno");
        $this->sv->join("TBLBangsal tb", "hbi.KdBangsal = tb.KdBangsal");
        if($KdDoc!=''){$this->sv->where("fi.KdDocRawat",$KdDoc);}
        if($tgl_awal!=''){$this->sv->where("hbi.Tanggal>=",$tgl_awal);}
        if($tgl_akhir!=''){$this->sv->where("hbi.Tanggal<=",$tgl_akhir);}
        // if(!empty($fill)) $this->sv->where_in('Notran',$fill);
        // $this->sv->order_by('Tanggal','ASC');
        $head = $this->sv->get()->result();
        foreach ($head as $r) {
            $fill[] = $r->Notran;
        }
        // GET DETAIL BILING RAWAT INAP
        if(!empty($fill)) {
            $this->sv->select("dbi.Notran, dbi.Tanggal, fpi.KdGroup, dbi.KdTarif, dbi.NmTarif, dbi.Qty, dbi.Sarana, dbi.Pelayanan, dbi.Disc, dbi.JumlahBiaya, dbi.KdDoc, fd.NmDoc, dbi.Sts");
            $this->sv->from("DetailBilIrna dbi");
            $this->sv->join("fTarifRawatInap fti", "dbi.KdTarif = fti.KDTarif");
            $this->sv->join("fPemeriksaanIRNA fpi", "fti.KDDetail = fpi.KDDetail");
            $this->sv->join("FtDokter fd", "fd.KdDoc = dbi.KdDoc", "LEFT");
//        $this->sv->where("dbi.Regno",$reg);
            $this->sv->where_in('dbi.Notran', $fill);
            // $this->sv->order_by('dbi.Tanggal','ASC');
            $detail = $this->sv->get()->result();
        }
        // FILL DATA IN ONE CONTGAINER
        foreach ($detail as $d) {
            // VISITE DOKTER
            if(substr($d->KdTarif,0,5) == '01004' || substr($d->KdTarif,0,5) == '15004'){
                foreach ($head as $h) {
                    if($h->Notran == $d->Notran) $hasil['visite'][$n1]['Medrec'] = $h->Medrec;
                    if($h->Notran == $d->Notran) $hasil['visite'][$n1]['Firstname'] = $h->Firstname;
                    if($h->Notran == $d->Notran) $hasil['visite'][$n1]['NmBangsal'] = $h->NmBangsal;
                }
                $hasil['visite'][$n1]['Tanggal'] = $d->Tanggal;
                $hasil['visite'][$n1]['KdTarif'] = $d->KdTarif;
                $hasil['visite'][$n1]['NmTarif'] = $d->NmTarif;
                $hasil['visite'][$n1]['NmDoc'] = $d->NmDoc;
                $hasil['visite'][$n1]['JumlahBiaya'] = isset($this->get_tarif()->visite_spesialis_kosul_telpon)?$this->get_tarif()->visite_spesialis_kosul_telpon:0;
                $n1++;
            }
            if(in_array(substr($d->KdTarif,0,5),$arr_icu)){
                foreach ($head as $h) {
                    if ($h->Notran == $d->Notran) $hasil['visite'][$n1]['Medrec'] = $h->Medrec;
                    if ($h->Notran == $d->Notran) $hasil['visite'][$n1]['Firstname'] = $h->Firstname;
                    if($h->Notran == $d->Notran) $hasil['visite'][$n1]['NmBangsal'] = $h->NmBangsal;
                }
                $hasil['visite'][$n1]['Tanggal'] = $d->Tanggal;
                $hasil['visite'][$n1]['KdTarif'] = $d->KdTarif;
                $hasil['visite'][$n1]['NmTarif'] = $d->NmTarif;
                $hasil['visite'][$n1]['NmDoc'] = $d->NmDoc;
                $hasil['visite'][$n1]['JumlahBiaya'] = isset($this->get_tarif()->visite_spesialis_visite_0)?$this->get_tarif()->visite_spesialis_visite_0:0;
                $n1++;
            }
            if(substr($d->KdTarif,0,5) == '01002' || substr($d->KdTarif,0,5) == '15002'){
                foreach ($head as $h) {
                    if ($h->Notran == $d->Notran) $hasil['visite'][$n1]['Medrec'] = $h->Medrec;
                    if ($h->Notran == $d->Notran) $hasil['visite'][$n1]['Firstname'] = $h->Firstname;
                    if($h->Notran == $d->Notran) $hasil['visite'][$n1]['NmBangsal'] = $h->NmBangsal;
                    if(substr($h->KdKelas,0,1) == '2'){
                        $hasil['visite'][$n1]['JumlahBiaya'] = isset($this->get_tarif()->visite_spesialis_visite_0)?$this->get_tarif()->visite_spesialis_visite_1:0;
                    }elseif(substr($h->KdKelas,0,1) == '3'){
                        $hasil['visite'][$n1]['JumlahBiaya'] = isset($this->get_tarif()->visite_spesialis_visite_0)?$this->get_tarif()->visite_spesialis_visite_2:0;
                    }elseif(substr($h->KdKelas,0,1) == '4'){
                        $hasil['visite'][$n1]['JumlahBiaya'] = isset($this->get_tarif()->visite_spesialis_visite_0)?$this->get_tarif()->visite_spesialis_visite_3:0;
                    }
                }
                $hasil['visite'][$n1]['Tanggal'] = $d->Tanggal;
                $hasil['visite'][$n1]['KdTarif'] = $d->KdTarif;
                $hasil['visite'][$n1]['NmTarif'] = $d->NmTarif;
                $hasil['visite'][$n1]['NmDoc'] = $d->NmDoc;
                $n1++;
            }

        }
        return $this->cl->convToObj($hasil['visite']);
    }

    public function get_apotek($reg="")
	{
		// set atgribut
		$raw = $fill = $hasil['apt'] = $hasil['di'] = array();
		$hasil['du'] = $hasil['do'] = $hasil['dh'] = array();
		$n0=$n1=$n2=$n3=$n4=$n5=0;
		// GET OBAT AOPTEK
		$this->sv->select("ha.Regno, ha.KdBangsal, ha.KdKelas, ha.KdCbayar, ha.Regdate, 
							ha.BLCode, da.BLDate, da.NamaObat, da.JumlahHarga");
		$this->sv->from("HeadApotik ha");
		$this->sv->join("DetailApotik da","ha.BLCode = da.BLCode","LEFT");
		$this->sv->where("ha.Regno", $reg);
		$apt = $this->sv->get()->result();
		// FILL DATA TO CONTAINER
		foreach ($apt as $d) {
			$hasil['apt'][$n1]['Tanggal'] = $d->BLDate;
			$hasil['apt'][$n1]['KdBangsal'] = $d->KdBangsal;
			$hasil['apt'][$n1]['NamaObat'] = $d->NamaObat;
			$hasil['apt'][$n1]['JumlahHarga'] = $d->JumlahHarga;
			$n1++;
		}
		// // GET OBAT DEPO IRNA
		// $this->sv->select("hdi.BLCode, hdi.BLDate, hdi.Jam, hdi.Regno, hdi.Regdate, hdi.Medrec, hdi.Firstname,
		// 					hdi.KdBangsal, hdi.KdKelas, hdi.KdPoli, hdi.KdDoc, hdi.KdCBayar, hdi.KdJaminan, 
		// 					ddi.KodeObat, ddi.NamaObat, ddi.Harga, ddi.Qty, ddi.JumlahHarga");
		// $this->sv->from("dbRSDEPO.dbo.HeadDepoIrna hdi");
		// $this->sv->join("dbRSDEPO.dbo.DetailDepoIrna ddi", "hdi.BLCode = ddi.BLCode","LEFT");
		// $this->sv->where("hdi.Regno", $reg);
		// $di = $this->sv->get()->result();
		// // FILL DATA TO CONTAINER
		// foreach ($di as $d) {
		// 	$hasil['di'][$n2]['Tanggal'] = $d->BLDate;
		// 	$hasil['di'][$n2]['KdBangsal'] = $d->KdBangsal;
		// 	$hasil['di'][$n2]['NamaObat'] = $d->NamaObat;
		// 	$hasil['di'][$n2]['JumlahHarga'] = $d->JumlahHarga;
		// 	$n2++;
		// }
		// // GET OBAT DEPO IGD
		// $this->sv->select("hdu.BLCode, hdu.BLDate, hdu.Jam, hdu.Regno, hdu.Regdate, hdu.Medrec, hdu.Firstname,
		// 					hdu.KdBangsal, hdu.KdKelas, hdu.KdPoli, hdu.KdDoc, hdu.KdCBayar, hdu.KdJaminan, 
		// 					ddu.KodeObat, ddu.NamaObat, ddu.Harga, ddu.Qty, ddu.JumlahHarga");
		// $this->sv->from("dbRSDEPO.dbo.HeadDepoIGD hdu");
		// $this->sv->join("dbRSDEPO.dbo.DetailDepoIGD ddu", "hdu.BLCode = ddu.BLCode","LEFT");
		// $this->sv->where("hdu.Regno", $reg);
		// $du = $this->sv->get()->result();
		// // FILL DATA TO CONTAINER
		// foreach ($du as $d) {
		// 	$hasil['du'][$n3]['Tanggal'] = $d->BLDate;
		// 	$hasil['du'][$n3]['KdBangsal'] = $d->KdBangsal;
		// 	$hasil['du'][$n3]['NamaObat'] = $d->NamaObat;
		// 	$hasil['du'][$n3]['JumlahHarga'] = $d->JumlahHarga;
		// 	$n3++;
		// }
		// // GET OBAT DEPO OK
		// $this->sv->select("hdo.BLCode, hdo.BLDate, hdo.Jam, hdo.Regno, hdo.Regdate, hdo.Medrec, hdo.Firstname,
		// 					hdo.KdBangsal, hdo.KdKelas, hdo.KdPoli, hdo.KdDoc, hdo.KdCBayar, hdo.KdJaminan, 
		// 					ddo.KodeObat, ddo.NamaObat, ddo.Harga, ddo.Qty, ddo.JumlahHarga");
		// $this->sv->from("dbRSDEPO.dbo.HeadDepoOK hdo");
		// $this->sv->join("dbRSDEPO.dbo.DetailDepoOK ddo ", "hdo.BLCode = ddo.BLCode","LEFT");
		// $this->sv->where("hdo.Regno", $reg);
		// $do = $this->sv->get()->result();
		// // FILL DATA TO CONTAINER
		// foreach ($do as $d) {
		// 	$hasil['do'][$n4]['Tanggal'] = $d->BLDate;
		// 	$hasil['do'][$n4]['KdBangsal'] = $d->KdBangsal;
		// 	$hasil['do'][$n4]['NamaObat'] = $d->NamaObat;
		// 	$hasil['do'][$n4]['JumlahHarga'] = $d->JumlahHarga;
		// 	$n4++;
		// }
		// GET OBAT DEPO HD
		// $this->sv->select("hdh.BLCode, hdh.BLDate, hdh.Jam, hdh.Regno, hdh.Regdate, hdh.Medrec, hdh.Firstname,
		// 				hdh.KdBangsal, hdh.KdKelas, hdh.KdPoli, hdh.KdDoc, hdh.KdCBayar, hdh.KdJaminan, 
		// 				ddh.KodeObat, ddh.NamaObat, ddh.Harga, ddh.Qty, ddh.JumlahHarga");
		// $this->sv->from("dbRSDEPO.dbo.HeadDepoHemo hdh");
		// $this->sv->join("dbRSDEPO.dbo.DetailDepoHemo ddh", "hdh.BLCode = ddh.BLCode","LEFT");
		// $this->sv->where("hdh.Regno", $reg);
		// $do = $this->sv->get()->result();	
		return $this->cl->convToObj($hasil);
	}

	/* public function get_aku($reg="")
	{
		$this->sv->select("fi.Regno, fi.Medrec, fi.Firstname, fi.KdBangsal, fi.KdKelas, fi.KdDocRS, fi.KdDocRawat,
			hba.NoTran, dba.Tanggal, hba.KdCbayar, hba.TotalBiaya, dba.KdTarif, dba.NmTarif, dba.Qty,
			dba.JumlahBiaya, dba.KdDoc, fd.NmDoc");
		$this->sv->from("FPPRI fi");
		$this->sv->join("HeadBilAku hba", "fi.Regno = hba.Regno", "LEFT");
		$this->sv->join("DetailBilAku dba", "hba.NoTran = dba.NoTran", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dba.KdDoc', "LEFT");
		$this->sv->where('fi.Regno', $reg);
		$ak = $this->sv->order_by('dba.Tanggal', 'ASC')->get();
		return $ak->result();
	}*/

	public function get_ibs($KdDoc="")
	{
		// SET ATRIBUT
		$head = $detail = $hasil['opr'] = null;
		$n0 = $n1 = 0;
		// GET DETAIL TRANSAKSI
		$this->sv->select("Notran, Regno, Tanggal, Medrec, Regdate, Firstname, KdBangsal, KdKelas, KdCbayar, KdJaminan,
					Kategori, KdDrOpr, KdDrRawat, KdAsDrOpr, KdDrAnestesi, KdDrPenata, KdDrAnak, KdDrPendamping, KdBidan,
					KodeTarif, NamaTindakan, DetailCode, DetailName, ByRrawat, ByAlatRS, ByDrOpr, ByAsDrOpr, ByDrAnestesi,
					ByDrPenata, ByDrAnak, ByDrPendamping, ByBidan, ByAsBidan, BySewaAlat, ByRroom, ByObat, JumlahBiaya,
					CitoDrOperator, CitoAsDrOperator, CitoDrAnestesi, CitoAsDrAnestesi, CitoDrAnak, CitoDrPendamping");
		$this->sv->from("BilOperasi");
//		$this->sv->where("Regno",$reg);
		$detail = $this->sv->get()->result();
		// FILL IH ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['opr'][$n1]['Notran'] = $d->Notran;
			$hasil['opr'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['opr'][$n1]['NamaTindakan'] = $d->NamaTindakan;
			foreach ($head as $h) {
				if($h->KdBangsal == $d->KdBangsal){
					$hasil['opr'][$n1]['KdBangsal'] = $h->KdBangsal;
					$hasil['opr'][$n1]['Firstname'] = $h->Firstname;
					$hasil['opr'][$n1]['NmBangsal'] = $h->NmBangsal;
				}
			}
			$hasil['opr'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$hasil['dtl'][$n1]['Notran'] = $d->Notran;
			$hasil['dtl'][$n1]['Biaya_Rawat'] = $d->ByRrawat;
			$hasil['dtl'][$n1]['Biaya_Alat_RS'] = $d->ByAlatRS;
			$hasil['dtl'][$n1]['Biaya_Dr_Operasi'] = $d->ByDrOpr;
			$hasil['dtl'][$n1]['Biaya_Asist_Dr_Operasi'] = $d->ByAsDrOpr;
			$hasil['dtl'][$n1]['Biaya_Dr_Anestesi'] = $d->ByDrAnestesi;
			$hasil['dtl'][$n1]['Biaya_Dr_Penata'] = $d->ByDrPenata;
			$hasil['dtl'][$n1]['Biaya_Dr_Anak'] = $d->ByDrAnak;
			$hasil['dtl'][$n1]['Biaya_Bidan'] = $d->ByBidan;
			$hasil['dtl'][$n1]['Biaya_Asist_Bidan'] = $d->ByAsBidan;
			$hasil['dtl'][$n1]['Biaya_Sewa_Alat'] = $d->BySewaAlat;
			$hasil['dtl'][$n1]['Biaya_Room'] = $d->ByRroom;
			$hasil['dtl'][$n1]['Biaya_Obat'] = $d->ByObat;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	}

	public function get_fis($KdDoc="",$tgl_awal='',$tgl_akhir='')
	{
		// SET ATRIBUT
		$fill = $head = $detail = $hasil['fis'] = [];
		$n0 = $n1 = 0;
		// GET HEAD TRANSAKSI
		$this->sv->select("hbf.Notran, hbf.Regno, hbf.Tanggal, hbf.Jam, hbf.Medrec, hbf.Regdate, hbf.Firstname, hbf.Sex,
				hbf.umurThn, hbf.UmurBln, hbf.UmurHari, hbf.KdBangsal, hbf.KdKelas, hbf.KdPoli, hbf.KdDoc, hbf.KdCbayar, hbf.KdJaminan,
				hbf.Kategori, hbf.TotalBiaya, pt.NMPoli, tk.NMKelas, tc.NMCbayar, tj.NMJaminan, hbf.ValidUser");
		$this->sv->from("HeadBilFis hbf");
		$this->sv->join("POLItpp pt", "hbf.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = hbf.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbf.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbf.KdJaminan=tj.KDJaminan", "LEFT");
		if($KdDoc!=''){$this->sv->where('hbf.KdDoc', $KdDoc);}
		if($tgl_awal!=''){$this->sv->where('hbf.Tanggal>=', $tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where('hbf.Tanggal<=', $tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
        // GET DETAIL TRANSAKSI
        if(!empty($fill)) {
            $this->sv->select(" dbf.Notran, dbf.Regno, dbf.Tanggal, fpe.KdGroup, fpe.NMDetail, fte.KDTarif, dbf.NmTarif, dbf.Qty,
							dbf.JumlahBiaya, dbf.KdDoc, fd.NmDoc");
            $this->sv->from("DetailBilFis dbf");
            $this->sv->join("fTarifElektromedik fte", "fte.KDTarif = dbf.KdTarif", "LEFT");
            $this->sv->join("fPemeriksaanEle fpe", "fte.KDDetail = fpe.KDDetail", "LEFT");
            $this->sv->join("FtDokter fd", 'fd.KdDoc = dbf.KdDoc', "LEFT");
            $this->sv->where_in('dbf.Notran', $fill);
//		$this->sv->where('dbf.Regno', $reg);
            $detail = $this->sv->get()->result();
        }
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['fis'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['fis'][$n1]['KdTarif'] = $d->KDTarif;
			$hasil['fis'][$n1]['NmTarif'] = $d->NmTarif;
			$hasil['fis'][$n1]['NmDoc'] = $d->NmDoc;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['fis'][$n1]['KdBangsal'] = $h->KdBangsal;
				if($h->Notran == $d->Notran) $hasil['fis'][$n1]['Firstname'] = $h->Firstname;
			}
			$hasil['fis'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	}

	public function get_hd($KdDoc="",$tgl_awal='',$tgl_akhir='')
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['hd'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbr.Notran, hbr.Tanggal, hbr.Regno, hbr.Regdate, hbr.KdBangsal, tb.NmBangsal,
						hbr.Medrec, hbr.Firstname, hbr.Shift, pt.NMPoli,
						tc.NMCbayar, tj.NMJaminan, hbr.ValidUser");
		$this->sv->from("HeadBilHD hbr");
		$this->sv->join("TBLBangsal tb","hbr.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("POLItpp pt", "hbr.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbr.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbr.KdJaminan=tj.KDJaminan", "LEFT");
		if($KdDoc!=''){$this->sv->where('hbr.KdDoc', $KdDoc);}
		if($tgl_awal!=''){$this->sv->where('hbr.Tanggal>=', $tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where('hbr.Tanggal<=', $tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
        if(!empty($fill)) {
            // GET DETAIL TRANSAKSI
            $this->sv->select("dbr.Notran, dbr.Regno, dbr.Tanggal, fpr.KdGroup, fpr.NmDetail, ftr.KDTarif, dbr.NmTarif, dbr.Qty,
							dbr.JumlahBiaya, dbr.KdDoc, fd.NmDoc");
            $this->sv->from("DetailBilHD dbr");
            $this->sv->join("fTarifHemodialisa ftr", "ftr.KDTarif = dbr.KdTarif", "LEFT");
            $this->sv->join("fPemeriksaanHemo fpr", "ftr.KDDetail = fpr.KDDetail", "LEFT");
            $this->sv->join("FtDokter fd", 'fd.KdDoc = dbr.KdDoc', "LEFT");
            $this->sv->where_in("dbr.Notran", $fill);
//		$this->sv->where('dbr.Regno', $reg);
            $detail = $this->sv->get()->result();
        }
        // FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['hd'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['hd'][$n1]['KdTarif'] = $d->KDTarif;
			$hasil['hd'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['hd'][$n1]['KdBangsal'] = $h->KdBangsal;
				if($h->Notran == $d->Notran) $hasil['hd'][$n1]['NmBangsal'] = $h->NmBangsal;
				if($h->Notran == $d->Notran) $hasil['hd'][$n1]['Firstname'] = $h->Firstname;
			}
			$hasil['hd'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil['hd']);
	} 

	public function get_lab($KdDoc="",$tgl_awal='',$tgl_akhir='')
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['lab'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbl.Notran, hbl.Tanggal, hbl.NoLab, hbl.Regno, hbl.Regdate,
			hbl.Medrec, hbl.Firstname, hbl.NmDoc, pt.NMPoli, tk.NMKelas, tb.NmBangsal,
			tc.NMCbayar,  hbl.Jam, hbl.Sex, hbl.Umurthn, hbl.UmurBln, hbl.UmurHari,
			hbl.KdBangsal, hbl.KdKelas, hbl.KdPoli, hbl.kdCbayar, hbl.Kategori, hbl.KdDoc, hbl.NmDoc, hbl.KdDokter, hbl.TglSelesai,
			hbl.JamSelesai, hbl.nStatus, hbl.nJenis, hbl.TotalBiaya, hbl.ValidUser");
		$this->sv->from("HeadBilLab hbl");
		$this->sv->join("POLItpp pt", "hbl.KdPoli=pt.KDPoli", "LEFT");
        $this->sv->join("TBLBangsal tb","hbl.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = hbl.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbl.KdCbayar", "LEFT");
//		$this->sv->where("hbl.Regno",$reg);
		if($KdDoc!=''){$this->sv->where("hbl.KdDoc",$KdDoc);}
		if($tgl_awal!=''){$this->sv->where("hbl.Tanggal>=",$tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where("hbl.Tanggal<=",$tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
        if(!empty($fill)) {
            // GET DETAIL TRANSAKSI
            $this->sv->select("dbl.Notran, dbl.Tanggal, fpl.KdGroup, dbl.KdTarif, dbl.NmTarif, dbl.Sarana, dbl.Pelayanan, dbl.JumlahBiaya, dbl.nCover,
				dbl.KdDokter, fd.NmDoc");
            $this->sv->from("DetailBilLab dbl");
            $this->sv->join("fTarifLaboratorium ftl", "ftl.KDTarif = dbl.KdTarif", "LEFT");
            $this->sv->join("fPemeriksaanLab fpl", "ftl.KDDetail = fpl.KDDetail", "LEFT");
            $this->sv->join("FtDokter fd", "fd.KdDoc = dbl.KdDokter", "LEFT");
            $this->sv->where_in('dbl.Notran', $fill);
            // $this->sv->where("dbl.Regno",$reg);
            $detail = $this->sv->get()->result();
        }
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['lab'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['lab'][$n1]['KdTarif'] = $d->KdTarif;
			$hasil['lab'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['KdBangsal'] = $h->KdBangsal;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['NmBangsal'] = $h->NmBangsal;
				if($h->Notran == $d->Notran) $hasil['lab'][$n1]['Firstname'] = $h->Firstname;
			}
			$hasil['lab'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}

		return $this->cl->convToObj($hasil['lab']);
	}

	public function get_rad($KdDoc="",$tgl_awal='',$tgl_akhir='')
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['rad'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbr.Notran, hbr.Tanggal, hbr.Regno, hbr.Regdate, hbr.KdBangsal, tb.NmBangsal,
						hbr.Medrec, hbr.Firstname, hbr.Shift, pt.NMPoli,
						tc.NMCbayar, tj.NMJaminan, hbr.ValidUser");
		$this->sv->from("HeadBilRad hbr");
		$this->sv->join("TBLBangsal tb","hbr.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("POLItpp pt", "hbr.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbr.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbr.KdJaminan=tj.KDJaminan", "LEFT");
//		$this->sv->where('hbr.Regno', $reg);
		if($KdDoc!=''){$this->sv->where('hbr.KdDoc', $KdDoc);}
		if($tgl_awal!=''){$this->sv->where('hbr.Tanggal>=', $tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where('hbr.Tanggal<=', $tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
        if(!empty($fill)) {
            $this->sv->select("dbr.Notran, dbr.Regno, dbr.Tanggal, fpr.KdGroup, fpr.NmDetail, ftr.KDTarif, dbr.NmTarif, dbr.Qty,
							dbr.JumlahBiaya, dbr.KdDoc, fd.NmDoc");
            $this->sv->from("DetailBilRad dbr");
            $this->sv->join("fTarifRadiologi ftr", "ftr.KDTarif = dbr.KdTarif", "LEFT");
            $this->sv->join("fPemeriksaanRad fpr", "ftr.KDDetail = fpr.KDDetail", "LEFT");
            $this->sv->join("FtDokter fd", 'fd.KdDoc = dbr.KdDoc', "LEFT");
//		$this->sv->where('dbr.Regno', $reg);
            $this->sv->where_in("dbr.Notran", $fill);
            $detail = $this->sv->get()->result();
        }
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['rad'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['rad'][$n1]['KdTarif'] = $d->KDTarif;
			$hasil['rad'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['KdBangsal'] = $h->KdBangsal;
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['NmBangsal'] = $h->NmBangsal;
				if($h->Notran == $d->Notran) $hasil['rad'][$n1]['Firstname'] = $h->Firstname;
			}
			$hasil['rad'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil['rad']);
	}

	public function get_ugd($KdDoc="",$tgl_awal='',$tgl_akhir='')
	{
		// SET ATRIBUT
		$head = $detail = $fill = $hasil['ugd'] = [];
		$n0=$n1=0;
		// GET HEAD TRANSAKSI
		$this->sv->select("hbu.Notran, hbu.Tanggal, hbu.Regno, hbu.Regdate, hbu.Medrec,
						hbu.Firstname, tc.NMCbayar, tj.NMJaminan, hbu.ValidUser");
		$this->sv->from("HeadBilUgd hbu");
		$this->sv->join("POLItpp pt", "hbu.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbu.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbu.KdJaminan=tj.KDJaminan", "LEFT");
//		$this->sv->where("hbu.Regno",$reg);
		if($KdDoc!=''){$this->sv->where("hbu.KdDoc",$KdDoc);}
		if($tgl_awal!=''){$this->sv->where("hbu.Tanggal>=",$tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where("hbu.Tanggal<=",$tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
        if(!empty($fill)) {
            $this->sv->select("dbu.Notran,dbu.Regno, dbu.Tanggal, fpu.KdGroup, fpu.NmDetail, 
							ftu.KDTarif, dbu.NmTarif, dbu.Qty, dbu.JumlahBiaya, dbu.KdDoc, fd.NmDoc");
            $this->sv->from("DetailBilUgd dbu");
            $this->sv->join("fTarifRawatDarurat ftu", "ftu.KDTarif = dbu.KdTarif", "LEFT");
            $this->sv->join("fPemeriksaanIGD fpu", "ftu.KDDetail = fpu.KDDetail", "LEFT");
            $this->sv->join("FtDokter fd", 'fd.KdDoc = dbu.KdDoc', "LEFT");
//		$this->sv->where('dbu.Regno', $reg);
            $this->sv->where_in('dbu.Notran', $fill);
            $detail = $this->sv->get()->result();
        }
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
            foreach ($head as $h) {
                if($h->Notran == $d->Notran) $hasil['ugd'][$n1]['Firstname'] = $h->Firstname;
            }
			$hasil['ugd'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['ugd'][$n1]['KdTarif'] = $d->KDTarif;
			$hasil['ugd'][$n1]['NmTarif'] = $d->NmTarif;
			$hasil['ugd'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil['ugd']);

		// return $ugd->result();
	}

	public function get_umuk($reg='')
	{
		$this->sv->where('Regno', $reg);
		$umuk = $this->sv->get('UangMuka');
		return $umuk->row();
	}
}

/* End of file biling_model.php */
/* Location: ./application/models/biling_model.php */