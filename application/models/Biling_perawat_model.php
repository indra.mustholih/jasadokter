<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_perawat_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}

	public function get_irna($ktg='',$tgl_awal='',$tgl_akhir='')
	{
		// SET ATRIBUT
		$raw = $fill = $hasil['adm'] = $hasil['visite'] = $hasil['askep'] = [];
		$hasil['jaskep'] = $hasil['adpj'] = $hasil['bak'] = $hasil['hmd'] = [];
		$n0=$n1=$n2=$n3=$n4=$n5=$n6=$n7=0;
		// GET HEADER BILING RAWAT INAP
		// $this->sv->select("hbi.Notran, hbi.Tanggal, hbi.Regno, hbi.Regdate, hbi.Medrec, hbi.Firstname,
		// 					hbi.KdBangsal, hbi.KdKelas, hbi.KdCbayar, hbi.KdJaminan, tb.NmBangsal,
		// 					hbi.Kategori, hbi.TotalBiaya, hbi.Verifikasi, hbi.VCode");
		// $this->sv->from("HeadBilIrna hbi");
		// $this->sv->join("TBLBangsal tb","tb.KdBangsal = hbi.KdBangsal", "LEFT");
		// $this->sv->join("TBLKelas tk","tk.KDKelas = hbi.KdKelas", "LEFT");
		// $this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbi.KdCbayar", "LEFT");
		// $this->sv->join("TBLJaminan tj", "hbi.KdJaminan=tj.KDJaminan", "LEFT");
		// // $this->sv->where("Regno",$reg);
		// if($tgl_awal){$this->sv->where('hbi.Tanggal >=', $tgl_awal);}
		// if($tgl_akhir){$this->sv->where('hbi.Tanggal <=', $tgl_akhir);}
		// if(!empty($fill)) $this->sv->where_in('Notran',$fill);
		// // $this->sv->order_by('Tanggal','ASC');
		// $head = $this->sv->get()->result();
		// foreach ($head as $r) {
		// 	$fill[] = $r->Notran;
		// }
		// // GET DETAIL BILING RAWAT INAP
		// $this->sv->select("dbi.Notran, dbi.Tanggal, fpi.KdGroup, dbi.KdTarif, dbi.NmTarif, dbi.Qty, dbi.Sarana, dbi.Pelayanan, dbi.Disc, dbi.JumlahBiaya, dbi.KdDoc, fd.NmDoc, dbi.Sts");
		// $this->sv->from("DetailBilIrna dbi");
		// $this->sv->join("fTarifRawatInap fti", "dbi.KdTarif = fti.KDTarif");
		// $this->sv->join("fPemeriksaanIRNA fpi", "fti.KDDetail = fpi.KDDetail");
		// $this->sv->join("FtDokter fd", "fd.KdDoc = dbi.KdDoc", "LEFT");
		// // $this->sv->where("dbi.Regno",$reg);
		// if(!empty($fill)) $this->sv->where_in('dbi.Notran',$fill);
		// $this->sv->order_by('dbi.Tanggal','ASC');
		// $detail = $this->sv->get()->result();
		$this->sv->select("hbi.Notran, hbi.Tanggal, hbi.Regno, hbi.Regdate, hbi.Medrec, hbi.Firstname,
		 					hbi.KdBangsal, hbi.KdKelas, hbi.KdCbayar, hbi.KdJaminan, tb.NmBangsal,
							hbi.Kategori, hbi.TotalBiaya, dbi.Tanggal, fpi.KdGroup, fgi.NmGroup, fpi.NmDetail,
							RIGHT(fti.KdKelas,2)as KdBangsal, fti.KdKelas as KdKelas, fti.KDTarif, 
							dbi.NmTarif, dbi.Qty, dbi.JumlahBiaya, dbi.KdDoc, fd.NmDoc");
		$this->sv->from("HeadBilIrna hbi");
		$this->sv->join("DetailBilIrna dbi", "hbi.NoTran = dbi.NoTran", "LEFT");
		$this->sv->join("fTarifRawatInap fti", "fti.KDTarif = dbi.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanIRNA fpi", "fti.KDDetail = fpi.KDDetail", "LEFT");
		$this->sv->join("fGroupIRNA fgi", "fpi.KdGroup = fgi.KdGroup", "LEFT");
		$this->sv->join("TBLBangsal tb","tb.KdBangsal = hbi.KdBangsal", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbi.KdDoc', "LEFT");
		if($tgl_awal!=''){$this->sv->where('hbi.Kategori', $ktg);}
		if($tgl_awal!=''){$this->sv->where('hbi.Tanggal >=', $tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where('hbi.Tanggal <=', $tgl_akhir);}
		// $this->sv->where('fi.Regno', $reg);
		$detail = $this->sv->order_by('dbi.Tanggal','ASC')->get()->result();
		// FILL DATA IN ONE CONTGAINER
		foreach ($detail as $d) {
			// JASA KEPAWATAN
			if($d->KdGroup == '02' || $d->KdGroup == '03' || $d->KdGroup == '06' || $d->KdGroup == '20'){
				$hasil['jaskep'][$n7]['Notran'] = $d->Notran;
				$hasil['jaskep'][$n7]['Firstname'] = $d->Firstname;
				$hasil['jaskep'][$n7]['KdBangsal'] = $d->KdBangsal;
				$hasil['jaskep'][$n7]['NmBangsal'] = $d->NmBangsal;
				$hasil['jaskep'][$n7]['Tanggal'] = $d->Tanggal;
				$hasil['jaskep'][$n7]['NmTarif'] = $d->NmTarif;
				$hasil['jaskep'][$n7]['NmDoc'] = $d->NmDoc;
				$hasil['jaskep'][$n7]['JumlahBiaya'] = $d->JumlahBiaya;
				$n7++;
			}
		}
		return $this->cl->convToObj($hasil);
	}
}