<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_ok_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}

	public function get_ibs($tgl_awal='',$tgl_akhir='')
	{
		// SET ATRIBUT
		$fill = $head = $detail = $hasil['opr'] = [];
		$n0 = $n1 = 0;
		// GET HEADER TRNSAKSI 
		$this->sv->select("bo.Notran, bo.Tanggal, bo.Regno, bo.Regdate, bo.Medrec, bo.Firstname,
							tb.KdBangsal, tb.NmBangsal, tk.NMKelas, tc.NMCbayar, tj.NMJaminan");
		$this->sv->from("BilOperasi bo");
		$this->sv->join("TBLBangsal tb","tb.KdBangsal = bo.KdBangsal", "LEFT");
		$this->sv->join("TBLKelas tk","tk.KDKelas = bo.KdKelas", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = bo.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "bo.KdJaminan=tj.KDJaminan", "LEFT");
		if ($tgl_awal!='') { $this->sv->where('bo.Tanggal >= ', $tgl_awal);}
		if ($tgl_akhir!='') { $this->sv->where('bo.Tanggal <= ', $tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("Notran, Regno, Tanggal, Medrec, Regdate, Firstname, KdBangsal, KdKelas, KdCbayar, KdJaminan,
					Kategori, KdDrOpr, KdDrRawat, KdAsDrOpr, KdDrAnestesi, KdDrPenata, KdDrAnak, KdDrPendamping, KdBidan,
					KodeTarif, NamaTindakan, DetailCode, DetailName, ByRrawat, ByAlatRS, ByDrOpr, ByAsDrOpr, ByDrAnestesi,
					ByDrPenata, ByDrAnak, ByDrPendamping, ByBidan, ByAsBidan, BySewaAlat, ByRroom, ByObat, JumlahBiaya,
					CitoDrOperator, CitoAsDrOperator, CitoDrAnestesi, CitoAsDrAnestesi, CitoDrAnak, CitoDrPendamping");
		$this->sv->from("BilOperasi");
		if(!empty($fill)) $this->sv->where_in("Notran",$fill);
		$detail = $this->sv->get()->result();
		// FILL IH ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['opr'][$n1]['Notran'] = $d->Notran;
			$hasil['opr'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['opr'][$n1]['NamaTindakan'] = $d->NamaTindakan;
			foreach ($head as $h) {
				if($h->KdBangsal == $d->KdBangsal){
					$hasil['opr'][$n1]['KdBangsal'] = $h->KdBangsal;
					$hasil['opr'][$n1]['Firstname'] = $h->Firstname;
					$hasil['opr'][$n1]['NmBangsal'] = $h->NmBangsal;
				}
			}
			$hasil['opr'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$hasil['dtl'][$n1]['Notran'] = $d->Notran;
			$hasil['dtl'][$n1]['Biaya_Rawat'] = $d->ByRrawat;
			$hasil['dtl'][$n1]['Biaya_Alat_RS'] = $d->ByAlatRS;
			$hasil['dtl'][$n1]['Biaya_Dr_Operasi'] = $d->ByDrOpr;
			$hasil['dtl'][$n1]['Biaya_Asist_Dr_Operasi'] = $d->ByAsDrOpr;
			$hasil['dtl'][$n1]['Biaya_Dr_Anestesi'] = $d->ByDrAnestesi;
			$hasil['dtl'][$n1]['Biaya_Dr_Penata'] = $d->ByDrPenata;
			$hasil['dtl'][$n1]['Biaya_Dr_Anak'] = $d->ByDrAnak;
			$hasil['dtl'][$n1]['Biaya_Bidan'] = $d->ByBidan;
			$hasil['dtl'][$n1]['Biaya_Asist_Bidan'] = $d->ByAsBidan;
			$hasil['dtl'][$n1]['Biaya_Sewa_Alat'] = $d->BySewaAlat;
			$hasil['dtl'][$n1]['Biaya_Room'] = $d->ByRroom;
			$hasil['dtl'][$n1]['Biaya_Obat'] = $d->ByObat;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	}
}