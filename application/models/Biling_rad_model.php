<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_rad_model extends CI_Model {

	protected $sv;
	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->sv = $this->load->database('server', true);
		$this->load->library('cl');
	}

	public function get_rad($tgl_awal='',$tgl_akhir='')
	{
		//SET ATRIBUT
		$head = $detail = $fill = $hasil['rad'] = [];
		$n0=$n1=0;
		// GET HEADER TRANSAKSI
		$this->sv->select("hbr.Notran, hbr.Tanggal, hbr.Regno, hbr.Regdate, hbr.KdBangsal,
						hbr.Medrec, hbr.Firstname, hbr.Shift, pt.NMPoli, tb.NmBangsal,
						tc.NMCbayar, tj.NMJaminan, hbr.ValidUser");
		$this->sv->from("HeadBilRad hbr");
		$this->sv->join("TBLBangsal tb","hbr.KdBangsal = tb.KdBangsal", "LEFT");
		$this->sv->join("POLItpp pt", "hbr.KdPoli=pt.KDPoli", "LEFT");
		$this->sv->join("TBLcarabayar tc","tc.KDCbayar = hbr.KdCbayar", "LEFT");
		$this->sv->join("TBLJaminan tj", "hbr.KdJaminan=tj.KDJaminan", "LEFT");
		if($tgl_awal!=''){$this->sv->where('hbr.Tanggal', $tgl_awal);}
		if($tgl_akhir!=''){$this->sv->where('hbr.Tanggal', $tgl_akhir);}
		$head = $this->sv->get()->result();
		foreach ($head as $h) {
			$fill[] = $h->Notran;
		}
		// GET DETAIL TRANSAKSI
		$this->sv->select("dbr.Notran, dbr.Regno, dbr.Tanggal, fpr.KdGroup, fpr.NmDetail, ftr.KDTarif, dbr.NmTarif, dbr.Qty,
							dbr.JumlahBiaya, dbr.KdDoc, fd.NmDoc");
		$this->sv->from("DetailBilRad dbr");
		$this->sv->join("fTarifRadiologi ftr", "ftr.KDTarif = dbr.KdTarif", "LEFT");
		$this->sv->join("fPemeriksaanRad fpr", "ftr.KDDetail = fpr.KDDetail", "LEFT");
		$this->sv->join("FtDokter fd", 'fd.KdDoc = dbr.KdDoc', "LEFT");
		if(!empty($fill)) $this->sv->where_in("dbr.Notran", $fill);
		$detail = $this->sv->get()->result();
		// FILL DATA IN ONE CONTAINER
		foreach ($detail as $d) {
			$hasil['rad'][$n1]['Notran'] = $d->Notran;
			$hasil['rad'][$n1]['Tanggal'] = $d->Tanggal;
			$hasil['rad'][$n1]['NmTarif'] = $d->NmTarif;
			foreach ($head as $h) {
				if($h->Notran == $d->Notran) {
					$hasil['rad'][$n1]['KdBangsal'] = $h->KdBangsal;
					$hasil['rad'][$n1]['NmBangsal'] = $h->NmBangsal;
					$hasil['rad'][$n1]['NMPoli'] = $h->NMPoli;
					$hasil['rad'][$n1]['Firstname'] = $h->Firstname;
				}
			}
			$hasil['rad'][$n1]['JumlahBiaya'] = $d->JumlahBiaya;
			$n1++;
		}
		return $this->cl->convToObj($hasil);
	}
}