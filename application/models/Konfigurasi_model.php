<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigurasi_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		$this->load->database();
	}

	// listing 1 row
	public function listing(){
		$query = $this->db->get('konfigurasi');
		return $query->row();
	}

	// edit konfigurasi
	public function edit($data){
		$this->db->where('id_konf',$data['id_konf']);
		$this->db->update('konfigurasi',$data);
	}


}

/* End of file Konfigurasi_model.php */
/* Location: ./application/models/Konfigurasi_model.php */