<?php



defined('BASEPATH') OR exit('No direct script access allowed');

class KomponenBiaya_model extends CI_Model {

  protected $sv;

  // variable utk datatable serverside
	var $tbl = 'komp_biaya';
	var $column_order = array(null,'nama_komp');
  var $column_search = array('nama_komp');
	var $order = array('nama_komp'=> 'nama_komp');
  

  //private variable untuk  memanggl nama table dan id nya
  private $table = 'komp_biaya';
  private $id = 'komp_biayaid';

	public function __construct()
	{
		parent::__construct();
    $this->sv = $this->load->database('server', true);
    $this->load->library('cl');
  }
  public function listing()
  {
    $this->sv->select("case when komp_biaya = 1 then 'Rawat Jalan' when komp_biaya = 2 then 'Rawat Inap' else '-' end as komp_biaya, nama_komp, komp_biayaid ");
    $this->sv->from('komp_biaya');
    $this->sv->order_by('nama_komp','ASC');
		$query = $this->sv->get();
    return $query->result_array();

  }
  // function tambah
  public function add($data){
    $this->sv->insert($this->tbl,$data);
  }

  //function edit by id
  public function edit($komp_biayaid){
    $this->sv->select("*");
    $this->sv->from($this->tbl);
    $this->sv->where('komp_biayaid',$komp_biayaid);
    $query = $this->sv->get();
    return $query->row_array();
  }

  //function save data edit
  public function editproses($komp_biayaid, $data){
    $this->sv->where($this->id,$komp_biayaid);
		$this->sv->update($this->tbl,$data);
  }

  //function delete data
  public function delete($data){
    $this->sv->where($data);
    $this->sv->delete($this->tbl);
  } 




  //datatables serverside
  private function _get_datatables_query() {
    
    $this->sv->select("case when komp_biaya = 1 then 'Rawat Jalan' when komp_biaya = 2 then 'Rawat Inap' else '-' end as komp_biaya, nama_komp, komp_biayaid");
    $this->sv->from('komp_biaya');

    $i = 0;

    foreach ($this->column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->sv->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->sv->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->sv->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->sv->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->sv->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->sv->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables(){
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->sv->limit($_POST['length'], $_POST['start']);
    $query = $this->sv->get();
    return $query->result();
  }

// function get_datatables_verify()
// {
//   $this->_get_datatables_query();
//   if($_POST['length'] != -1)
//   $this->db->limit($_POST['length'], $_POST['start']);
//   $this->db->where("akses_level","kecamatan");
//   $query = $this->db->get();
//   return $query->result();
// }

  function count_filtered(){
    $this->_get_datatables_query();
    $query = $this->sv->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->sv->from($this->tbl);
    return $this->sv->count_all_results();
  }

}