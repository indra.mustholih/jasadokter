<?php

  function money_to_view($amount, $simbol = ''){		
		if($amount == '' || $amount == 'null'){
			$amount = 0;
		}
		
		if($simbol != ''){
			$simbol = $simbol.' ';
		}
		
		$amount = round($amount, 2);
		if($amount < 0){		
			$amount = $amount*-1;
			$amount = "(". $simbol .number_format($amount, 2, '.', ',').")";
		} else if($amount > 0) {
			$amount =  $simbol .number_format($amount, 2, '.', ',');
		} else {
			$amount = '-';
		}
		return $amount;
	}

?>
