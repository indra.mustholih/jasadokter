<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><i class="fa fa-file-o"></i> <?=$title_panel?></h2>
                <!-- <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div class="body">
                <!-- Nav tabs -->
<!--                <ul class="nav nav-tabs tab-nav-right" role="tablist">-->
<!--                    <li role="presentation" class="active"><a href="#umum" data-toggle="tab">Umum</a></li>-->
<!--                </ul>-->
                <div>
                    <button type="button" onclick="loadForm('PUT')" class="btn btn-primary btn-sm">Tambah Data <i class="fa fa-plus"></i></button>
                </div>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- start umum -->
                    <div role="tabpanel" class="tab-pane fade in active" id="umum">
                        <div>

                        </div>
                        <hr>
                        <div id="targetTable" style="overflow:;"></div>
                    </div>
                    <!-- end umum -->
                </div>
            </div>
        </div>
    </div>
</div>

<!--<!-- Button trigger modal -->
<!--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">-->
<!--    Launch demo modal-->
<!--</button>-->

<!-- Modal -->
<div class="modal fade" id="modalData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Data</h4>
            </div>
            <div class="modal-body">
                <div id="targetForm"></div>
            </div>
<!--            <div class="modal-footer">-->
<!--                -->
<!--            </div>-->
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        loadTable();
    });

    function loadTable() {
        $.ajax({
            type:'POST',
            url:'<?=base_url('biling_dokter/sisa_bayar_table')?>',
            // data:{},
            beforeSend:function(){
                $('#targetTable').html('<div class="alert alert-info"><h3>Memuat Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
            },
            success:function(data){
                $('#targetTable').html(data);
            }
        });
    }

    function loadForm(way='',id='') {
        $.ajax({
            type:'POST',
            url:'<?=base_url('biling_dokter/sisa_bayar_form')?>',
            data:{way:way,id:id},
            beforeSend:function(){
                if(way=='PUT'){
                    $('#modalData').modal('show');
                    $('#targetForm').html('<div class="alert alert-info"><h3>Memuat Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
                }
            },
            success:function(data){
                if(way=='PUT'){
                    $('#targetForm').html(data);
                }else{
                    data = JSON.parse(data);
                    if(data.stat){
                        loadTable();
                        $('#modalData').modal('hide');
                    }else{
                        alert("Oops... Something Wrong...");
                    }

                }

            }
        });
    }

    // $('.datepicker').datepicker({
    //     autoclose: true,
    //     format: "yyyy-mm-dd",
    //   });
    // $('.select2').select2();
</script>