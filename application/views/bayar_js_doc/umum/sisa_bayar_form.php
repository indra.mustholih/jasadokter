<?php
/**
 * Created by PhpStorm.
 * User: Zaenal Alim Prayoga
 * Date: 26/11/2018
 * Time: 11.28
 */
?>

<form class="" id="formUmum" style="">
    <input type="hidden" id="id" name="id" value="<?=isset($edit->id)?$edit->id:''?>">
    <input type="hidden" value="POST" name="way" id="way">
    <div class="form-group">
        <label>Nama Dokter : </label>
        <select class="form-control select2" id="kd_doc" name="kd_doc" style="width:100%;">
            <option value="">-- Pilih Dokter --</option>
            <?php foreach ($dokter as $dr): ?>
                <option <?=isset($edit->kd_doc) && $edit->kd_doc == $dr->KdDoc? 'selected':''?> value="<?=$dr->KdDoc?>"><?=$dr->NmDoc?></option>
            <?php endforeach ?>
        </select>
    </div>

<!--    <div class="form-group">-->
<!--        <label>Periode : </label>-->
<!--        <div class="input-group">-->
<!--            <div class="form-line">-->
<!--                <input type="date" name="tgl_awalU" class="form-control " id="tgl_awalU">-->
<!--            </div>-->
<!--            <span class="input-group-addon">&nbsp;s/d&nbsp;</span>-->
<!--            <div class="form-line">-->
<!--                <input type="date" name="tgl_awalU" class="form-control " id="tgl_akhirU">-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <div class="form-group">
        <label for="Sisa yang belum di bayarkan?"></label>
        <div class="input-group">
            <span class="input-group-addon">
                Rp.
            </span>
            <div class="form-line">
                <input type="text" class="form-control" id="sisa_bayar" name="sisa_bayar" placeholder="0,00" value="<?=isset($edit->sisa_bayar)?$edit->sisa_bayar:''?>">
            </div>
        </div>
    </div>
    <div style="text-align:right;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="fa fa-remove"></i></button>
        <button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
    </div>
</form>

<script>
    $('#sisa_bayar').keypress(function(e){
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            //    $("#errNom").html("*input haynya untuk digit angka saja!").show().fadeOut(2500);
            return false;
        }
    });
    $('.select2').select2();

    $('#formUmum').submit(function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url:'<?=base_url('biling_dokter/sisa_bayar_form')?>',
            data:$(this).serialize(),
            dataType:'JSON',
            beforeSend:function(){

            },
            success:function(data){
                if(data.stat){
                    loadTable();
                    $('#modalData').modal('hide');
                }else{
                    alert("Oops... Something Wrong...");
                }
            }
        });
    });
</script>
