<?php
/**
 * Created by PhpStorm.
 * User: Zaenal Alim Prayoga
 * Date: 26/11/2018
 * Time: 10.49
 */
?>

<table id="tblSisa" class="table table-bordered" style="width:100%;">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Dokter</th>
            <th>Sisa Yang Belum Dibayarkan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $n=1;foreach ($list as $l):?>
            <tr>
                <td><?=$n?></td>
                <td><?=$l->nm_doc?></td>
                <td><?=$this->cl->formatAngka($l->sisa_bayar)?></td>
                <td>
                    <button type="button" id="btnEdit" class="btn btn-info" onclick="loadForm('PUT','<?=$l->id?>')"><i class="fa fa-pencil"></i></button>
                    <button type="button" id="btnDelete" class="btn btn-warning" onclick="loadForm('DEL','<?=$l->id?>')"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
        <?php $n++;endforeach;?>
    </tbody>
</table>
<script>
    $('#tblSisa').dataTable();
</script>