<?php
    $tarif=0;$jsDok=0;$ok=0;$medis=0;
    $pph=0;$jasa=0;
    $periode = '';
    $NmDoc != '' ? $NmDoc:'';
    $tgl_awal != '' ? $tgl_awal:'';
    $tgl_akhir != '' ? $tgl_akhir:'';
    $periode = 'Periode: '.$this->cl->convDate($tgl_awal,7).' - '.$this->cl->convDate($tgl_akhir,7);
//  echo "<pre>";print_r($ok);echo "</pre>";die();
?>
<small>
    <table class="table table-bordered table-hover" id="tblBilUmum" style="width:100%;">
        <thead>
        <tr>
            <th rowspan="2">No.</th>
            <th rowspan="2">Nomor Registrasi </th>
            <th rowspan="2">Nama Pasien</th>
            <th rowspan="2">Kelas / Poli</th>
            <th rowspan="2" style="width:9%;">Tanggal</th>
            <th rowspan="2" style="width:15%;">Keterangan</th>
            <th rowspan="2" style="width:10%;">Tarif</th>
            <th colspan="3" style="width:10%;">Rawat Inap dan Rawat Jalan</th>
            <th rowspan="2" style="width:10%;">Aksi</th>
        </tr>
        <tr>
            <th>Visite / Jasa</th>
            <th>Operasi</th>
            <th>Tindakan / P. Medis</th>
        </tr>
        </thead>
        <?php $n=1;$hd='';foreach($rajal->head as $r1):?>
            <?php if($hd!="IRJ"):$hd="IRJ"?>
                <tr>
                    <th colspan="11" style="text-align: center">RAWAT JALAN</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->regno?></td>
                <td><?=$r1->nama?></td>
                <td><?=$r1->NMPoli?></td>
                <td colspan="6" style="padding:0;margin:0">
                    <table class="" style="margin:0;width:100%;">
                        <?php $jasa=0;foreach ($rajal->detail as $r2):if($r2->notran == $r1->notran):?>
                        <tr>
                            <td><?=$this->cl->convDate($r2->tanggal,7)?></td>
                            <td style="width:40%;"><?=$r2->detail_tarif?></td>
                            <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r2->biaya_tarif)?><?php $tarif+=$r2->biaya_tarif;?></td>
                            <td style="text-align:right;width:20%;"><?php $jsDok+=$r2->visite_konsul;$jasa+=$r2->visite_konsul;?>
                                <?=$this->cl->formatAngka($r2->visite_konsul)?></td>
                            <td style="text-align:right;width:20%;"><?php $ok+=$r2->operasi;$jasa+=$r2->operasi;?>
                                <?=$this->cl->formatAngka($r2->operasi)?></td></td>
                            <td style="text-align:right;width:20%;"><?php $medis+=$r2->tindakan_medis;$jasa+=$r2->tindakan_medis;?>
                                <?=$this->cl->formatAngka($r2->tindakan_medis)?></td>
                        </tr>
                        <?php endif;endforeach;?>
                    </table>
                </td>

                <td>
                    <button type="button" id="btnBayar_<?=$n-1?>" onclick="verf_bayar('<?=$n-1?>','<?=$r1->id?>','<?=$jasa?>')" data-stat="1" class="btn btn-info"><i class="fa fa-check"></i> Bayar</button>
                    <a href="<?= base_url('Biling_dokter/cetak_billing/pdf/rajal/'.$r2->regno)?>" class="btn btn-purple btn-round" title="Cetak Rincian Pembayaran" target="_blank"><i class="fa fa-print"></i></a>
                </td>
            </tr>
        <?php $n++;endforeach;?>

        <?php $hd='';foreach($ugd->head as $r1):?>
            <?php if($hd!="UGD"):$hd="UGD"?>
                <tr>
                    <th colspan="11" style="text-align: center">UGD</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->regno?></td>
                <td><?=$r1->nama?></td>
                <td><?=$r1->NMPoli?></td>
                <td colspan="6" style="padding:0;margin:0">
                    <table class="" style="margin:0;width:100%;">
                        <?php $jasa=0;foreach ($ugd->detail as $r2):if($r2->notran == $r1->notran):?>
                            <tr>
                                <td><?=$this->cl->convDate($r2->tanggal,7)?></td>
                                <td style="width:40%;"><?=$r2->detail_tarif?></td>
                                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r2->biaya_tarif)?><?php $tarif+=$r2->biaya_tarif;?></td>
                                <td style="text-align:right;width:20%;"><?php $jsDok+=$r2->visite_konsul;$jasa+=$r2->visite_konsul;?>
                                    <?=$this->cl->formatAngka($r2->visite_konsul)?></td>
                                <td style="text-align:right;width:20%;"><?php $ok+=$r2->operasi;$jasa+=$r2->operasi;?>
                                    <?=$this->cl->formatAngka($r2->operasi)?></td></td>
                                <td style="text-align:right;width:20%;"><?php $medis+=$r2->tindakan_medis;$jasa+=$r2->tindakan_medis;?>
                                    <?=$this->cl->formatAngka($r2->tindakan_medis)?></td>
                            </tr>
                        <?php endif;endforeach;?>
                    </table>
                </td>

                <td>
                    <button type="button" id="btnBayar_<?=$n-1?>" onclick="verf_bayar('<?=$n-1?>','<?=$r1->id?>','<?=$jasa?>')" data-stat="1" class="btn btn-info"><i class="fa fa-check"></i> Bayar</button>
                </td>
            </tr>
            <?php $n++;endforeach;?>

        <?php $hd='';foreach($irna->head as $r1):?>
            <?php if($hd!="IRNA"):$hd="IRNA"?>
                <tr>
                    <th colspan="11" style="text-align: center">RAWAT INAP</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->regno?></td>
                <td><?=$r1->nama?></td>
                <td><?=$r1->NmBangsal?> / <?=$r1->NMKelas?></td>
                <td colspan="6" style="padding:0;margin:0">
                    <table class="" style="margin:0;width:100%;">
                        <?php $jasa=0;foreach ($irna->detail as $r2):if($r2->notran == $r1->notran):?>
                            <tr>
                                <td><?=$this->cl->convDate($r2->tanggal,7)?></td>
                                <td style="width:40%;"><?=$r2->detail_tarif?></td>
                                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r2->biaya_tarif)?><?php $tarif+=$r2->biaya_tarif;?></td>
                                <td style="text-align:right;width:20%;"><?php $jsDok+=$r2->visite_konsul;$jasa+=$r2->visite_konsul;?>
                                    <?=$this->cl->formatAngka($r2->visite_konsul)?></td>
                                <td style="text-align:right;width:20%;"><?php $ok+=$r2->operasi;$jasa+=$r2->operasi;?>
                                    <?=$this->cl->formatAngka($r2->operasi)?></td></td>
                                <td style="text-align:right;width:20%;"><?php $medis+=$r2->tindakan_medis;$jasa+=$r2->tindakan_medis;?>
                                    <?=$this->cl->formatAngka($r2->tindakan_medis)?></td>
                            </tr>
                        <?php endif;endforeach;?>
                    </table>
                </td>

                <td>
                    <button type="button" id="btnBayar_<?=$n-1?>" onclick="verf_bayar('<?=$n-1?>','<?=$r1->id?>','<?=$jasa?>')" data-stat="1" class="btn btn-info"><i class="fa fa-check"></i> Bayar</button>
                </td>
            </tr>
            <?php $n++;endforeach;?>

        <?php $hd='';foreach($opr->head as $r1):?>
            <?php if($hd!="OK"):$hd="OK"?>
                <tr>
                    <th colspan="11" style="text-align: center">OK</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->regno?></td>
                <td><?=$r1->nama?></td>
                <td><?=$r1->NmBangsal?> / <?=$r1->NMKelas?></td>
                <td colspan="6" style="padding:0;margin:0">
                    <table class="" style="margin:0;width:100%;">
                        <?php $jasa=0;foreach ($opr->detail as $r2):if($r2->notran == $r1->notran):?>
                            <tr>
                                <td><?=$this->cl->convDate($r2->tanggal,7)?></td>
                                <td style="width:40%;"><?=$r2->detail_tarif?></td>
                                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r2->biaya_tarif)?><?php $tarif+=$r2->biaya_tarif;?></td>
                                <td style="text-align:right;width:20%;"><?php $jsDok+=$r2->visite_konsul;$jasa+=$r2->visite_konsul;?>
                                    <?=$this->cl->formatAngka($r2->visite_konsul)?></td>
                                <td style="text-align:right;width:20%;"><?php $ok+=$r2->operasi;$jasa+=$r2->operasi;?>
                                    <?=$this->cl->formatAngka($r2->operasi)?></td></td>
                                <td style="text-align:right;width:20%;"><?php $medis+=$r2->tindakan_medis;$jasa+=$r2->tindakan_medis;?>
                                    <?=$this->cl->formatAngka($r2->tindakan_medis)?></td>
                            </tr>
                        <?php endif;endforeach;?>
                    </table>
                </td>

                <td>
                    <button type="button" id="btnBayar_<?=$n-1?>" onclick="verf_bayar('<?=$n-1?>','<?=$r1->id?>','<?=$jasa?>')" data-stat="1" class="btn btn-info"><i class="fa fa-check"></i> Bayar</button>
                </td>
            </tr>
            <?php $n++;endforeach;?>

        <?php $hd='';foreach($rad as $r1):?>
            <?php if($hd!="Radiologi"):$hd="Radiologi"?>
                <tr>
                    <th colspan="11" style="text-align: center">Radiologi</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->Regno?></td>
                <td><?=$r1->Firstname?></td>
                <?php if($r1->NmBangsal == '') :?>
                <td><?=$r1->NMPoli?></td>
                <?php else : ?>
                <td><?=$r1->NmBangsal?> / <?=$r1->NMKelas?></td>
                <?php endif;?>
                <td><?=$this->cl->convDate($r1->Tanggal,7)?></td>
                <td style="width:40%;"><?=$r1->NmTarif?></td>
                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r1->JumlahBiaya)?><?php $tarif+=$r1->JumlahBiaya;?></td>
                <td style="text-align:right;width:20%;">
                    <?php $jsDok+=($r1->JumlahBiaya*0.19);$jum=($r1->JumlahBiaya*0.19);$jasa+=$jum;?><?=$this->cl->formatAngka($jum)?></td>
                    </td>
                <td style="text-align:right;width:20%;">0.00</td>
                <td style="text-align:right;width:20%;">0.00</td>

                <td>
                    <button type="button" id="btnBayar_<?=$n-1?>" onclick="verf_bayar('<?=$n-1?>','<?=$r1->Notran?>','<?=$jasa?>')" data-stat="1" class="btn btn-info"><i class="fa fa-check"></i> Bayar</button>
                </td>
            </tr>
        <?php $n++;endforeach;?>

        <!-- <?php $hd='';foreach($lab as $r1):?>
            <?php if($hd!="Laboratorium"):$hd="Laboratorium"?>
                <tr>
                    <th colspan="11" style="text-align: center">Laboratorium</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->Regno?></td>
                <td><?=$r1->Firstname?></td>
                <?php if($r1->NmBangsal == '') :?>
                <td><?=$r1->NMPoli?></td>
                <?php else : ?>
                <td><?=$r1->NmBangsal?> / <?=$r1->NMKelas?></td>
                <?php endif;?>
                <td><?=$this->cl->convDate($r1->Tanggal,7)?></td>
                <td style="width:40%;"><?=$r1->NmTarif?></td>
                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r1->JumlahBiaya)?><?php $tarif+=$r1->JumlahBiaya;?></td>
                <td style="text-align:right;width:20%;">
                    <?php $jsDok+=($r1->JumlahBiaya*0.10);$jum=($r1->JumlahBiaya*0.10);$jasa+=$jum;?><?=$this->cl->formatAngka($jum)?></td>
                    </td>
                <td style="text-align:right;width:20%;">0.00</td>
                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r1->JumlahBiaya*0.175)?><?php $medis+=($r1->JumlahBiaya*0.175);?></td>

                <td>
                    <button type="button" id="btnBayar_<?=$n-1?>" onclick="verf_bayar('<?=$n-1?>','<?=$r1->Notran?>','<?=$jasa?>')" data-stat="1" class="btn btn-info"><i class="fa fa-check"></i> Bayar</button>
                </td>
            </tr>
        <?php $n++;endforeach;?> -->

        <!-- <?php $hd='';foreach($hd as $r1):?>
            <?php if($hd!="Hemodialisa"):$hd="Hemodialisa"?>
                <tr>
                    <th colspan="11" style="text-align: center">Hemodialisa</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->Regno?></td>
                <td><?=$r1->Firstname?></td>
                <?php if($r1->NmBangsal == '') :?>
                <td><?=$r1->NMPoli?></td>
                <?php else : ?>
                <td><?=$r1->NmBangsal?> / <?=$r1->NMKelas?></td>
                <?php endif;?>
                <td><?=$this->cl->convDate($r1->Tanggal,7)?></td>
                <td style="width:40%;"><?=$r1->NmTarif?></td>
                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r1->JumlahBiaya)?><?php $tarif+=$r1->JumlahBiaya;?></td>
                <td style="text-align:right;width:20%;">
                    <?php $jsDok+=($r1->JumlahBiaya*0.19);$jum=($r1->JumlahBiaya*0.12);$jasa+=$jum;?><?=$this->cl->formatAngka($jum)?></td>
                    </td>
                <td style="text-align:right;width:20%;">0.00</td>
                <td style="text-align:right;width:20%;">0.00</td>

                <td>
                    <button type="button" id="btnBayar_<?=$n-1?>" onclick="verf_bayar('<?=$n-1?>','<?=$r1->Notran?>','<?=$jasa?>')" data-stat="1" class="btn btn-info"><i class="fa fa-check"></i> Bayar</button>
                </td>
            </tr>
        <?php $n++;endforeach;?> -->

        </tbody>
        <tfoot>
            <tr>
                <td colspan="6" style="text-align: right;">Total : </td>
                <td style="text-align: right;"><?=$this->cl->formatAngka($tarif)?></td>
                <td style="text-align: right;"><?=$this->cl->formatAngka($jsDok)?></td>
                <td style="text-align: right;"><?=$this->cl->formatAngka($ok)?></td>
                <td style="text-align: right;"><?=$this->cl->formatAngka($medis)?></td>
                <td>-</td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: right;">Total Jasa Didapat : </td>
                <td colspan="4" style="text-align: right;"><?=$this->cl->formatAngka($jsDok+$ok+$medis)?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: right;">Total Jasa Dibayarkan : </td>
                <td colspan="4" style="text-align: right;"><span id="jasa_dibayar">0,00</span></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</small>
<script type="text/javascript">
var nmdoc = '<?=$NmDoc?>';
var periode = '<?=$periode?>';
var headPrint = '<div style="border-top:;border-bottom:1px solid black;width:35%;text-align:center;"><span style="text-align:center;font-size:12px;">DINAS KESHATAN ANGKATAN UDARA</span><br><span style="text-align:center;font-size:12px;">RSPAU dr. S. HARDJOLUKITO</span></div><h4 style="text-align:center;">JASA PELAYANAN PASIEN UMUM</h4><h5 style="text-align:center">'+periode+'</h5><hr><table><tr><td>Dokter:&nbsp;'+nmdoc+'</td></tr></table><br>';

/*table = $('#tblBilUmum').DataTable({
    dom: '<lfB<t>ip>',
    buttons: [
        {
            text: '<span style="color:black"><i class="fa fa-clipboard"></i> Cetak Data</span>',
            extend: 'print',
            footer:true,
            title: 'JASA DOKTER',
            autoPrint: true,
            className: 'btn btn-sm btn-primary',
        },
        {
            text: '<span style="color:black"><i class="fa fa-file-excel-o"></i> Export Ke Excel</span>',
            extend: 'excelHtml5',
            // footer:true,
            title: 'JASA DOKTER',
            // autoPrint: true,
            className: 'btn btn-sm btn-success',
        },
    ],
    lengthMenu: [[15, 25, 50, 100, -1], [15, 25, 50, 100, "All"]],
  "pageLength":15,
  "scrollX":true,
  "processing": true, //Feature control the processing indicator.
    // "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    "language": {
      "decimal": ",",
      "thousands": ".",
      "processing": '<i class="fa fa-refresh fa-spin fa-3x fa-fw" style="color:#2980b9"></i><span>Mohon Tunggu...</span>',
      "paginate": {
        "first"   : "Pertama",
        "previous": "<span class='fa fa-arrow-left'></span>",
        "next"    : "<span class='fa fa-arrow-right'></span>",
        "last"    : "Terakhir"
      },
      "zeroRecords": "Tidak ada data yang dapat ditampilkan"

    },
    "bLengthChange": false,

  });*/
    var tmpBayar = new Array();
    var tmpJmlDibayar = 0;
    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };

    function formatNum(num=0) {
        num = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return num;
    }

    function verf_bayar(index='',id='',jasa=''){
        var stat = $('#btnBayar_'+index).data('stat');
        if(stat == '1'){
            // if(id!='' && jasa != ''){
            //     tmpBayar[index] = {
            //         'key' : id,
            //         'val' : jasa,
            //     }
            // }
            tmpJmlDibayar = Number(tmpJmlDibayar);
            jasa = Number(jasa);
            tmpJmlDibayar+=jasa;
            $('#btnBayar_'+index).data('stat','2');
            $('#btnBayar_'+index).html('<i class="fa fa-remove"></i> Batal');
            $('#btnBayar_'+index).removeClass('btn-info');
            $('#btnBayar_'+index).addClass('btn-warning');
            $('#jasa_dibayar').html(formatNum(tmpJmlDibayar)+',00');
        }else if(stat == '2'){
            tmpJmlDibayar = Number(tmpJmlDibayar);
            jasa = Number(jasa);
            tmpJmlDibayar-=jasa;
            $('#btnBayar_'+index).data('stat','1');
            $('#jasa_dibayar').html(formatNum(tmpJmlDibayar)+',00');
            $('#btnBayar_'+index).html('<i class="fa fa-check"></i> Bayar');
            $('#btnBayar_'+index).removeClass('btn-warning');
            $('#btnBayar_'+index).addClass('btn-info');
        }
    }
</script>