<div class="row clearfix">
    <div class="card">

        <div class="body">
            <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
            <h2 class="card-inside-title">Edit Biaya Perkomponen</h2>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <?php echo form_open(base_url('biayaPerkomponen/edit/'.$this->cl->enkrip($komp_perbiayaid)));?>

                    <div class="form-group">
                        <label for="">Komponen Biaya</label>
                        <div class="form-line">
                          <select name="komp_biayaid" class="form-control">
                            <option value=""> - Pilih Komponen Biaya - </option>
                            <?php foreach($komponenbiaya as $row){?>
                              <option value="<?php echo $row['komp_biayaid']?>" <?php if($biayaperkomponen['komp_biayaid'] == $row['komp_biayaid']){echo "selected";}?>><?php echo $row['nama_komp']?></option>
                            <?php }?>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Jasa Pelayanan</label>
                        <div class="form-line">
                          <select name="komp_jasaid" class="form-control">
                            <option value=""> - Pilih Jasa Pelayanan - </option>
                            <?php foreach($komponenjasa as $row){?>
                              <option value="<?php echo $row['komp_jasaid']?>" <?php if($biayaperkomponen['komp_jasaid'] == $row['komp_jasaid']){echo "selected";}?>><?php echo $row['nama_jasa']?></option>
                            <?php }?>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Jenis Jasa</label>
                        <div class="form-line">
                          <select name="jenis_jasa" class="form-control">
                            <option value=""> - Pilih Jenis Jasa - </option>
                            <option value="1" <?php if($biayaperkomponen['jenis_jasa'] == '1'){echo "selected";}?>> Medis </option>
                            <option value="2" <?php if($biayaperkomponen['jenis_jasa'] == '2'){echo "selected";}?>> Dokter </option>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Type</label>
                        <div class="form-line">
                          <select name="tipe" class="form-control" onchange="getType(this.value)">
                            <option value="0"> - Type - </option>
                            <option value="1" <?php if($biayaperkomponen['tipe'] == '1'){echo "selected";}?>> Amount </option>
                            <option value="2" <?php if($biayaperkomponen['tipe'] == '2'){echo "selected";}?>> Persen </option>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Biaya Jasa</label>
                        <div class="form-line">
                            <input id="komp_biaya_jasa" type="number" name="biaya_jasa" class="form-control" placeholder="Masukan Biaya Jasa" value="<?php echo $biayaperkomponen['biaya_jasa']?>" <?php if($biayaperkomponen['tipe'] == '2'){echo "readonly";}?>>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Persen</label>
                        <div class="form-line">
                            <input id="komp_persen" type="number" name="persen" class="form-control" placeholder="Masukan Persen" value="<?php echo $biayaperkomponen['persen']?>" <?php if($biayaperkomponen['tipe'] == '1'){echo "readonly";}?>>
                        </div>
                    </div>
                    
                    <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg">
                        <i class="fa fa-save"></i> &nbsp Simpan Data
                    </button> 
                    <a href="<?php echo base_url('biayaPerkomponen')?>" class="btn btn-default btn-lg">Batal</a>     
                    </div>
                    
                  <?php echo form_close();?>
                </div>
            </div>

        </div>
    </div>
 </div>
 <script type="text/javascript">
  function getType(value){
    var value = value;
    if(value == 1){
      document.getElementById('komp_biaya_jasa').readOnly = false;
      document.getElementById('komp_persen').readOnly = true;
      $('#komp_persen').val(0);
    }else if(value == 2){
      document.getElementById('komp_biaya_jasa').readOnly = true;
      document.getElementById('komp_persen').readOnly = false;
      $('#komp_biaya_jasa').val(0);
    }else {
      document.getElementById('komp_biaya_jasa').readOnly = true;
      document.getElementById('komp_persen').readOnly = true;
      $('#komp_biaya_jasa').val(0);
      $('#komp_persen').val(0);
    }
  }
 </script>