<div class="row clearfix">
    <div class="card">

        <div class="body">
            <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
            <h2 class="card-inside-title">Tambah Biaya Perkomponen</h2>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <?php echo form_open(base_url('biayaPerkomponen/add'));?>

                    <div class="form-group">
                        <label for="">Komponen Biaya</label>
                        <div class="form-line">
                          <select name="komp_biayaid" class="form-control">
                            <option value=""> - Pilih Komponen Biaya - </option>
                            <?php foreach($komponenbiaya as $row){?>
                              <option value="<?php echo $row['komp_biayaid']?>"> <?php echo $row['nama_komp']?> </option>
                            <?php }?>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Jasa Pelayanan</label>
                        <div class="form-line">
                          <select name="komp_jasaid" class="form-control">
                            <option value=""> - Pilih Jasa Pelayanan - </option>
                            <?php foreach($komponenjasa as $row){?>
                              <option value="<?php echo $row['komp_jasaid']?>"> <?php echo $row['nama_jasa']?> </option>
                            <?php }?>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Jenis Jasa</label>
                        <div class="form-line">
                          <select name="jenis_jasa" class="form-control">
                            <option value=""> - Pilih Jenis Jasa - </option>
                            <option value="1"> Medis </option>
                            <option value="2"> Dokter </option>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Type</label>
                        <div class="form-line">
                          <select name="tipe" class="form-control" onchange="getType(this.value)">
                            <option value="0"> - Type - </option>
                            <option value="1"> Amount </option>
                            <option value="2"> Persen </option>
                          </select>
                        </div>
                    </div>

                    <div class="form-group" >
                        <label for="">Biaya Jasa</label>
                        <div class="form-line">
                            <input type="number" name="biaya_jasa" class="form-control" placeholder="Masukan Biaya Jasa" id="komp_biaya_jasa" readonly>
                        </div>
                    </div>

                    <div class="form-group" >
                        <label for="">Persen</label>
                        <div class="form-line">
                            <input type="number" name="persen" class="form-control" placeholder="Masukan Persen" id="komp_persen" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg">
                        <i class="fa fa-save"></i> &nbsp Simpan Data
                    </button> 
                    <a href="<?php echo base_url('biayaPerkomponen')?>" class="btn btn-default btn-lg">Batal</a>     
                    </div>
                    
                  <?php echo form_close();?>
                </div>
            </div>

        </div>
    </div>
 </div>
 <script type="text/javascript">
  function getType(value){
    var value = value;
    if(value == 1){
      document.getElementById('komp_biaya_jasa').readOnly = false;
      document.getElementById('komp_persen').readOnly = true;
      $('#komp_persen').val(0);
    }else if(value == 2){
      document.getElementById('komp_biaya_jasa').readOnly = true;
      document.getElementById('komp_persen').readOnly = false;
      $('#komp_biaya_jasa').val(0);
    }else {
      document.getElementById('komp_biaya_jasa').readOnly = true;
      document.getElementById('komp_persen').readOnly = true;
      $('#komp_biaya_jasa').val(0);
      $('#komp_persen').val(0);
    }
  }
 </script>