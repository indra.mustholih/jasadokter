<div class="row clearfix">
    <div class="card">

        <div class="body">
            <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
            <h2 class="card-inside-title">Ubah Password Admin Website</h2>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <?php echo form_open(base_url('admin/user/ubah_password/'.$user['id_user']));?>
                    <div class="form-group">
                            <label for="">Password</label>
                            <div class="form-line">
                                <input type="password" name="password" class="form-control" placeholder="Masukan password" required>
                            </div>
                        </div>
                     <div class="form-group">
                            <label for="">Ulangi Password</label>
                            <div class="form-line">
                                <input type="password" name="password2" class="form-control" placeholder="Masukan Kembali Password" required>
                            </div>
                        </div>
                    <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg">
                        <i class="fa fa-save"></i> &nbsp Simpan Data
                    </button> 
                    <a href="<?php echo base_url()?>" class="btn btn-default btn-lg">Batal</a>     
                    </div>
                    
                  <?php echo form_close();?>
                </div>
            </div>

        </div>
    </div>
 </div>