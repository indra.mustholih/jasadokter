<div class="row clearfix">
    <div class="card">

        <div class="body">
            <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
            <h2 class="card-inside-title">Tambah User</h2>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <?php echo form_open(base_url('admin/user/tambah'));?>
                    <div class="form-group">
                        <label for="">Username</label>
                        <div class="form-line">
                            <input type="text" name="username" class="form-control" placeholder="Masukan Username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <div class="form-line">
                            <input type="password" name="password" class="form-control" placeholder="Masukan password" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Akses Level</label>
                        <div class="form-line">
                           <select name="akses_level" class="form-control">
                               <option value="admin">Admin</option>
                               <option value="user">User</option>
                           </select>
                        </div>
                    </div>
                    <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg">
                        <i class="fa fa-save"></i> &nbsp Simpan Data
                    </button> 
                    <a href="<?php echo base_url('Administratif/Data-User')?>" class="btn btn-default btn-lg">Batal</a>     
                    </div>
                    
                  <?php echo form_close();?>
                </div>
            </div>

        </div>
    </div>
 </div>