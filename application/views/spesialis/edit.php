<div class="row clearfix">
    <div class="card">

        <div class="body">
            <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
            <h2 class="card-inside-title">Edit Klinik Spesialis</h2>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <?php echo form_open(base_url('spesialis/edit/'.$this->cl->enkrip($trans_spesialisid)));?>
                    <div class="form-group">
                        <label for="">Jasa Pelayanan</label>
                        <div class="form-line">
                          <select name="mst_jasapelayananid" class="form-control">
                            <option value=""> - Pilih Group Jasa Pelayanan - </option>
                            <?php foreach($jasapelayananGetAll as $row){?>
                            <option value="<?php echo $row->mst_jasapelayananid?>" <?php if($spesialis['mst_jasapelayananid'] == $row->mst_jasapelayananid){echo "selected";}?>><?php echo $row->nama_jasa?></option> 
                            <?php }?>
                          </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Jumlah </label>
                        <div class="form-line">
                            <input type="number" name="jumlah" class="form-control" placeholder="Masukan Jumlah" value="<?php echo $spesialis['jumlah']?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <div class="form-line">
                            <input type="text" name="keterangan" class="form-control" placeholder="Masukan Keterangan" value="<?php echo $spesialis['keterangan']?>" required>
                        </div>
                    </div>
                   
                    
                    <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg">
                        <i class="fa fa-save"></i> &nbsp Simpan Data
                    </button> 
                    <a href="<?php echo base_url('spesialis')?>" class="btn btn-default btn-lg">Batal</a>     
                    </div>
                    
                  <?php echo form_close();?>
                </div>
            </div>

        </div>
    </div>
 </div>