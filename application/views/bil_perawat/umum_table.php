<?php
	$jt=$td=0;
	$jbj=$tbj=0;$jbv=$tbv=0;
	$jbh=$tbh=0;$jbo=$tbo=0;
	$jbl=$tbl=0;$jbr=$tbr=0;
	$jbu=$tbu=0;
	$pph=0;$jasa='';
	$periode = '';
	// $NmDoc != '' ? $NmDoc:'';
	$tgl_awal != '' ? $tgl_awal:'';
	$tgl_akhir != '' ? $tgl_akhir:'';
	$periode = 'Periode: '.$this->cl->convDate($tgl_awal,7).' - '.$this->cl->convDate($tgl_akhir,7);
?>
<table class="table table-bordered table-hover" id="tblBilUmum" style="width:100%;">
	<thead>
		<tr>
			<th>No.</th>
			<th>Tanggal</th>
			<th>Nama Pasien</th>
			<th>Tindakan</th>
			<th>Ruangan/Poli</th>
			<th>Pembayaran Pasien</th>
			<th>Jasa Perawat</th>
		</tr>
	</thead>
	<tbody>
		<?php $n=1;foreach ($jasper->jaskep as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
		<tr>
			<td><?=$n?></td>
			<td><?=$this->cl->convDate($l->Tanggal,7)?></td>
			<td><?=ucwords(strtolower($l->Firstname))?></td>
			<td><?=$l->NmTarif?></td>
			<td><?=$l->NmBangsal?></td>
			<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
			<td style="text-align:right;"><?=$this->cl->formatUang($jb*0.35,2)?></td>
		</tr>
		<?php $n++;$jbj+=$jb;$tbj+=$jb*0.35;endforeach;?>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th style="text-align:right;">Jumlah Total: </th>
			<?php $jt=$jbj+$jbv+$jbh+$jbl+$jbr+$jbu; $td=$tbj+$tbv+$tbh+$tbl+$tbr+$tbu;?>
			<th style="text-align:right;"><?=$this->cl->formatUang($jt,2)?></th>
			<th style="text-align:right;"><?=$this->cl->formatUang($td,2)?></th>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
var periode = '<?=$periode?>';
var headPrint = '<div style="border-top:;border-bottom:1px solid black;width:35%;text-align:center;"><span style="text-align:center;font-size:12px;">DINAS KESHATAN ANGKATAN UDARA</span><br><span style="text-align:center;font-size:12px;">RSPAU dr. S. HARDJOLUKITO</span></div><h4 style="text-align:center;">JASA PELAYANAN PASIEN UMUM</h4><h5 style="text-align:center">'+periode+'</h5><hr><br>';
table = $('#tblBilUmum').DataTable({    
  dom: 'Bfrtip',      
  buttons: [
  {
   text: '<span style="color:black"><i class="fa fa-lg fa-clipboard"></i> Cetak Data</span>',
   extend: 'print',
   footer:true,
      title: headPrint,
      autoPrint: true,
      className: 'btn btn-sm btn-primary',
    },
    // {
    //   text: '<span style="color:black"><i class="fa fa-lg fa-file-excel-o"></i> Export Ke Excel</span>',
    //   extend: 'excel',
    //   footer:true,
    //     // title: 'Laporan Depo Hemodialisa',
    //     // autoPrint: true,
    //     className: 'btn btn-sm btn-success',
        
    //   },
  ],
  "pageLength":15,
  "scrollX":true,
  "processing": true, //Feature control the processing indicator.
    // "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    "language": {
      "decimal": ",",
      "thousands": ".",
      "processing": '<i class="fa fa-refresh fa-spin fa-3x fa-fw" style="color:#2980b9"></i><span>Mohon Tunggu...</span>',
      "paginate": {
        "first"   : "Pertama",
        "previous": "<span class='fa fa-arrow-left'></span>",
        "next"    : "<span class='fa fa-arrow-right'></span>",
        "last"    : "Terakhir"
      },
      "zeroRecords": "Tidak ada data yang dapat ditampilkan"

    },
    "bLengthChange": false,

  });


</script>