<?php
/**
 * Created by PhpStorm.
 * User: Zaenal Alim Prayoga
 * Date: 31/10/2018
 * Time: 21.12
 */
?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><i class="fa fa-file-o"></i> <?=$title_panel?></h2>
                <!-- <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div class="body">
                <div role="tabpanel" class="tab-pane fade in active" id="umum">
                    <div id="targetHasilUmum" style="overflow:;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        loadForm();
    });

    function loadForm() {
        $.ajax({
            type:'POST',
            url:'<?=base_url('master_data/tarif_bpjs_irna_form')?>',
            data:{way:'PUT'},
            beforeSend:function(){
                $('#targetHasilUmum').html('<div class="alert alert-info"><h3>Memuat Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
            },
            success:function(data){
                $('#targetHasilUmum').html(data);
            }
        });
    }
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });
    $('.select2').select2();
</script>
