<?php
/**
 * Created by PhpStorm.
 * User: Zaenal Alim Prayoga
 * Date: 31/10/2018
 * Time: 21.32
 */
?>
<div class="row clearfix">
    <form action="" id="formTarif">
        <input type="hidden" name="way" value="POST">
<!--        <input type="hidden" name="IdTarif" value="1">-->
        <input type="hidden" name="Inst" value="2">
        <!-- A. VISITE DOKTER SPESIALIS / SUBSPESIALIS -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h5>A. VISITE DOKTER SPESIALIS / SUBSPESIALIS</h5>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <!---->
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span>1. KONSUL VIA TELPON</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="visite_spesialis_konsul_telpon" class="form-control input-sm numeric" placeholder="KONSUL VIA TELPON" value="<?=isset($tarif->visite_spesialis_konsul_telpon)?$tarif->visite_spesialis_konsul_telpon:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">2 VISITE</div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;ICU / VIP</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="visite_spesialis_visite_0" class="form-control input-sm numeric" placeholder="(ICU / VIP)" value="<?=isset($tarif->visite_spesialis_visite_0)?$tarif->visite_spesialis_visite_0:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;KELAS I</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="visite_spesialis_visite_1" class="form-control input-sm numeric" placeholder="(KELAS I)" value="<?=isset($tarif->visite_spesialis_visite_1)?$tarif->visite_spesialis_visite_1:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;KELAS II</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="visite_spesialis_visite_2" class="form-control input-sm numeric" placeholder="(KELAS II)" value="<?=isset($tarif->visite_spesialis_visite_2)?$tarif->visite_spesialis_visite_2:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;KELAS III / PERI / ISO</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="visite_spesialis_visite_3" class="form-control input-sm numeric" placeholder="(KELAS III / PERI / ISO)" value="<?=isset($tarif->visite_spesialis_visite_3)?$tarif->visite_spesialis_visite_3:0?>">
                                </div>
                            </div>
                        </div>

                        <!---->
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# A. VISITE DOKTER SPESIALIS / SUBSPESIALIS -->

        <!-- B. VISITE DOKTER UMUM -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h5>B. VISITE DOKTER UMUM</h5>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <!--                    -->
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <span>1. VISITE</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="visite_umum_visite" class="form-control input-sm numeric" placeholder="VISITE" value="<?=isset($tarif->visite_umum_visite)?$tarif->visite_umum_visite:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <span>2. DOKTER JAGA BANGSAL</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="visite_umum_jaga" class="form-control input-sm numeric" placeholder="JAGA BANGSAL" value="<?=isset($tarif->visite_umum_jaga)?$tarif->visite_umum_jaga:0?>">
                                </div>
                            </div>
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# B. VISITE DOKTER UMUM -->

        <!-- C. PASIEN RANAP DARI IGD -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h5>C. PASIEN RAWAT INAP DARI IGD</h5>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <!--                    -->
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <span>1. DOKTER UMUM</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="ranap_dari_igd_dokter" class="form-control input-sm numeric" placeholder="DOKTER UMUM" value="<?=isset($tarif->ranap_dari_igd_dokter)?$tarif->ranap_dari_igd_dokter:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <span>2. PERAWAT IGD</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="ranap_dari_igd_perawat" class="form-control input-sm numeric" placeholder="PERAWAT IGD" value="<?=isset($tarif->ranap_dari_igd_perawat)?$tarif->ranap_dari_igd_perawat:0?>">
                                </div>
                            </div>
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# C. PASIEN RANAP DARI IGD -->

        <!-- D. ASUHAN KEPERAWATAN -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h5>D. ASUHAN KEPWRAWATAN <span style="text-align:right">(96% PERAWAT, 4% WAKTUM)</span></h5>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <!---->
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;ICU / VIP</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="askep_0" class="form-control input-sm numeric" placeholder="(ICU / VIP)" value="<?=isset($tarif->askep_0)?$tarif->askep_0:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;KELAS I</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="askep_1" class="form-control input-sm numeric" placeholder="(KELAS I)" value="<?=isset($tarif->askep_1)?$tarif->askep_1:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;KELAS II</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="askep_2" class="form-control input-sm numeric" placeholder="(KELAS II)" value="<?=isset($tarif->askep_2)?$tarif->askep_2:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;KELAS III</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="askep_3" class="form-control input-sm numeric" placeholder="(KELAS III)" value="<?=isset($tarif->askep_3)?$tarif->askep_3:0?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <span>-&emsp;PERI / ISOLASI</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="askep_4" class="form-control input-sm numeric" placeholder="(PERI / ISO)" value="<?=isset($tarif->askep_4)?$tarif->askep_4:0?>">
                                </div>
                            </div>
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# C. PASIEN RANAP DARI IGD -->

        <!-- E. PASIEN RANAP DARI IGD -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h5>E. ASUHAN GIZI</h5>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <!--                    -->
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <span>1 kali selama pasien dirawat</span>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <div class="form-line">
                                    <input type="text" name="asgiz" class="form-control input-sm numeric" placeholder="Biaya Asuhan Gizi" value="<?=isset($tarif->asgiz)?$tarif->asgiz:0?>">
                                </div>
                            </div>
                        </div>

                        <!---->
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# E. PASIEN RANAP DARI IGD -->

        <!-- -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div class="row clearfix">
                        <!--                    -->
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:right;">
                            <button type="submit" class="btn btn-success btn-lg">Simpan Data&nbsp;<i class="fa fa-save"></i></button>
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# -->
    </form>
<script>

    $(document).ready(function(){

    });
    $('.numeric').keypress(function(e){
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            //    $("#errNom").html("*input haynya untuk digit angka saja!").show().fadeOut(2500);
            return false;
        }
    });
    $('#formTarif').submit(function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url:'<?=base_url('master_data/tarif_bpjs_irna_form')?>',
            data:$(this).serialize(),
            dataType:'JSON',
            success:function(data){
                console.log(data);
            }
        });
    });

</script>
