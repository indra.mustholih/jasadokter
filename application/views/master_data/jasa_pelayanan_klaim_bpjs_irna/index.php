<?php
/**
 * Created by PhpStorm.
 * User: Zaenal Alim Prayoga
 * Date: 31/10/2018
 * Time: 21.12
 */
?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><i class="fa fa-file-o"></i> <?=$title_panel?></h2>
                <!-- <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#umum" data-toggle="tab">Umum</a></li>
                    <li role="presentation"><a href="#bpjs" data-toggle="tab">BPJS</a></li>
                    <li role="presentation"><a href="#jmlain" data-toggle="tab">Jaminan Lainnya</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- start umum -->
                    <div role="tabpanel" class="tab-pane fade in active" id="umum">
                        <div>
                            <form class="" id="formUmum" style="width:50%;">
                                <div class="form-group">
                                    <label>Nama Dokter : </label>
                                    <select class="form-control select2" id="KdDokU" name="KdDokU" style="width:100%;">
                                        <option value="">-- Pilih Dokter --</option>
                                        <?php foreach ($dokter as $dr): ?>
                                            <option value="<?=$dr->KdDoc?>"><?=$dr->NmDoc?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Periode : </label>
                                    <div class="input-group">
                                        <div class="form-line">
                                            <input type="date" name="tgl_awalU" class="form-control " id="tgl_awalU">
                                        </div>
                                        <span class="input-group-addon">&nbsp;s/d&nbsp;</span>
                                        <div class="form-line">
                                            <input type="date" name="tgl_awalU" class="form-control " id="tgl_akhirU">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                                </div>
                            </form>
                        </div>
                        <hr>
                        <div id="targetHasilUmum" style="overflow:;"></div>
                    </div>
                    <!-- end umum -->
                    <div role="tabpanel" class="tab-pane fade" id="bpjs">
                        <div>
                            <form class="" id="formBpjs" style="width:50%;">
                                <div class="form-group">
                                    <label>Nama Dokter : </label>
                                    <select class="form-control select2" id="KdDokB" name="KdDokB" style="width:100%;">
                                        <option value="">-- Pilih Dokter --</option>
                                        <?php foreach ($dokter as $dr): ?>
                                            <option value="<?=$dr->KdDoc?>"><?=$dr->NmDoc?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Periode : </label>
                                    <div class="input-group">
                                        <div class="form-line">
                                            <input type="text" name="tgl_awalB" class="form-control datepicker" id="tgl_awalB">
                                        </div>
                                        <span class="input-group-addon">&nbsp;s/d&nbsp;</span>
                                        <div class="form-line">
                                            <input type="text" name="tgl_awalB" class="form-control datepicker" id="tgl_akhirB">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                                </div>
                            </form>
                        </div>
                        <hr>
                        <div id="targetHasilBpjs" style="overflow:;"></div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="jmlain">
                        <div>
                            <form class="" id="formJmLain" style="width:50%;">
                                <div class="form-group">
                                    <label>Nama Dokter : </label>
                                    <select class="form-control select2" id="KdDokL" name="KdDokL" style="width:100%;">
                                        <option value="">-- Pilih Dokter --</option>
                                        <?php foreach ($dokter as $dr): ?>
                                            <option value="<?=$dr->KdDoc?>"><?=$dr->NmDoc?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Periode : </label>
                                    <div class="input-group">
                                        <div class="form-line">
                                            <input type="text" name="tgl_awalL" class="form-control datepicker" id="tgl_awalL">
                                        </div>
                                        <span class="input-group-addon">&nbsp;s/d&nbsp;</span>
                                        <div class="form-line">
                                            <input type="text" name="tgl_awalL" class="form-control datepicker" id="tgl_akhirL">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                                </div>
                            </form>
                        </div>
                        <hr>
                        <div id="targetHasilJmLain" style="overflow:;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        // getHasil();
    });
    $('#formUmum').submit(function(e){
        e.preventDefault();
        KdDoc = $('#KdDokU').val();
        NmDoc = $('#KdDokU option:selected').text();
        tgl_awal = $('#tgl_awalU').val();
        tgl_akhir = $('#tgl_akhirU').val();
        if (KdDoc != '' && tgl_awal != '' && tgl_akhir != '') {
            getHasilUmum(KdDoc,NmDoc,tgl_awal,tgl_akhir);
        }else{
            $('#formUmum').before('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-warning"></i>Silahkan pilih dokter dan isi inputan yang ada!</div>');
        }

    });
    function getHasilUmum(kd='',nm='',tgl1='',tgl2='') {
        $.ajax({
            type:'POST',
            url:'<?=base_url('biling_dokter/rawat_umum_table')?>',
            data:{KdDoc:kd,NmDoc:nm,tgl_awal:tgl1,tgl_akhir:tgl2},
            beforeSend(){
                $('#targetHasilUmum').html('<div class="alert alert-info"><h3>Memuat Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
            },
            success(data){
                $('#targetHasilUmum').html(data);
            }
        });
    }

    $('#formBpjs').submit(function(e){
        e.preventDefault();
        KdDoc = $('#KdDokB').val();
        NmDoc = $('#KdDokB option:selected').text();
        tgl_awal = $('#tgl_awalB').val();
        tgl_akhir = $('#tgl_akhirB').val();
        if (KdDoc != '' && tgl_awal != '' && tgl_akhir != '') {
            getHasilBpjs(KdDoc,NmDoc,tgl_awal,tgl_akhir);
        }else{
            $('#formBpjs').before('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-warning"></i>Silahkan pilih dokter dan isi inputan yang ada!</div>');
        }

    });
    function getHasilBpjs(kd='',nm='',tgl1='',tgl2='') {
        $.ajax({
            type:'POST',
            url:'<?=base_url('biling_dokter/rawat_bpjs_table')?>',
            data:{KdDoc:kd,NmDoc:nm,tgl_awal:tgl1,tgl_akhir:tgl2},
            beforeSend(){
                $('#targetHasilBpjs').html('<div class="alert alert-info"><h3>Memuat Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
            },
            success(data){
                $('#targetHasilBpjs').html(data);
            }
        });
    }
    $('#formJmLain').submit(function(e){
        e.preventDefault();
        KdDoc = $('#KdDokL').val();
        NmDoc = $('#KdDokL option:selected').text();
        tgl_awal = $('#tgl_awalL').val();
        tgl_akhir = $('#tgl_akhirL').val();
        if (KdDoc != '' && tgl_awal != '' && tgl_akhir != '') {
            getHasilJmLain(KdDoc,NmDoc,tgl_awal,tgl_akhir);
        }else{
            $('#formJmLain').before('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-warning"></i>Silahkan pilih dokter dan isi inputan yang ada!</div>');
        }
    });
    function getHasilJmLain(kd='',nm='',tgl1='',tgl2='') {
        $.ajax({
            type:'POST',
            url:'<?=base_url('biling_dokter/rawat_jmlain_table')?>',
            data:{KdDoc:kd,NmDoc:nm,tgl_awal:tgl1,tgl_akhir:tgl2},
            beforeSend:function(){
                $('#targetHasilJmLain').html('<div class="alert alert-info"><h3>Memuat Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
            },
            success:function(data){
                $('#targetHasilJmLain').html(data);
            }
        });
    }
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });
    $('.select2').select2();
</script>
