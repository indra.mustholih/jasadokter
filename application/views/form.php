<!-- DATEPICKER -->
    <link href="<?php echo base_url()?>assets/datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- DATEPICKER -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- <style>
    th{
        text-align: center;
    }
    td{
        padding: 0px !important;
        text-align: center;
    }
</style> -->
<div class="row">
<div class="card">
    <ul class="nav nav-tabs tab-nav-right" role="tablist">
        <li role="presentation" class="active"><a href="#kosong" data-toggle="tab"> <i class="material-icons">hotel</i> RUANG KOSONG</a></li>
        <li role="presentation"><a href="#all" data-toggle="tab"><i class="material-icons">view_list</i> DAFTAR RUANG DAN PENCARIAN</a></li>
    </ul>
    <div class="body">
     <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="kosong">
            <div class="row clearfix">
            <div class="card">
                <div class="body bg-<?php echo substr($this->konfigurasi_model->listing()->tema,6,100)?>">
                    
                    <h3><i class="fa fa-bed"></i> DAFTAR RUANG KOSONG</h3>
                    <p><h4>Keterangan : <span class="label bg-orange">Hampir Penuh</span> <span class="label bg-cyan">Tersedia</span></h4></p>
                </div>
            </div>
            <div id="load">
                <!-- Loading GIF -->
                <div id="wait" style="display:none;border:0px solid black;position:absolute;top:100%;left:40%;padding:2px;"><img src='<?php echo base_url()?>assets/images/loading1.gif' width="100" height="100" /><br><h4>Sedang Mencari..<h4></div>
            </div>
            <!-- <?php //foreach ($kosong as $null): ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <?php //if($null['kosong'] < 4):?>
                <div class="info-box-2 bg-orange">
                <?php //else:?>   
                <div class="info-box-2 bg-blue">
                <?php //endif;?>
                        <div class="icon">
                            <i class="material-icons">hotel</i>
                        </div>
                        <div class="content">
                            <div class="text"><?php //echo $null['nmkelas']?></div>
                            <div class="number count-to" data-from="0" data-to="<?php //cho $null['kosong']?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                </div>
            </div>
            <?php //endforeach;?> -->
        </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="all">
            <div class="row clearfix">
                <!-- <div class="card"> -->
                   <!--  <div class="header bg-<?php //echo substr($this->konfigurasi_model->listing()->tema,6,100)?>">
                        <h2>
                            <i class="fa fa-search"></i><span> Form Pencarian</span>
                        </h2>
                    </div> -->   
                    <!-- <div class="body">
                        <form id="form-s">
                        <div class="row clearfix">
                            <div class="form-group hidden">
                                <label>Kelas</label>
                                <div class="form-line">
                                    <input type="text" id="kelas" class="form-control" placeholder="Masukan Kelas Yang Ingin Dicari">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nama Ruangan</label>
                                <div class="form-line">
                                    <input type="text" id="namakelas" class="form-control" placeholder="Masukan Nama Ruangan Yang Ingin Dicari">
                                </div>
                            </div>
                        </div>
                        </form>
                    </div> -->
                <!-- </div> -->
                
                <div class="header bg-<?php echo substr($this->konfigurasi_model->listing()->tema,6,100)?>">
                    <h2>
                        <i class="fa fa-bed"></i><span> Data Bed Management</span>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-md-12 col-lg-12">
                        <form id="form-s">
                            <div class="form-group hidden">
                                <label>Kelas</label>
                                <div class="form-line">
                                    <input type="text" id="kelas" class="form-control" placeholder="Masukan Kelas Yang Ingin Dicari">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nama Ruangan</label>
                                <div class="form-line">
                                    <input type="text" id="date" class="form-control" placeholder="Masukan Nama Ruangan Yang Ingin Dicari">
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div class="table-responsive">

                        <div class="row clearfix">
                        <div class="col-md-12 col-lg-12">
                        <h4>Keterangan : </h4>
                        <span class="text-info">Klik Button Jika Ingin Memfilter Sesuai Keterangan</span>
                        </div>
                        <span class="col-md-4 col-lg-3">
                        <button class="btn btn-info btn-sm btn-block" id="tersedia">Tersedia</button>
                            
                        </span>
                       
                        <span class="col-md-4 col-lg-3">
                        <button class="btn bg-orange btn-sm btn-block" id="mendekati">Hampir Penuh</button>
                        </span>

                         <span class="col-md-4 col-lg-3">
                        <button class="btn btn-danger btn-sm btn-block" id="habis">Penuh</button>
                        </span>

                        <span class="col-md-4 col-lg-3">
                        <button  class="btn bg-indigo btn-sm btn-block" id=reset><i class="fa fa-refresh"></i> Reset Filter</button>
                        </span>
                    </div>
                    <table class="table" class="table table-bordered table-striped" cellspacing="0" width="100%" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kelas</th>
                                <th>Ruangan</th>
                                <th width="5%">Kapasitas</th>
                                <th>Terisi</th>
                                <th width="20%">Status Ketersediaan</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    </div>
                </div>
                
                    <!-- /.box-body -->
                  <!-- /.box -->
           
            </div>
        </div>
    </div>   
    </div>
    
</div>
</div>
<script>
	$(document).ready(function(){
		$("#date").datepicker({
			language: "id",
			changeMonth: true,
    		changeYear: true,
			autoclose: true,
			todayHighlight : true,
			format: "yyyy-mm-dd",
		});
	});
</script>
  
