<style type="text/css">
	@page {
		size: 21.0cm 29.7cm; margin:1cm;
	}
	*{ font-size:12px; }
	#tblHs tr th {padding-right:10px;}
	#tblHs tr td {padding-right:10px;word-wrap: break-word;}
	.blb {border-right:1px solid black;border-bottom:1px solid black;}
	.bt {border-top:1px solid black;}
	.btr {border-right:1px solid black;border-top:1px solid black;}
	.br{border-right:1px solid black;}
	.bb{border-bottom:1px solid black;}
</style>

<div id="printDetail">
	<div style="border-bottom:2px solid black;width:45%;text-align:center;">
		<span style="text-align:center;">DINAS KESHATAN ANGKATAN UDARA</span><br>
		<span style="text-align:center;">RSPAU dr. HARDJOLUKITO</span>
	</div>
	<h4 style="text-align:center;">BIAYA PELAYANAN PASIEN RAWAT JALAN</h4>
	<hr>
	<?php 
	// echo "<pre>";print_r($way);echo "</pre>";die();
	if (!empty($ps)): ?>
        <table style="width:100%;border-collapse:collapse;" id="tblDt">
            <tr>
                <td style="text-align:left;width:15%;">No. Reg /  No. RM</td>
                <td style="text-align:left;width:20%;">: <?=$ps->Regno?> / <?=$ps->Medrec?></td>
                <td style="text-align:left;">Umur</td>
                <td style="text-align:left;width:15%;">: <?=$ps->UmurThn?> Thn</td>
            </tr>
            <tr>
                <td style="text-align:left;">Nama</td>
                <td style="text-align:left;">: <?=$ps->Firstname?></td>
                <td style="text-align:left;"></td>
                <td style="text-align:left;"></td>
            </tr>
            <tr>
                <td style="text-align:left;">Alamat</td>
                <td style="text-align:left;width:67%">: <?=$ps->Address?></td>
                <td style="text-align:left;"></td>
                <td style="text-align:left;"></td>
            </tr>
        </table>
	<?php 
		$tglKel = isset($ps->Tanggal) ? $ps->Tanggal : date('Y-m-d');
	?>
	<table style="width:100%;border-collapse:collapse;">
		<tr>
			<td style="text-align:left;width:15%;">No. Telepon</td><td style="text-align:left;">: <?=$ps->Phone?></td>
			<td style="text-align:left;"></td><td style="text-align:left;"></td>
		</tr>
        <?php if($way == 'ranap'):?>
            <tr>
                <td style="text-align:left;">Ruangan / Kelas</td><td style="text-align:left;">: <?=$ps->NmBangsal?> / <?=$ps->NMKelas?></td>
                <td style="text-align:right;">Tanggal</td><td style="text-align:left;">: <?=date('d/m/Y',strtotime($ps->Regdate))?> - <?=date('d/m/Y',strtotime($tglKel))?></td>
            </tr>
        <?php elseif($way == 'rajal'):?>
            <tr>
                <td style="text-align:left;">Poli</td><td style="text-align:left;">: <?=$ps->NMPoli?></td>
                <td style="text-align:right;">Tanggal</td><td style="text-align:left;">: <?=date('d/m/Y',strtotime($ps->Regdate))?> </td>
            </tr>
        <?php endif;?>

		<tr>
			<td style="text-align:left;">Cara Bayar</td><td style="text-align:left;">: <?=$ps->GroupUnit?></td>
			<td style="text-align:left;"></td><td style="text-align:left;"></td>
		</tr>
	</table>
	<br>

    <?php if ($way == 'ranap'):?>

    <?php elseif ($way == 'rajal'):?>
        <?php 
            $tbrj=$tpoa=$tumuk=0;
        ?>
        <div class="">
            <table style="width:100%;margin:auto;border-collapse:collapse;border:1px solid black;" class="" id="tblHs">
                <tr>
                    <th class="blb" rowspan="2" style="text-align:center;width:2.5%">NO</th>
                    <th class="blb" rowspan="2" style="text-align:center;width:35%;">URAIAN</th>
                    <th class="blb" rowspan="2" style="text-align:center;width:2%">RR</th>
                    <th class="br" colspan="2" style="text-align:center;width:15%;">BIAYA</th>
                </tr>
                <tr>
                    <td class="bb"></td>
                    <td class="bb"></td>
                </tr>
                <!-- HASIL  RAWAT JALAN -->
                <tr>
                    <td class="br" style="text-align:right;">1 &nbsp;</td>
                    <td class="br">&nbsp;HASIL RAWAT JALAN</td>
                    <td class="br"></td>
                    <td style="text-align:right;"></td>
                    <td></td>
                </tr>
                <?php foreach ($irj->irj as $irj): $jb = isset($irj->JumlahBiaya)?$irj->JumlahBiaya:0;?>
                    <tr>
                        <td class="br"></td>
                        <td class="br">&nbsp;-&nbsp;<?=$irj->NmTarif?>  <?= isset($irj->NmDoc) ? '- '.  $irj->NmDoc : ''  ?><span style="float:right;"></span></td>
                        <td class="br" style="text-align:center;"></td>
                        <td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
                        <td></td>
                    </tr>
                <?php $tbrj+=$jb;endforeach;?>
                <tr>
                    <td class="br"></td>
                    <td class="br"></td>
                    <td class="br"></td>
                    <td></td>
                    <td style="text-align:right;"><?=$this->cl->formatUang($tbrj,2)?></td>
                </tr>
                <!-- HASIL PEMAKAIAN OBAT / APOTIK -->
                <tr>
                    <td class="br" style="text-align:right;">2 &nbsp;</td>
                    <td class="br">&nbsp;PEMAKAIAN OBAT / APOTIK</td>
                    <td class="br"></td>
                    <td></td>
                    <td></td>
                </tr>
                <!-- OBAT APOTEK -->
                <?php if (isset($apt->apt) && !empty($apt->apt)): ?>
                <tr>
                    <td class="br"></td>
                    <td class="br">&nbsp;- OBAT APOTIK</td>
                    <td class="br"></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php foreach ($apt->apt as $at): $jb = isset($at->JumlahHarga)?$at->JumlahHarga:0;?>
                <tr>
                    <td class="br"></td>
                    <td class="br">&nbsp;&nbsp;&nbsp;-&nbsp;<?=$at->NamaObat?><span style="float:right;"></span></td>
                    <td class="br" style="text-align:center;"></td>
                    <td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
                    <td></td>
                </tr>
                <?php $tpoa+=$jb;endforeach;?>
                <tr>
                    <td class="br"></td>
                    <td class="br"></td>
                    <td class="br"></td>
                    <td></td>
                    <td style="text-align:right;"><?=$this->cl->formatUang($tpoa,2)?></td>
                </tr>
                <?php endif ?>

                <?php 
                    $tot = $tbrj+$tpoa+$tumuk;

                    $adm = ((intval($tot)/100) * 5);
                    $selisih = ($tot - $tumuk ) + $adm;

                ?>
                <!-- potong Uang Muka -->
                <tr>
                    <td class="bt"></td>
                    <td class="bt" style="text-align:right;">JUMLAH BIAYA </td>
                    <td class="btr" style="text-align:center;">:</td>
                    <td colspan="2" class="bt" style="text-align:right;"><?=$this->cl->formatUang($tot,2)?></td>
                </tr>
                <tr>
                    <td class="bt"></td>
                    <td class="bt" style="text-align:right;">BIAYA ADMINISTRASI </td>
                    <td class="btr" style="text-align:center;">:</td>
                    <td colspan="2" class="bt" style="text-align:right;"><?=$this->cl->formatUang($adm,2)?></td>
                </tr>
                <tr>
                    <td class=""></td>
                    <td class="" style="text-align:right;">UANG MUKA</td>
                    <td class="br" style="text-align:center;">:</td>
                    <td colspan="2" class="br" style="text-align:right;"><?=$this->cl->formatUang($tumuk,2)?></td>
                </tr>
                <tr>
                    <td class=""></td>
                    <td style="text-align:right;">SELISIH</td>
                    <td class="br" style="text-align:center;">:</td>
                    <td colspan="2" class="br" style="text-align:right;"><?=$this->cl->formatUang($selisih,2)?></td>
                </tr>
                <tr>
                    <td class=""></td>
                    <td class="br"></td>
                    <td class="br"></td>
                    <td colspan="2" class="br"></td>
                </tr>
                <tr>
                    <td colspan="2" class="bt" style="text-align: left;">BIAYA YANG HARUS DIBAYAR : </td>
                    <td class="btr"></td>
                    <td colspan="2" class="bt" style="text-align:right;"><?=$this->cl->formatUang($selisih,2)?></td>
                </tr>
                <tr>
                    <td class="">TERBILANG</td>
                    <td class="" style="text-align:right;">: # <?=strtolower($this->cl->terbilang($selisih,2))?> #</td>
                    <td class="br"></td>
                    <td colspan="2" class=""></td>
                </tr>
            </table>
            <br>
            <table style="width:100%;">
                <tr>
                    <td style="width:50%;text-align:center;">Tandatangan Pasien</td>
                    <?php $b = date('n');$date = date('d').' '.$bln[$b].' '.date('Y');?>
                    <td style="width:50%;text-align:center;">Yogyakarta, <?=$date?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <br><br><br><br>
        </div>
    <?php endif;?>

</div>
	<?php if ($set!='pdf'): ?>
		<hr>
		<div>
			<button type="button" class="btn btn-primary" id="btnPrintDetail"><i class="fa fa-save"></i> Simpan</button>
		</div>
	<?php endif ?>
	<?php else: ?>
		<div class="alert alert-warning"><h3>Nomor Registrasi Tidak Ditemukan!</h3></div>
	<?php endif ?>
<script type="text/javascript">
	var regno = '<?=$regno?>';
	$('#btnPrintDetail').click(function(){
		save_Biling(regno);
		$('#btnGetBilIrna').empty();
		$('#btnGetBilIrna').attr('stat','1');
		$('#btnGetBilIrna').html('<i class="fa fa-file"></i> Ambil Biling');
		$('#existBiling').show();
	});
	function save_Biling(regno='') {
		$.ajax({
            type:'POST',
            url:'<?=base_url('BerkasClime/laod_BilingRanap_form')?>',
            data:{regno:regno,format:'pdf'},
            dataType:'JSON',
            beforeSend(){
                $('#targetBilIrna').html('<div class="alert alert-info"><h3>Menyimpan Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
            },
            success(resp){
                laodBilingRanap(resp.regno);
            }
        });
	}
</script>