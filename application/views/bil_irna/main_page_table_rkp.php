<div id="printDetail">
	<div style="border-top:2px solid black;border-bottom:2px solid black;width:45%;text-align:center;">
		<span style="text-align:center;">DINAS KESHATAN ANGKATAN UDARA</span><br>
		<span style="text-align:center;">RSPAU dr. S. HARDJOLUKITO</span>
	</div>
	<h4 style="text-align:center;">BIAYA PELAYANAN PASIEN RAWAT INAP</h4>
	<hr>
	<?php 
	// echo "<pre>";print_r($apt->di);echo "</pre>";die();
	if (!empty($ps)): ?>
	<table style="width:100%;border-collapse:collapse;">
		<tr>
			<td style="text-align:left;width:18%;">No. Reg /  No. RM</td>
			<td style="text-align:left;">: <?=$ps->Regno?> / <?=$ps->Medrec?></td>
			<td style="text-align:left;">Umur</td>
			<td style="text-align:left;">: <?=$ps->UmurThn?> Thn</td>
		</tr>
		<tr>
			<td style="text-align:left;">Nama</td>
			<td style="text-align:left;">: <?=$ps->Firstname?></td>
			<td style="text-align:left;"></td>
			<td style="text-align:left;"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Alamat</td>
			<td style="text-align:left;width:67%">: <?=$ps->Address?></td>
			<td style="text-align:left;"></td>
			<td style="text-align:left;"></td>
		</tr>
	</table>
	<?php 
		$tglKel = isset($ps->Tanggal) ? $ps->Tanggal : date('Y-m-d');
	?>
	<table style="width:100%;border-collapse:collapse;">
		<tr>
			<td style="text-align:left;width:18%;">No. Telepon</td><td style="text-align:left;">: <?=$ps->Phone?></td>
			<td style="text-align:left;"></td><td style="text-align:left;"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Ruangan / Kelas</td><td style="text-align:left;">: <?=$ps->NmBangsal?> / <?=$ps->NMKelas?></td>
			<td style="text-align:left;">Tanggal</td><td style="text-align:left;">: <?=date('d/m/Y',strtotime($ps->Regdate))?> - <?=date('d/m/Y',strtotime($tglKel))?></td>
		</tr>
		<tr>
			<td style="text-align:left;">Cara Bayar</td><td style="text-align:left;">: <?=$ps->GroupUnit?></td>
			<td style="text-align:left;"></td><td style="text-align:left;"></td>
		</tr>
	</table>
	<br>
	<?php 
		$tfr=$tfrp=$tpr=$tibs=$tlab=$trad=$thd=$tkt=$tpl=$tpoa=$tpodi=$tpodu=$tpodo=$tbak=$tbu=$tbj=$tfi=$tvd=$tjp=$tapj=$tpmi=$tadm=$tak=$tag=0;
		$lmInap = $this->cl->selisihHari($tglKel,$ps->Regdate)+1;
		$lmInapP = $ps->LamaPindah;
		$lmInapA = $lmInap-$lmInapP;
		$tfr = (float)$lmInapA * (float)$ps->ByTarif;
		$tfrp = (float)$lmInapP * (float)$ps->TarifPindah;
	?>
	<style type="text/css">
		#tblHs tr th {padding-right:10px;}
		#tblHs tr td {padding-right:10px;}
		.blb {border-right:1px solid black;border-bottom:1px solid black;}
		.bt {border-top:1px solid black;}
		.btr {border-right:1px solid black;border-top:1px solid black;}
		.br{border-right:1px solid black;}
		.bb{border-bottom:1px solid black;}
	</style>
	<div class="">
		<table style="width:;margin:auto;border-collapse:collapse;border:1px solid black;" class="" id="tblHs">
			<tr>
				<th class="blb" rowspan="2" style="text-align:center;">NO</th>
				<th class="blb" rowspan="2" style="text-align:center;">URAIAN</th>
				<th class="blb" rowspan="2" style="text-align:center;">RR</th>
				<th class="br" colspan="2" style="text-align:center;width:20%;">BIAYA</th>
			</tr>
			<tr>
				<td class="bb"></td>
				<td class="bb"></td>
			</tr>
			<!-- FASILITAS RUANGAN -->
			<tr>
				<td class="br" style="text-align:right;widtd:5%;">1 &nbsp;</td>
				<td class="br">&nbsp;RUANGAN RAWAT</td>
				<td class="br"></td>
				<td class=""></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tfr+$tfrp,2)?></td>
			</tr>
			<!-- HASIL PERTOLONGAN PERSALINAN -->
			<!-- <tr>
				<td class="br" style="text-align:right;width:5%;">2 &nbsp;</td>
				<td class="br">&nbsp;PERTOLONGAN PERSALINAN</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($irna as $in):if($in->KdGroup == '09'):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tpr+=$jb;endif;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpr,2)?></td>
			</tr> -->
			<!-- HASIL OPERASI -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">2 &nbsp;</td>
				<td class="br">&nbsp;OPERASI</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($ibs->opr as $ibs): $jb = isset($ibs->JumlahBiaya)?$ibs->JumlahBiaya:0;?>
				<?php $tibs+=$jb;endforeach ?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tibs,2)?></td>
			</tr>
			<!-- HASIL LABORATORIUM -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">3 &nbsp;</td>
				<td class="br">&nbsp;LABORATORIUM</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($lab->lab as $lab):$jb = isset($lab->JumlahBiaya)?$lab->JumlahBiaya:0;?>
				<?php $tlab+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tlab,2)?></td>
			</tr>
			<!-- HASIL RADIOLOGI -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">4 &nbsp;</td>
				<td class="br">&nbsp;RADIOLOGI</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($rad->rad as $rad):$jb = isset($rad->JumlahBiaya)?$rad->JumlahBiaya:0;?>
				<?php $trad+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($trad,2)?></td>
			</tr>
			<!-- HASIL HEMODIALISA -->
			<!-- <tr>
				<td class="br" style="text-align:right;width:5%;">6 &nbsp;</td>
				<td class="br">&nbsp;HEMODIALISA</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($hd as $hd): $jb = isset($hd->JumlahBiaya)?$hd->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;<?=$hd->NmTarif?><span style="float:right;"><?=isset($hd->Tanggal)?date('d/m/Y',strtotime($hd->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$hd->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $thd+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($thd,2)?></td>
			</tr> -->
			<!-- HASIL KEMOTERAPI -->
			<!-- <tr>
				<td class="br" style="text-align:right;width:5%;">7 &nbsp;</td>
				<td class="br">&nbsp;KEMOTERAPI</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($irna as $in):if($in->KdGroup == '12'):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tkt+=$jb;endif;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tkt,2)?></td>
			</tr> -->
			<!-- HASIL PENUNJANG LAINNYA -->
			<!-- <tr>
				<td class="br" style="text-align:right;width:5%;">8 &nbsp;</td>
				<td class="br">&nbsp;PENUNJANG LAINNYA</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br" style="text-align:center;"></td>
				<td style="text-align:right;">0</td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;">0</td>
			</tr> -->
			<!-- HASIL PEMAKAIAN OBAT / APOTIK -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">5 &nbsp;</td>
				<td class="br">&nbsp;PEMAKAIAN OBAT / APOTIK</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<!-- OBAT APOTEK -->
			<?php if (isset($apt->apt) && !empty($apt->apt)): ?>
			<tr>
				<td class="br" style="text-align:right;width:5%;">&nbsp;</td>
				<td class="br">&nbsp;- OBAT APOTIK</td>
				<td class="br"></td>
				<td></td>
				<?php foreach ($apt->apt as $at): $jb = isset($at->JumlahHarga)?$at->JumlahHarga:0;?>
				<?php $tpoa+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpoa,2)?></td>
			</tr>
			<?php endif ?>
			<!-- OBAT DEPO IGD -->
			<?php if (isset($apt->du) && !empty($apt->du)): ?>
			<tr>
				<td class="br" style="text-align:right;width:5%;">&nbsp;</td>
				<td class="br">&nbsp;- OBAT DEPO IGD</td>
				<td class="br"></td>
				<td></td>
				<?php foreach ($apt->du as $au): $jb = isset($au->JumlahHarga)?$au->JumlahHarga:0;?>
				<?php $tpodu+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpodu,2)?></td>
			</tr>
			<?php endif ?>
			<!-- OBAT DEPO IRNA -->
			<?php if (isset($apt->di) && !empty($apt->di)): ?>
			<tr>
				<td class="br" style="text-align:right;width:5%;">&nbsp;</td>
				<td class="br">&nbsp;- OBAT DEPO IRNA</td>
				<td class="br"></td>
				<td></td>
				<?php foreach ($apt->di as $ai): $jb = isset($ai->JumlahHarga)?$ai->JumlahHarga:0;?>
				<?php $tpodi+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpodi,2)?></td>
			</tr>
			<?php endif; ?>
			<!-- OBAT DEPO OK -->
			<?php if (isset($apt->do) && !empty($apt->do)): ?>
			<tr>
				<td class="br" style="text-align:right;width:5%;">&nbsp;</td>
				<td class="br">&nbsp;- OBAT DEPO OK</td>
				<td class="br"></td>
				<td></td>
				<?php foreach ($apt->do as $ao): $jb = isset($ao->JumlahHarga)?$ao->JumlahHarga:0;?>
				<?php $tpodo+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpodo,2)?></td>
			</tr>
			<?php endif; ?>
			<!-- HASIL PEMAKAIAN BAHAN / ALAT KESEHATAN -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">6 &nbsp;</td>
				<td class="br">&nbsp;PEMAKAIAN BAHAN / ALAT KESEHATAN</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($irna->bak as $in): $jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<?php $tbak+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tbak,2)?></td>
			</tr>
			<!-- HASIL TINDAKAN UGD -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">7 &nbsp;</td>
				<td class="br">&nbsp;TINDAKAN UGD</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($ugd->ugd as $u): $jb = isset($u->JumlahBiaya)?$u->JumlahBiaya:0;?>
				<?php $tbu+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tbu,2)?></td>
			</tr>
			<!-- HASIL RAWAT JALAN -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">8 &nbsp;</td>
				<td class="br">&nbsp;RAWAT JALAN <?='- POLI '.strtoupper($ps->NMPoli)?></td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($irj->irj as $ij): $jb = isset($ij->JumlahBiaya)?$ij->JumlahBiaya:0;?>
				<?php $tbj+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tbj,2)?></td>
			</tr>
			<!-- HASIL FISIOTERAPI -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">9 &nbsp;</td>
				<td class="br">&nbsp;FISIOTERAPI</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($fis->fis as $fi): $jb = isset($fi->JumlahBiaya)?$fi->JumlahBiaya:0;?>
				<?php $tfi+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tfi,2)?></td>
			</tr>
			<!-- HASIL JASA VISIT DOKTER -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">10 &nbsp;</td>
				<td class="br">&nbsp;VISIT DOKTER</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($irna->visite as $in): $jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<?php $tvd+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tvd,2)?></td>
			</tr>
			<!-- HASIL JASA KEPERAWATAN -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">11 &nbsp;</td>
				<td class="br">&nbsp;TINDAKAN RUANGAN</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($irna->jaskep as $in): $jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<?php $tjp+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tjp,2)?></td>
			</tr>
			<!-- HASIL ADMINISTRASI -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">12 &nbsp;</td>
				<td class="br">&nbsp;ADMINISTRASI</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<?php foreach ($irna->adm as $in): $jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<?php $tadm+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tadm,2)?></td>
			</tr>
			<!-- HASIL AMBULAN DAN PEMULASARAN JENAZAH -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">13 &nbsp;</td>
				<td class="br">&nbsp;AMBULAN DAN PEMULASARAN JENAZAH</td>
				<td class="br"></td>
				<td></td>
				<?php foreach ($irna->adpj as $in): $jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<?php $tapj+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tapj,2)?></td>
			</tr>
			<!-- HASIL PMI -->
			<!-- <tr>
				<td class="br" style="text-align:right;width:5%;">14 &nbsp;</td>
				<td class="br">&nbsp;PMI</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br" style="text-align:center;"></td>
				<td style="text-align:right;">0</td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;">0</td>
			</tr> -->
			<!-- HASIL ASKEP -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">14 &nbsp;</td>
				<td class="br">&nbsp;JASA PERAWAT</td>
				<td class="br"></td>
				<td></td>
				<?php foreach ($irna->askep as $in):$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<?php $tak+=$jb;endforeach;?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tak,2)?></td>
			</tr>
			<!-- HASIL ASUHAN GIZI -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">15 &nbsp;</td>
				<td class="br">&nbsp;ASUHAN GIZI</td>
				<td class="br"></td>
				<td></td>
				<?php $tag = (float)$lmInap *5000;?>
				<td class="br" style="text-align:right;"><?=$this->cl->formatUang($tag,2)?></td>
			</tr>
			<?php 
				$tot = $tfr+$tfrp+$tpr+$tibs+$tlab+$trad+$thd+$tkt+$tpl+$tpoa+$tpodi+$tpodu+$tpodo+$tbak+$tbu+$tbj+$tfi+$tvd+$tjp+$tapj+$tpmi+$tadm+$tak+$tag;
			?>
			<tr>
				<td class="bt">JUMLAH</td>
				<td class="btr">: </td>
				<td class="btr"></td>
				<td colspan="2" class="bt" style="text-align:right;"><?=$this->cl->formatUang($tot,2)?></td>
			</tr>
			<tr>
				<td class="">TERBILANG</td>
				<td class="br">: # <?=strtolower($this->cl->terbilang($tot,2))?> #</td>
				<td class="br"></td>
				<td colspan="2" class=""></td>
			</tr>
		</table>
		<br>
		<table style="width:100%;">
			<tr>
				<td style="width:50%;text-align:center;">Tandatangan Pasien</td>
				<?php $b = date('n');$date = date('d').' '.$bln[$b].' '.date('Y');?>
				<td style="width:50%;text-align:center;">Yogyakarta, <?=$date?><br>Pembantu Bendahara</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
		<br><br><br><br>
	</div>
</div>
<?php if ($set != 'pdf'): ?>
	<hr>
	<div>
		<button type="button" class="btn btn-primary" id="btnPrintDetail"><i class="fa fa-print"></i> Cetak</button>
	</div>
	<?php else: ?>
		<div class="alert alert-warning"><h3>Nomor Registrasi Tidak Ditemukan!</h3></div>
	<?php endif ?>
	<script type="text/javascript">
		var regno = '<?=$ps->Regno?>';
	$('#btnPrintDetail').click(function(){
		// printJS({
		// 	printable: 'printRekap',
		// 	type: 'html',
		// 	// header: 'PrintJS - Form Element Selection'
		// });
		// $('#printDetail').printElement();
	});
	
</script>
<?php endif ?>