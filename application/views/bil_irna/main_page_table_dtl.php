<style type="text/css">
	*{
		font-size:12px;
	}
	#tblHs tr th {padding-right:10px;}
	#tblHs tr td {padding-right:10px;word-wrap: break-word;}
	.blb {border-right:1px solid black;border-bottom:1px solid black;}
	.bt {border-top:1px solid black;}
	.btr {border-right:1px solid black;border-top:1px solid black;}
	.br{border-right:1px solid black;}
	.bb{border-bottom:1px solid black;}
</style>

<div id="printDetail">
	<div style="border-top:2px solid black;border-bottom:2px solid black;width:45%;text-align:center;">
		<span style="text-align:center;">DINAS KESHATAN ANGKATAN UDARA</span><br>
		<span style="text-align:center;">RSPAU dr. S. HARDJOLUKITO</span>
	</div>
	<h4 style="text-align:center;">BIAYA PELAYANAN PASIEN RAWAT INAP</h4>
	<hr>
	<?php 
	// echo "<pre>";print_r($apt->di);echo "</pre>";die();
	if (!empty($ps)): ?>
	<table style="width:100%;border-collapse:collapse;" id="tblDt">
		<tr>
			<td style="text-align:left;width:15%;">No. Reg /  No. RM</td>
			<td style="text-align:left;width:20%;">: <?=$ps->Regno?> / <?=$ps->Medrec?></td>
			<td style="text-align:left;">Umur</td>
			<td style="text-align:left;width:15%;">: <?=$ps->UmurThn?> Thn</td>
		</tr>
		<tr>
			<td style="text-align:left;">Nama</td>
			<td style="text-align:left;">: <?=$ps->Firstname?></td>
			<td style="text-align:left;"></td>
			<td style="text-align:left;"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Alamat</td>
			<td style="text-align:left;width:67%">: <?=$ps->Address?></td>
			<td style="text-align:left;"></td>
			<td style="text-align:left;"></td>
		</tr>
	</table>
	<?php 
		$tglKel = isset($ps->Tanggal) ? $ps->Tanggal : date('Y-m-d');
	?>
	<table style="width:100%;border-collapse:collapse;">
		<tr>
			<td style="text-align:left;width:15%;">No. Telepon</td><td style="text-align:left;">: <?=$ps->Phone?></td>
			<td style="text-align:left;"></td><td style="text-align:left;"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Ruangan / Kelas</td><td style="text-align:left;">: <?=$ps->NmBangsal?> / <?=$ps->NMKelas?></td>
			<td style="text-align:right;">Tanggal</td><td style="text-align:left;">: <?=date('d/m/Y',strtotime($ps->Regdate))?> - <?=date('d/m/Y',strtotime($tglKel))?></td>
		</tr>
		<tr>
			<td style="text-align:left;">Cara Bayar</td><td style="text-align:left;">: <?=$ps->GroupUnit?></td>
			<td style="text-align:left;"></td><td style="text-align:left;"></td>
		</tr>
	</table>
	<br>
	<?php 
		$tfr=$tfrp=$tpr=$tibs=$tlab=$trad=$thd=$tkt=$tpl=$tpoa=$tpodi=$tpodu=$tpodo=$tbak=$tbu=$tbj=$tfi=$tvd=$tjp=$tapj=$tpmi=$tadm=$tak=$tag=$tumuk=0;
		$lmInap = $this->cl->selisihHari($tglKel,$ps->Regdate)+1;
		$lmInapP = $ps->LamaPindah;
		$lmInapA = $lmInap-$lmInapP;
		if (substr(strtoupper(str_replace(' ', '', $ps->KelasPindah)), 0,8) != 'NONKELAS') {
			$tfr = (float)$lmInapA * (float)$ps->ByTarif;
			$tfrp = (float)$lmInapP * (float)$ps->TarifPindah;
		}else{
			$tfr = (float)$lmInap * (float)$ps->ByTarif;
		}
	?>
	<div class="">
		<table style="width:100%;margin:auto;border-collapse:collapse;border:1px solid black;" class="" id="tblHs">
			<tr>
				<th class="blb" rowspan="2" style="text-align:center;width:2.5%">NO</th>
				<th class="blb" rowspan="2" style="text-align:center;width:35%;">URAIAN</th>
				<th class="blb" rowspan="2" style="text-align:center;width:2%">RR</th>
				<th class="br" colspan="2" style="text-align:center;width:15%;">BIAYA</th>
			</tr>
			<tr>
				<td class="bb"></td>
				<td class="bb"></td>
			</tr>
			<!-- FASILITAS RUANGAN -->
			<tr>
				<td class="br" style="text-align:right;widtd:2%;">1 &nbsp;</td>
				<td class="br">&nbsp;RUANGAN RAWAT</td>
				<td class="br"></td>
				<td class=""></td>
				<td class=""></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br" >&nbsp;-&nbsp;<?=$ps->NmBangsal?> / <?=$ps->NMKelas?></td>
				<td class="br" style="text-align:center;"><?=$ps->KdBangsal?></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tfr,2)?></td>
				<td></td>
			</tr>
			<?php if ($ps->KdBangsalPindah): ?>
			<tr>
				<td class="br"></td>
				<td class="br" >&nbsp;-&nbsp;<?=$ps->BangsalPindah?> / <?=$ps->KelasPindah?></td>
				<td class="br" style="text-align:center;"><?=$ps->KdBangsalPindah?></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tfrp,2)?></td>
				<td></td>
			</tr>
			<?php endif ?>
			<tr>
				<td class="br" ></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tfr+$tfrp,2)?></td>
			</tr>
			<!-- HASIL PERTOLONGAN PERSALINAN -->
			<!-- <tr>
				<td class="br" style="text-align:right;width:5%;">2 &nbsp;</td>
				<td class="br">&nbsp;PERTOLONGAN PERSALINAN</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($irna as $in):if($in->KdGroup == '09'):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tpr+=$jb;endif;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpr,2)?></td>
			</tr> -->
			<!-- HASIL OPERASI -->
			<tr>
				<td class="br" style="text-align:right;">2 &nbsp;</td>
				<td class="br">&nbsp;OPERASI</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($ibs->opr as $op): $jb = isset($op->JumlahBiaya)?$op->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$op->NamaTindakan?>	<span style="float:right;"><?=$this->cl->convDate($op->Tanggal,7)?></span></td>
					<td class="br" style="text-align:center;"><?=$op->KdBangsal?></td>
					<td style="text-align:right;"></td>
					<td></td>
				</tr>
			<?php foreach ($ibs->dtl as $key => $value): if($value->Notran && $value->Notran == $op->Notran):?>
					<?php $tos=0;foreach ($value as $key => $value):if($key!='Notran'):?>
					<tr>
						<td class="br"></td>
						<td class="br">&nbsp;&nbsp;&nbsp;-&nbsp;<?=str_replace('_', ' ', $key)?></td>
						<td class="br"></td>
						<td style="text-align:right;"><?=$this->cl->formatUang($value,2)?></td>
						<td></td>
					</tr>
					<?php $tos+=$value;endif;endforeach ?>
					<tr>
						<td class="br"></td>
						<td class="br"></td>
						<td class="br"></td>
						<td></td>
						<td style="text-align:right;"><?=$this->cl->formatUang($tos,2)?></td>
					</tr>
					<?php endif;endforeach;?>
			<?php $tibs+=$jb;endforeach ?>
			<!-- <tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tibs,2)?></td>
			</tr> -->
			<!-- HASIL LABORATORIUM -->
			<tr>
				<td class="br" style="text-align:right;">3 &nbsp;</td>
				<td class="br">&nbsp;LABORATORIUM</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($lab->lab as $lab):$jb = isset($lab->JumlahBiaya)?$lab->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$lab->NmTarif?><span style="float:right;"><?=isset($lab->Tanggal)? date('d/m/Y',strtotime($lab->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tlab+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tlab,2)?></td>
			</tr>
			<!-- HASIL RADIOLOGI -->
			<tr>
				<td class="br" style="text-align:right;">4 &nbsp;</td>
				<td class="br">&nbsp;RADIOLOGI</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($rad->rad as $rad):$jb = isset($rad->JumlahBiaya)?$rad->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$rad->NmTarif?><span style="float:right;"><?=isset($rad->Tanggal)? date('d/m/Y',strtotime($rad->Tanggal)):''?></span></td>
					<td class="br"></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $trad+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($trad,2)?></td>
			</tr>
			<!-- HASIL HEMODIALISA -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">6 &nbsp;</td>
				<td class="br">&nbsp;HEMODIALISA</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($hemo->hd as $hm): $jb = isset($hm->JumlahBiaya)?$hm->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;<?=$hm->NmTarif?><span style="float:right;"><?=isset($hm->Tanggal)?date('d/m/Y',strtotime($hm->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$hm->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $thd+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($thd,2)?></td>
			</tr>
			<!-- HASIL KEMOTERAPI -->
			<tr>
				<td class="br" style="text-align:right;width:5%;">7 &nbsp;</td>
				<td class="br">&nbsp;KEMOTERAPI</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($irna->hmd as $in): $jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tkt+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tkt,2)?></td>
			</tr>
			<!-- HASIL PENUNJANG LAINNYA -->
			<!-- <tr>
				<td class="br" style="text-align:right;width:5%;">8 &nbsp;</td>
				<td class="br">&nbsp;PENUNJANG LAINNYA</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br" style="text-align:center;"></td>
				<td style="text-align:right;">0</td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;">0</td>
			</tr> -->
			<!-- HASIL PEMAKAIAN OBAT / APOTIK -->
			<tr>
				<td class="br" style="text-align:right;">5 &nbsp;</td>
				<td class="br">&nbsp;PEMAKAIAN OBAT / APOTIK</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<!-- OBAT APOTEK -->
			<?php if (isset($apt->apt) && !empty($apt->apt)): ?>
			<tr>
				<td class="br"></td>
				<td class="br">&nbsp;- OBAT APOTIK</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($apt->apt as $at): $jb = isset($at->JumlahHarga)?$at->JumlahHarga:0;?>
			<tr>
				<td class="br"></td>
				<td class="br">&nbsp;&nbsp;&nbsp;-&nbsp;<?=$at->NamaObat?><span style="float:right;"><?=isset($at->BLDate)?date('d/m/Y',strtotime($at->BLDate)):''?></span></td>
				<td class="br" style="text-align:center;"></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
				<td></td>
			</tr>
			<?php $tpoa+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpoa,2)?></td>
			</tr>
			<?php endif ?>
			<!-- OBAT DEPO IGD -->
			<!-- <?php if (isset($apt->du) && !empty($apt->du)): ?>
			<tr>
				<td class="br"></td>
				<td class="br">&nbsp;- OBAT DEPO IGD</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($apt->du as $au): $jb = isset($au->JumlahHarga)?$au->JumlahHarga:0;?>
			<tr>
				<td class="br"></td>
				<td class="br">&nbsp;&nbsp;&nbsp;-&nbsp;<?=$au->NamaObat?><span style="float:right;"><?=isset($au->BLDate)?date('d/m/Y',strtotime($au->BLDate)):''?></span></td>
				<td class="br" style="text-align:center;"></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
				<td></td>
			</tr>
			<?php $tpodu+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpodu,2)?></td>
			</tr>
			<?php endif ?> -->
			<!-- OBAT DEPO IRNA -->
			<!-- <?php if (isset($apt->di) && !empty($apt->di)): ?>
			<tr>
				<td class="br"></td>
				<td class="br">&nbsp;- OBAT DEPO IRNA</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($apt->di as $ai): $jb = isset($ai->JumlahHarga)?$ai->JumlahHarga:0;?>
			<tr>
				<td class="br"></td>
				<td class="br">&nbsp;&nbsp;&nbsp;-&nbsp;<?=$ai->NamaObat?><span style="float:right;"><?=isset($ai->BLDate)?date('d/m/Y',strtotime($ai->BLDate)):''?></span></td>
				<td class="br" style="text-align:center;"></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
				<td></td>
			</tr>
			<?php $tpodi+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpodi,2)?></td>
			</tr>
			<?php endif; ?> -->
			<!-- OBAT DEPO OK -->
			<!-- <?php if (isset($apt->do) && !empty($apt->do)): ?>
			<tr>
				<td class="br"></td>
				<td class="br">&nbsp;- OBAT DEPO OK</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($apt->do as $ao): $jb = isset($ao->JumlahHarga)?$ao->JumlahHarga:0;?>
			<tr>
				<td class="br"></td>
				<td class="br">&nbsp;&nbsp;&nbsp;-&nbsp;<?=$ao->NamaObat?><span style="float:right;"><?=isset($ao->BLDate)?date('d/m/Y',strtotime($ao->BLDate)):''?></span></td>
				<td class="br" style="text-align:center;"></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
				<td></td>
			</tr> -->
			<!-- <?php $tpodo+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tpodo,2)?></td>
			</tr>
			<?php endif; ?> -->
			<!-- HASIL PEMAKAIAN BAHAN / ALAT KESEHATAN -->
			<tr>
				<td class="br" style="text-align:right;">6 &nbsp;</td>
				<td class="br">&nbsp;PEMAKAIAN BAHAN / ALAT KESEHATAN</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($irna->bak as $in):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tbak+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tbak,2)?></td>
			</tr>
			<!-- HASIL TINDAKAN UGD -->
			<tr>
				<td class="br" style="text-align:right;">7 &nbsp;</td>
				<td class="br">&nbsp;TINDAKAN UGD</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($ugd->ugd as $u): $jb = isset($u->JumlahBiaya)?$u->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$u->NmTarif?><span style="float:right;"><?=isset($u->Tanggal)?date('d/m/Y',strtotime($u->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tbu+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tbu,2)?></td>
			</tr>
			<!-- HASIL RAWAT JALAN -->
			<tr>
				<td class="br" style="text-align:right;">8 &nbsp;</td>
				<td class="br">&nbsp;RAWAT JALAN <?='- POLI '.strtoupper($ps->NMPoli)?></td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($irj->irj as $ij):
			$jb = isset($ij->JumlahBiaya)?$ij->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;<?=$ij->NmTarif?><span style="float:right;"><?=isset($ij->Tanggal)?date('d/m/Y',strtotime($ij->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$ij->KdPoli:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tbj+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tbj,2)?></td>
			</tr>
			<!-- HASIL FISIOTERAPI -->
			<tr>
				<td class="br" style="text-align:right;">9 &nbsp;</td>
				<td class="br">&nbsp;FISIOTERAPI</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($fis->fis as $fi):
			$jb = isset($fi->JumlahBiaya)?$fi->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$fi->NmTarif?><span style="float:right;"><?=isset($fi->Tanggal)?date('d/m/Y',strtotime($fi->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$fi->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tfi+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tfi,2)?></td>
			</tr>
			<!-- HASIL JASA VISIT DOKTER -->
			<tr>
				<td class="br" style="text-align:right;">10 &nbsp;</td>
				<td class="br">&nbsp;VISIT DOKTER</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($irna->visite as $in):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$in->NmTarif?> - <?=$in->NmDoc?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tvd+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tvd,2)?></td>
			</tr>
			<!-- HASIL JASA KEPERAWATAN -->
			<tr>
				<td class="br" style="text-align:right;">11 &nbsp;</td>
				<td class="br">&nbsp;TINDAKAN RUANGAN</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($irna->jaskep as $in):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tjp+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tjp,2)?></td>
			</tr>
			<!-- HASIL ADMINISTRASI -->
			<tr>
				<td class="br" style="text-align:right;">12 &nbsp;</td>
				<td class="br">&nbsp;ADMINISTRASI</td>
				<td class="br"></td>
				<td style="text-align:right;"></td>
				<td></td>
			</tr>
			<?php foreach ($irna->adm as $in):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tadm+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tadm,2)?></td>
			</tr>
			<!-- HASIL AMBULAN DAN PEMULASARAN JENAZAH -->
			<tr>
				<td class="br" style="text-align:right;">13 &nbsp;</td>
				<td class="br">&nbsp;AMBULAN DAN PEMULASARAN JENAZAH</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($irna->adpj as $in):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tapj+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tapj,2)?></td>
			</tr>
			<!-- HASIL PMI -->
	<!-- 		<tr>
				<td class="br" style="text-align:right;width:5%;">14 &nbsp;</td>
				<td class="br">&nbsp;PMI</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br" style="text-align:center;"></td>
				<td style="text-align:right;">0</td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;">0</td>
			</tr> -->
			<!-- HASIL ASKEP -->
			<tr>
				<td class="br" style="text-align:right;">14 &nbsp;</td>
				<td class="br">&nbsp;JASA PERAWAT</td>
				<td class="br"></td>
				<td></td>
				<td></td>
			</tr>
			<?php foreach ($irna->askep as $in):
			$jb = isset($in->JumlahBiaya)?$in->JumlahBiaya:0;?>
				<tr>
					<td class="br"></td>
					<td class="br">&nbsp;-&nbsp;<?=$in->NmTarif?><span style="float:right;"><?=isset($in->Tanggal)?date('d/m/Y',strtotime($in->Tanggal)):''?></span></td>
					<td class="br" style="text-align:center;"><?=$jb!=0?$in->KdBangsal:''?></td>
					<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
					<td></td>
				</tr>
			<?php $tak+=$jb;endforeach;?>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tak,2)?></td>
			</tr>
			<!-- HASIL ASUHAN GIZI -->
			<tr>
				<td class="br" style="text-align:right;;">15 &nbsp;</td>
				<td class="br">&nbsp;ASUHAN GIZI</td>
				<td class="br"></td>
				<td></td>
				<td class="br"></td>
			</tr>
			<tr>
				<td class="br"></td>
				<?php if ($ps->Kategori == '1'): ?>
				<td class="br">&nbsp;-&nbsp;<?=$ps->NmBangsal?> / <?=$ps->NMKelas?> (<?=$lmInap?> hari) @ Rp 5,000.00</td>
				<?php $tag = (float)$lmInap *5000;?>
				<td class="br" style="text-align:center;"><?=$ps->KdBangsal?></td>
				<?php endif ?>
				<td style="text-align:right;"><?=$this->cl->formatUang($tag,2)?></td>
				<td></td>
			</tr>
			<tr>
				<td class="br"></td>
				<td class="br"></td>
				<td class="br"></td>
				<td></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($tag,2)?></td>
			</tr>
			<?php 
				$tot = $tfr+$tfrp+$tpr+$tibs+$tlab+$trad+$thd+$tkt+$tpl+$tpoa+$tpodi+$tpodu+$tpodo+$tbak+$tbu+$tbj+$tfi+$tvd+$tjp+$tapj+$tpmi+$tadm+$tak+$tag;
				$tumuk = isset($umuk->JumlahUang) ? $umuk->JumlahUang:0;
				$selisih = $tot - $tumuk;
			?>
			<!-- potong Uang Muka -->
			<tr>
				<td class="bt"></td>
				<td class="bt" style="text-align:right;">JUMLAH BIAYA </td>
				<td class="btr" style="text-align:center;">:</td>
				<td colspan="2" class="bt" style="text-align:right;"><?=$this->cl->formatUang($tot,2)?></td>
			</tr>
			<tr>
				<td class=""></td>
				<td class="" style="text-align:right;">UANG MUKA</td>
				<td class="br" style="text-align:center;">:</td>
				<td colspan="2" class="br" style="text-align:right;"><?=$this->cl->formatUang($tumuk,2)?></td>
			</tr>
			<tr>
				<td class=""></td>
				<td style="text-align:right;">SELISIH</td>
				<td class="br" style="text-align:center;">:</td>
				<td colspan="2" class="br" style="text-align:right;"><?=$this->cl->formatUang($selisih,2)?></td>
			</tr>
			<tr>
				<td class=""></td>
				<td class="br"></td>
				<td class="br"></td>
				<td colspan="2" class="br"></td>
			</tr>
			<tr>
		<!-- <td class="bt"></td> -->
				<td colspan="2" class="bt" style="text-align: left;">BIAYA YANG HARUS DIBAYAR : </td>
				<td class="btr"></td>
				<td colspan="2" class="bt" style="text-align:right;"><?=$this->cl->formatUang($selisih,2)?></td>
			</tr>
			<tr>
				<td class="">TERBILANG</td>
				<td class="" style="text-align:right;">: # <?=strtolower($this->cl->terbilang($selisih,2))?> #</td>
				<td class="br"></td>
				<td colspan="2" class=""></td>
			</tr>
		</table>
		<br>
		<table style="width:100%;">
			<tr>
				<td style="width:50%;text-align:center;">Tandatangan Pasien</td>
				<?php $b = date('n');$date = date('d').' '.$bln[$b].' '.date('Y');?>
				<td style="width:50%;text-align:center;">Yogyakarta, <?=$date?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
		<br><br><br><br>
	</div>
</div>
	<?php if ($set!='pdf'): ?>
		<hr>
		<div>
			<button type="button" class="btn btn-primary" id="btnPrintDetail"><i class="fa fa-print"></i> Cetak</button>
		</div>
	<?php endif ?>
	<?php else: ?>
		<div class="alert alert-warning"><h3>Nomor Registrasi Tidak Ditemukan!</h3></div>
	<?php endif ?>
	<script type="text/javascript">
	$('#btnPrintDetail').click(function(){
		var url = '<?=base_url('biling_ranap/biling_irna_table/').$ps->Regno?>'+'/pdf/dtl';
		window.open(url,'_blank');
		// printJS({
		// 	printable: 'printRekap',
		// 	type: 'html',
		// 	// header: 'PrintJS - Form Element Selection'
		// });
		// $('#printDetail').printElement();
	});
</script>