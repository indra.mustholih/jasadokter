<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-6">
                        <h2><?=$title_panel?></h2>
                    </div>
                    <!-- <div class="col-xs-12 col-sm-6 align-right">
                        <div class="switch panel-switch-btn">
                            <span class="m-r-10 font-12">REAL TIME</span>
                            <label>OFF<input type="checkbox" id="realtime" checked><span class="lever switch-col-cyan"></span>ON</label>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="body">
                <div>
                    <form class="" id="formKey">
                        <table style="width:100%;" class="table">
                            <tr>
                                <th style="text-align:right;"><label>No. Reg</label></th>
                                <th><input type="text" class="form-control" placeholder="No. Registrasi" name="nokey" id="nokey" aria-describedby="basic-addon1"></th>
                                <th><button type="button" class="btn btn-info" id="btn-cari"><i class="fa fa-search"></i></button></th>
                            </tr>
                            <tr>
                                <th style="text-align:right;"><label>Format</label></th>
                                <th>
                                    <input name="format" type="radio" id="rekap" class="with-gap radio-col-blue" value="rkp" checked/>
                                    <label for="rekap">REKAP</label>
                                    <input name="format" type="radio" id="detail" class="with-gap radio-col-blue" value="dtl" />
                                    <label for="detail">DETAIL</label>
                                </th>
                            </tr>
                        </table>
                    </form>
                </div>
                <div id="targetHasil" style="overflow:;"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){

    });
    $('#btn-cari').click(function(){
        regno = $('#nokey').val();
        format = $('[name=format]:checked').val();
        if (regno!='') {
            getHasil(regno,format);
        }else{
            $('#targetHasil').html('<div class="alert alert-warning" id="pesan"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-warning"></i> Nomor Registrasi Harus Disi!</div>');
        }
    });
     $('#formKey').submit(function(e){
        e.preventDefault();
        regno = $('#nokey').val();
        format = $('[name=format]:checked').val();
        if (regno!='') {
            getHasil(regno,format);
        }else{
            $('#targetHasil').html('<div class="alert alert-warning" id="pesan"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-warning"></i> Nomor Registrasi Harus Disi!</div>');
        }
    });
    
    function getHasil(regno='',format='') {
        $.ajax({
            type:'POST',
            url:'<?=base_url('biling_ranap/biling_irna_table')?>',
            data:{regno:regno,format:format},
            beforeSend(){
                $('#targetHasil').html('<div class="alert alert-info"><h3>Memuat Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
            },
            success(data){
                $('#targetHasil').html(data);
            }
        });
    }
</script>