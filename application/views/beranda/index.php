             <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
            <?php if($this->session->userdata("username") == ""):?>
            <div class="body">
                <div class="alert bg-deep-purple">
                    <!-- Safety Typed JS -->
                <p id="type" style="display: inline-block;font-size:20px"></p>
                </div>
            </div>
            <?php endif;?>
            <div class="block-header">
                <h2>DASHBOARD <?php echo $this->session->userdata('username');?></h2>
            </div>

            <!-- Widgets -->
<!--             <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-indigo hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">face</i>
                        </div>
                        <div class="content">
                            <div class="text">Jumlah Pasien Teregister Sekarang</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $reg_now?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-deep-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">face</i>
                        </div>
                        <div class="content">
                            <div class="text">Jumlah Pasien Teregister Kemarin</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $reg_kemarin?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">hotel</i>
                        </div>
                        <div class="content">
                            <div class="text">Jumlah Pasien Yang Dirawat</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $rawat;?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- #END# Widgets -->
            <!-- CPU Usage -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>CPU USAGE (%)</h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                    <div class="switch panel-switch-btn">
                                        <span class="m-r-10 font-12">REAL TIME</span>
                                        <label>OFF<input type="checkbox" id="realtime" checked><span class="lever switch-col-cyan"></span>ON</label>
                                    </div>
                                </div>
                            </div>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body">
                            <div id="real_time_chart" class="dashboard-flot-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# CPU Usage -->
<style>
    /* safety typed cursor size custom size typed.js */
.typed-cursor{
    font-size: 20px;
    /* opacity: 1;
    -webkit-animation: blink 0.7s infinite;
    -moz-animation: blink 0.7s infinite;
    animation: blink 0.7s infinite; */
}    
</style>
<script>
    // var typed = new Typed('#type', {
    //   strings: ["<i class='fa fa-info-circle'></i> Selamat Datang Di Portal" , 
    //             "<i class='fa fa-info-circle'></i> Portal Ini Menampilkan Menu Rekam Medik dan Farmasi dan sub menunya Antara Lain", 
    //             "<i class='fa fa-info-circle'></i> Bed Management : Untuk Melihat Dan Mencari Tempat Tidur Yang Masih Tersedia Di Ruangan", 
    //             "<i class='fa fa-info-circle'></i> Stock Apotik : Untuk Melihat Dan Mencari Data Stock Apotik" ,
    //             "<i class='fa fa-info-circle'></i> Stock Depo Hemo : Untuk Melihat Dan Mencari Data Stock Depo Hemo", 
    //             "<i class='fa fa-info-circle'></i> Stock Depo IDG : Untuk Melihat Dan Mencari Data Stock Depo IGD", 
    //             "<i class='fa fa-info-circle'></i> Stock Depo Inap : Untuk Melihat Dan Mencari Data Stock Depo Inap", 
    //             "<i class='fa fa-info-circle'></i> Stock Depo OK : Untuk Melihat Dan Mencari Data Stock OK"],
    //   typeSpeed: 40,
    //   // fadeOut: true,
    //   loop:true,
    //   backSpeed: 0,
    //   backDelay: 500,
    //   startDelay: 1000,
    //   showCursor: false,
    // });
</script>

            
        