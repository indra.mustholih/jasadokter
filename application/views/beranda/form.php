 <div class="row clearfix">
 	<div class="card">
 		<!-- <div class="header">
            <h2>
                INPUT
                <small>Different sizes and widths</small>
            </h2>
            <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);">Action</a></li>
                        <li><a href="javascript:void(0);">Another action</a></li>
                        <li><a href="javascript:void(0);">Something else here</a></li>
                    </ul>
                </li>
            </ul>
        </div> -->
        <div class="body">
        	<h2 class="card-inside-title">Basic Examples</h2>
            <div class="row clearfix">
                <div class="col-sm-12">
                	<form id="form_validation" method="POST">
                     <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" required/>
                           <label class="form-label">Username</label>
                        </div>
                    </div>
                     <div class="form-group form-float">
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" required/>
                           <label class="form-label">Password</label>

                        </div>
                    </div>
                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                  </form>
                </div>
            </div>

        </div>
 	</div>
 </div>