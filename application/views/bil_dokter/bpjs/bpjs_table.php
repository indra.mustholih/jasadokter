<?php
	$jt=$td=0;
	$jbj=$tbj=0;$jbv=$tbv=0;
	$jbh=$tbh=0;$jbo=$tbo=0;
	$jbl=$tbl=0;$jbr=$tbr=0;
	$jbu=$tbu=0;
	$pph=0;$jasa='';
	$periode = '';
	$NmDoc != '' ? $NmDoc:'';
	$tgl_awal != '' ? $tgl_awal:'';
	$tgl_akhir != '' ? $tgl_akhir:'';
	$periode = 'Periode: '.$this->cl->convDate($tgl_awal,7).' - '.$this->cl->convDate($tgl_akhir,7);
?>
<table class="table table-bordered table-hover" id="tblBilBpjs" style="width:100%;">
	<thead>
		<tr>
			<th>No.</th>
			<th>Jenis</th>
			<th>Tanggal</th>
			<th>Nama Pasien</th>
			<th>Tindakan</th>
			<th>Ruangan/Poli</th>
			<th>Pembayaran Pasien</th>
			<th>Jasa Dokter</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1. </td>
			<td>RAWAT JALAN</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<?php foreach ($rajal as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
		<tr>
			<td></td>
			<td></td>
			<td><?=$this->cl->convDate($l->Tanggal,7)?></td>
			<td><?=ucwords(strtolower($l->Firstname))?></td>
			<td><?=$l->NmTarif?></td>
			<td><?=$l->NMPoli?></td>
			<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
			<td style="text-align:right;"></td>
		</tr>
		<?php $jbj+=$jb;endforeach;?>
		<tr>
			<td>2. </td>
			<td>RAWAT INAP</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<?php foreach ($visite_umum as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
			<tr>
				<td></td>
				<td><?php if($jasa!='VISITE'){echo $jasa='VISITE';}?></td>
				<td><?=$this->cl->convDate($l->Tanggal,7)?></td>
				<td><?=ucwords(strtolower($l->Firstname))?></td>
				<td><?=$l->NmTarif?></td>
				<td><?=$l->NmBangsal?></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
				<td style="text-align:right;"><?=$this->cl->formatUang(($jb),2)?></td>
			</tr>
		<?php $jbv+=$jb;$tbv+=($jb);endforeach ?>
        <?php foreach ($visite_dpjp as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
            <tr>
                <td></td>
                <td><?php if($jasa!='VISITE'){echo $jasa='VISITE';}?></td>
                <td><?=$this->cl->convDate($l->Tanggal,7)?></td>
                <td><?=ucwords(strtolower($l->Firstname))?></td>
                <td><?=$l->NmTarif?></td>
                <td><?=$l->NmBangsal?></td>
                <td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
                <td style="text-align:right;"><?=$this->cl->formatUang(($jb),2)?></td>
            </tr>
        <?php $jbv+=$jb;$tbv+=($jb);endforeach ?>
		<?php foreach ($hd as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
			<tr>
				<td></td>
				<td><?php if($jasa!='HEMODIALISA'){echo $jasa='HEMODIALISA';}?></td>
				<td><?=$this->cl->convDate($l->Tanggal,7)?></td>
				<td><?=ucwords(strtolower($l->Firstname))?></td>
				<td><?=$l->NmTarif?></td>
				<td><?=$l->NmBangsal?></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
				<td style="text-align:right;"></td>
			</tr>
		<?php $jbh+=$jb;endforeach ?>
<!--		--><?php //foreach ($ibs as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
<!--			<tr>-->
<!--				<td></td>-->
<!--				<td>--><?php //if($jasa!='OK'){echo $jasa='OK';}?><!--</td>-->
<!--				<td>--><?//=$this->cl->convDate($l->Tanggal,7)?><!--</td>-->
<!--				<td>--><?//=ucwords(strtolower($l->Firstname))?><!--</td>-->
<!--				<td>--><?//=$l->NamaTindakan?><!--</td>-->
<!--				<td>--><?//=$l->NmBangsal?><!--</td>-->
<!--				<td style="text-align:right;">--><?//=$this->cl->formatUang($jb,2)?><!--</td>-->
<!--				<td style="text-align:right;"></td>-->
<!--			</tr>-->
<!--		--><?php //$jbo+=$jb;endforeach ?>
		<?php foreach ($lab as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
			<tr>
				<td></td>
				<td><?php if($jasa!='LABORATORION'){echo $jasa='LABORATORION';}?></td>
				<td><?=$this->cl->convDate($l->Tanggal,7)?></td>
				<td><?=ucwords(strtolower($l->Firstname))?></td>
				<td><?=$l->NmTarif?></td>
				<td><?=$l->NmBangsal?></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
                <td style="text-align:right;"><?=$this->cl->formatUang($jb*0.1,2)?></td>
			</tr>
		<?php $jbl+=$jb;$jbl+=($jb*0.1);endforeach ?>
		<?php foreach ($rad as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
			<tr>
				<td></td>
				<td><?php if($jasa!='RADIOLOGI'){echo $jasa='RADIOLOGI';}?></td>
				<td><?=$this->cl->convDate($l->Tanggal,7)?></td>
				<td><?=ucwords(strtolower($l->Firstname))?></td>
				<td><?=$l->NmTarif?></td>
				<td><?=$l->NmBangsal?></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
				<td style="text-align:right;"></td>
			</tr>
		<?php $jbr+=$jb;endforeach ?>
		<?php foreach ($rad as $l): $jb = $l->JumlahBiaya != '' ? $l->JumlahBiaya:0;?>
			<tr>
				<td></td>
				<td><?php if($jasa!='UGD'){echo $jasa='UGD';}?></td>
				<td><?=$this->cl->convDate($l->Tanggal,7)?></td>
				<td><?=ucwords(strtolower($l->Firstname))?></td>
				<td><?=$l->NmTarif?></td>
				<td><?=$l->NmBangsal?></td>
				<td style="text-align:right;"><?=$this->cl->formatUang($jb,2)?></td>
				<td style="text-align:right;"></td>
			</tr>
		<?php $jbu+=$jb;endforeach ?>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th style="text-align:right;">Jumlah Total: </th>
			<?php $jt=$jbj+$jbv+$jbh+$jbl+$jbr+$jbu;
					$td=$tbj+$tbv+$tbh+$tbl+$tbr+$tbu;?>
			<th style="text-align:right;"><?=$this->cl->formatUang($jt,2)?></th>
			<th style="text-align:right;"><?=$this->cl->formatUang($td,2)?></th>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
var nmdoc = '<?=$NmDoc?>';
var periode = '<?=$periode?>';
var headPrint = '<div style="border-top:;border-bottom:1px solid black;width:35%;text-align:center;"><span style="text-align:center;font-size:12px;">DINAS KESHATAN ANGKATAN UDARA</span><br><span style="text-align:center;font-size:12px;">RSPAU dr. S. HARDJOLUKITO</span></div><h4 style="text-align:center;">JASA PELAYANAN PASIEN BPJS</h4><h5 style="text-align:center">'+periode+'</h5><hr><table><tr><td>Dokter:&nbsp;'+nmdoc+'</td></tr></table><br>';
table = $('#tblBilBpjs').DataTable({    
  dom: 'Bfrtip',      
  buttons: [
  {
   text: '<span style="color:black"><i class="fa fa-lg fa-clipboard"></i> Cetak Data</span>',
   extend: 'print',
   footer:true,
      title: headPrint,
      autoPrint: true,
      className: 'btn btn-sm btn-primary',
    },
    // {
    //   text: '<span style="color:black"><i class="fa fa-lg fa-file-excel-o"></i> Export Ke Excel</span>',
    //   extend: 'excel',
    //   footer:true,
    //     // title: 'Laporan Depo Hemodialisa',
    //     // autoPrint: true,
    //     className: 'btn btn-sm btn-success',
        
    //   },
  ],
  "pageLength":15,
  "scrollX":true,
  "processing": true, //Feature control the processing indicator.
    // "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    "language": {
      "decimal": ",",
      "thousands": ".",
      "processing": '<i class="fa fa-refresh fa-spin fa-3x fa-fw" style="color:#2980b9"></i><span>Mohon Tunggu...</span>',
      "paginate": {
        "first"   : "Pertama",
        "previous": "<span class='fa fa-arrow-left'></span>",
        "next"    : "<span class='fa fa-arrow-right'></span>",
        "last"    : "Terakhir"
      },
      "zeroRecords": "Tidak ada data yang dapat ditampilkan"

    },
    "bLengthChange": false,

  });


</script>