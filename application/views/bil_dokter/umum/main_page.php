<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><i class="fa fa-file-o"></i> <?=$title_panel?></h2>
                <!-- <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#umum" data-toggle="tab">Umum</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- start umum -->
                    <div role="tabpanel" class="tab-pane fade in active" id="umum">
                        <div>
                            <form class="" id="formUmum" style="width:50%;">
                                <div class="form-group">
                                    <label>Nama Dokter : </label>
                                    <select class="form-control select2" id="KdDokU" name="KdDokU" style="width:100%;">
                                        <option value="">-- Pilih Dokter --</option>
                                        <?php foreach ($dokter as $dr): ?>
                                            <option value="<?=$dr->KdDoc?>"><?=$dr->NmDoc?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kategori Pasien : </label>
                                    <select class="form-control select2" id="KdKategoriPsn" name="KdKategoriPsn" style="width:100%;">
                                        <option value="">-- Pilih Kategori --</option>
                                        <?php foreach ($kategoripasien as $row): ?>
                                            <option value="<?=$row->KdKategori?>"><?=$row->NmKategori?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Cara Bayar : </label>
                                    <select class="form-control select2" id="KDCbayar" name="KDCbayar" style="width:100%;">
                                        <option value="">-- Pilih Cara Bayar --</option>
                                        <?php foreach ($kategoribayar as $row): ?>
                                            <option value="<?=$row->KDCbayar?>"><?=$row->NMCbayar?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Periode : </label>
                                    <div class="input-group">
                                        <div class="form-line">
                                            <input type="date" name="tgl_awalU" class="form-control " id="tgl_awalU">
                                        </div>
                                        <span class="input-group-addon">&nbsp;s/d&nbsp;</span>
                                        <div class="form-line">
                                            <input type="date" name="tgl_awalU" class="form-control " id="tgl_akhirU">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                                </div>
                            </form>
                        </div>
                        <div>
                            <div class="input-group input-group-sm pull-right" style="width:20%;background-color:lightgrey;padding:5px;border-radius:5px;">
                                <span class="input-group-addon" id="srcText-addon"><i class="fa fa-search"></i></span>
                                <input type="text" style="background-color:lightgrey;" class="form-control" id="srcText" name="srcText" placeholder="No.Reg / Nama / Kelas / Poli">
                            </div>
                        </div>
                        <hr>
                        <div>
                            <button class="btn btn-danger" id="btnPdf"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</button>
                        </div>
                        <br>
                        <div id="targetHasilUmum" style="overflow:;"></div>
                    </div>
                    <!-- end umum -->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        // getHasilUmum();
    });
    $('#formUmum').submit(function(e){
        e.preventDefault();
        KdDoc = $('#KdDokU').val();
        KDCbayar = $('#KDCbayar').val();
        NmDoc = $('#KdDokU option:selected').text();
        tgl_awal = $('#tgl_awalU').val();
        tgl_akhir = $('#tgl_akhirU').val();
        if (KdDoc != '' && tgl_awal != '' && tgl_akhir != '' ) {
            getHasilUmum(KdDoc,NmDoc,tgl_awal,tgl_akhir,'', KDCbayar);
        }else{
            $('#formUmum').before('<div class="alert038  alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-warning"></i>Silahkan pilih dokter dan isi inputan yang ada!</div>');
        }

    });
    function getHasilUmum(kd='',nm='',tgl1='',tgl2='',txtSrc='', kdbayar = '') {
        $.ajax({
            type:'POST',
            url:'<?=base_url('biling_dokter/bayar_umum_table')?>',
            data:{KdDoc:kd,NmDoc:nm,tgl_awal:tgl1,tgl_akhir:tgl2,txtSrc:txtSrc, KDCbayar:kdbayar},
            beforeSend:function(){
                $('#targetHasilUmum').html('<div class="alert alert-info"><h3>Memuat Data ... <i class="fa fa-spinner fa-pulse fa-lg" style="float:right;"></i></h3></div>');
            },
            success:function(data){
                $('#targetHasilUmum').html(data);
            }
        });
    }
    $('#srcText').keyup(function(){
        KdDoc = $('#KdDokU').val();
        NmDoc = $('#KdDokU option:selected').text();
        tgl_awal = $('#tgl_awalU').val();
        tgl_akhir = $('#tgl_akhirU').val();
        KdBayar = $('#KDCbayar').val();
        txtSrc = $(this).val();
        if(KdDoc != '' && tgl_awal != '' && tgl_akhir != ''){
            getHasilUmum(KdDoc,NmDoc,tgl_awal,tgl_akhir,txtSrc, KdBayar);
        }
    });

    $('#btnPdf').click(function () {
        KdDoc = $('#KdDokU').val().trim();
        tgl_awal = $('#tgl_awalU').val().trim();
        tgl_akhir = $('#tgl_akhirU').val().trim();
        if (KdDoc != '' && tgl_awal != '' && tgl_akhir != '') {
            url = "<?=base_url('Biling/Jasa-Dokter/Umum/PDF/')?>"+KdDoc+"/"+tgl_awal+"/"+tgl_akhir;
            window.open(url,"_blank");
        }else{
            alert("???");
        }

    });
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
      });
    $('.select2').select2();
</script>