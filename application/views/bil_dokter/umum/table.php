<?php
$tarif=0;$jsDok=0;$ok=0;$medis=0;
$pph=0;$jasa='';
$periode = '';
//$NmDoc != '' ? $NmDoc:'';
$tgl_awal != '' ? $tgl_awal:'';
$tgl_akhir != '' ? $tgl_akhir:'';
$periode = 'Periode: '.$this->cl->convDate($tgl_awal,7).' - '.$this->cl->convDate($tgl_akhir,7);
//	echo "<pre>";print_r($ok);echo "</pre>";die();
?>
    <table class="table table-bordered table-hover" id="tblBilUmum" style="width:100%;">
        <thead>
        <tr>
            <th rowspan="2">No.</th>
            <th rowspan="2">Nomor Registrasi </th>
            <th rowspan="2">Nama Pasien</th>
            <th rowspan="2">Kelas / Poli</th>
            <th rowspan="2" style="width:9%;">Tanggal</th>
            <th rowspan="2" style="width:15%;">Keterangan</th>
            <th rowspan="2" style="width:10%;">Tarif</th>
            <th colspan="3" style="width:10%;">Rawat Inap dan Rawat Jalan</th>
        </tr>
        <tr>
            <th>Visite / Jasa</th>
            <th>Operasi</th>
            <th>Tindakan / P. Medis</th>
        </tr>
        </thead>
        <?php $n=1;$hd='';foreach($rajal->head as $r1):?>
            <?php if($hd!="IRJ"):$hd="IRJ"?>
                <tr>
                    <th colspan="10" style="text-align: center">RAWAT JALAN</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?> </td>
                <td><?=$r1->regno?></td>
                <td><?=$r1->nama?></td>
                <td><?=$r1->NMPoli?></td>
                <td colspan="6" style="padding:0;margin:0">
                    <table class="" style="margin:0;width:100%;">
                        <?php foreach ($rajal->detail as $r2):if($r2->notran == $r1->notran):?>
                            <tr>
                                <td style="width:12.5%;"><?=$this->cl->convDate($r2->tanggal,7)?></td>
                                <td style="width:40%;"><?=$r2->detail_tarif?></td>
                                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r2->biaya_tarif)?><?php $tarif+=$r2->biaya_tarif;?></td>
                                <td style="text-align:right;width:20%;">
                                    <?php $jsDok+=$r2->visite_konsul;?>
                                    <?=$this->cl->formatAngka($r2->visite_konsul)?></td>
                                <td style="text-align:right;width:20%;"><?php $ok+=$r2->operasi;?>
                                    <?=$this->cl->formatAngka($r2->operasi)?></td></td>
                                <td style="text-align:right;width:20%;">
                                    <?php $medis+=$r2->tindakan_medis;?>
                                    <?=$this->cl->formatAngka($r2->tindakan_medis)?></td>
                            </tr>
                        <?php endif;endforeach;?>
                    </table>
                </td>
            </tr>
            <?php $n++;endforeach;?>

        <?php $n=1;$hd='';foreach($ugd->head as $r1):?>
            <?php if($hd!="UGD"):$hd="UGD"?>
                <tr>
                    <th colspan="10" style="text-align: center">UGD</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->regno?></td>
                <td><?=$r1->nama?></td>
                <td><?=$r1->NMPoli?></td>
                <td colspan="6" style="padding:0;margin:0">
                    <table class="" style="margin:0;width:100%;">
                        <?php foreach ($ugd->detail as $r2):if($r2->notran == $r1->notran):?>
                            <tr>
                                <td style="width:12.5%;"><?=$this->cl->convDate($r2->tanggal,7)?></td>
                                <td style="width:40%;"><?=$r2->detail_tarif?></td>
                                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r2->biaya_tarif)?><?php $tarif+=$r2->biaya_tarif;?></td>
                                <td style="text-align:right;width:20%;">
                                    <?php $jsDok+=$r2->visite_konsul;?>
                                    <?=$this->cl->formatAngka($r2->visite_konsul)?></td>
                                <td style="text-align:right;width:20%;"><?php $ok+=$r2->operasi;?>
                                    <?=$this->cl->formatAngka($r2->operasi)?></td></td>
                                <td style="text-align:right;width:20%;">
                                    <?php $medis+=$r2->tindakan_medis;?>
                                    <?=$this->cl->formatAngka($r2->tindakan_medis)?></td>
                            </tr>
                        <?php endif;endforeach;?>
                    </table>
                </td>
            </tr>
            <?php $n++;endforeach;?>

        <?php $n=1;$hd='';foreach($irna->head as $r1):?>
            <?php if($hd!="IRNA"):$hd="IRNA"?>
                <tr>
                    <th colspan="10" style="text-align: center">RAWAT INAP</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->regno?></td>
                <td><?=$r1->nama?></td>
                <td><?=$r1->NmBangsal?> / <?=$r1->NMKelas?></td>
                <td colspan="6" style="padding:0;margin:0">
                    <table class="" style="margin:0;width:100%;">
                        <?php foreach ($irna->detail as $r2):if($r2->notran == $r1->notran):?>
                            <tr>
                                <td style="width:12.5%;"><?=$this->cl->convDate($r2->tanggal,7)?></td>
                                <td style="width:40%;"><?=$r2->detail_tarif?></td>
                                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r2->biaya_tarif)?><?php $tarif+=$r2->biaya_tarif;?></td>
                                <td style="text-align:right;width:20%;">
                                    <?php $jsDok+=$r2->visite_konsul;?>
                                    <?=$this->cl->formatAngka($r2->visite_konsul)?></td>
                                <td style="text-align:right;width:20%;"><?php $ok+=$r2->operasi;?>
                                    <?=$this->cl->formatAngka($r2->operasi)?></td></td>
                                <td style="text-align:right;width:20%;">
                                    <?php $medis+=$r2->tindakan_medis;?>
                                    <?=$this->cl->formatAngka($r2->tindakan_medis)?></td>
                            </tr>
                        <?php endif;endforeach;?>
                    </table>
                </td>
            </tr>
            <?php $n++;endforeach;?>

        <?php $n=1;$hd='';foreach($opr->head as $r1):?>
            <?php if($hd!="OK"):$hd="OK"?>
                <tr>
                    <th colspan="10" style="text-align: center">OK</th>
                </tr>
            <?php endif;?>
            <tr>
                <td><?=$n;?></td>
                <td><?=$r1->regno?></td>
                <td><?=$r1->nama?></td>
                <td><?=$r1->NmBangsal?> / <?=$r1->NMKelas?></td>
                <td colspan="6" style="padding:0;margin:0">
                    <table class="" style="margin:0;width:100%;">
                        <?php foreach ($opr->detail as $r2):if($r2->notran == $r1->notran):?>
                            <tr>
                                <td style="width:12.5%;"><?=$this->cl->convDate($r2->tanggal,7)?></td>
                                <td style="width:40%;"><?=$r2->detail_tarif?></td>
                                <td style="text-align:right;width:20%;"><?=$this->cl->formatAngka($r2->biaya_tarif)?><?php $tarif+=$r2->biaya_tarif;?></td>
                                <td style="text-align:right;width:20%;">
                                    <?php $jsDok+=$r2->visite_konsul;?>
                                    <?=$this->cl->formatAngka($r2->visite_konsul)?></td>
                                <td style="text-align:right;width:20%;"><?php $ok+=$r2->operasi;?>
                                    <?=$this->cl->formatAngka($r2->operasi)?></td></td>
                                <td style="text-align:right;width:20%;">
                                    <?php $medis+=$r2->tindakan_medis;?>
                                    <?=$this->cl->formatAngka($r2->tindakan_medis)?></td>
                            </tr>
                        <?php endif;endforeach;?>
                    </table>
                </td>
            </tr>
            <?php $n++;endforeach;?>

        </tbody>
        <tfoot>
        <tr>
            <td colspan="6" style="text-align: right;">Total : </td>
            <td style="text-align: right;"><?=$this->cl->formatAngka($tarif)?></td>
            <td style="text-align: right;"><?=$this->cl->formatAngka($jsDok)?></td>
            <td style="text-align: right;"><?=$this->cl->formatAngka($ok)?></td>
            <td style="text-align: right;"><?=$this->cl->formatAngka($medis)?></td>
        </tr>
        </tfoot>
    </table>
<script type="text/javascript">
    var nmdoc = '<?=$NmDoc?>';
    var periode = '<?=$periode?>';
    var headPrint = '<div style="border-top:;border-bottom:1px solid black;width:35%;text-align:center;"><span style="text-align:center;font-size:12px;">DINAS KESHATAN ANGKATAN UDARA</span><br><span style="text-align:center;font-size:12px;">RSPAU dr. S. HARDJOLUKITO</span></div><h4 style="text-align:center;">JASA PELAYANAN PASIEN UMUM</h4><h5 style="text-align:center">'+periode+'</h5><hr><table><tr><td>Dokter:&nbsp;'+nmdoc+'</td></tr></table><br>';

    /*table = $('#tblBilUmum').DataTable({
        dom: '<lfB<t>ip>',
        buttons: [
            {
                text: '<span style="color:black"><i class="fa fa-clipboard"></i> Cetak Data</span>',
                extend: 'print',
                footer:true,
                title: 'JASA DOKTER',
                autoPrint: true,
                className: 'btn btn-sm btn-primary',
            },
            {
                text: '<span style="color:black"><i class="fa fa-file-excel-o"></i> Export Ke Excel</span>',
                extend: 'excelHtml5',
                // footer:true,
                title: 'JASA DOKTER',
                // autoPrint: true,
                className: 'btn btn-sm btn-success',
            },
        ],
        lengthMenu: [[15, 25, 50, 100, -1], [15, 25, 50, 100, "All"]],
      "pageLength":15,
      "scrollX":true,
      "processing": true, //Feature control the processing indicator.
        // "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "language": {
          "decimal": ",",
          "thousands": ".",
          "processing": '<i class="fa fa-refresh fa-spin fa-3x fa-fw" style="color:#2980b9"></i><span>Mohon Tunggu...</span>',
          "paginate": {
            "first"   : "Pertama",
            "previous": "<span class='fa fa-arrow-left'></span>",
            "next"    : "<span class='fa fa-arrow-right'></span>",
            "last"    : "Terakhir"
          },
          "zeroRecords": "Tidak ada data yang dapat ditampilkan"

        },
        "bLengthChange": false,

      });*/
    // function verf_bayar(id=''){
    //
    // }

</script>