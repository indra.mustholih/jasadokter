<?php $konf = $this->konfigurasi_model->listing();?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $konf->nama_web." | ".$title_atas?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('assets/images/thumb/'.$konf->logo)?>" type="image/png" sizes="16x16">
    <!-- <link rel="icon" href="<?php //echo $this->konfigurasi_model->listing()->logo;?>" type="image/x-icon"> -->

    <!-- Google Fonts -->
    <link href="<?=base_url('assets/googlefont/googlefont.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/googlefont/material-icon.css')?>" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('assets/adminsb/plugins/bootstrap/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('assets/adminsb/plugins/node-waves/waves.css')?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url('assets/adminsb/plugins/animate-css/animate.css')?>" rel="stylesheet" />

     <!-- Bootstrap Material Datetime Picker Css -->
    <!-- <link href="<?php echo base_url('assets/adminsb/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')?>" rel="stylesheet" /> -->
    <link href="<?php echo base_url('assets/datepicker/dist/css/bootstrap-datepicker.css')?>" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <!-- <link href="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet"> -->

    <!-- Multi Select Css -->
    <link href="<?php echo base_url('assets/adminsb/plugins/multi-select/css/multi-select.css')?>" rel="stylesheet">

     <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('assets/adminsb/plugins/bootstrap-select/css/bootstrap-select.css')?>" rel="stylesheet" />


    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css')?>">

    <!-- Wait Me Css -->
    <link href="<?php echo base_url('assets/adminsb/plugins/waitme/waitMe.css')?>" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?php echo base_url('assets/adminsb/plugins/morrisjs/morris.css')?>" rel="stylesheet" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css')?>">

    <!-- Custom Css -->
    <link href="<?php echo base_url('assets/adminsb/css/style.css')?>" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('assets/adminsb/css/themes/all-themes.css')?>" rel="stylesheet" />

     <!-- HighChart -->
    <link href="<?php echo base_url('assets/highchart/code/css/highcharts.css')?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/datatables.net-bs/css/buttons.dataTables.min.css')?>">

    <!-- SELECT 2 -->
    <link href="<?php echo base_url('assets/select2/dist/css/select2.min.css')?>" rel="stylesheet" />
    <!-- <link href="<?php echo base_url('assets/sweetalert/css/sweetalert2.min.css')?>" rel="stylesheet"/> -->
    <!-- Core Jquery -->
    <script src="<?=base_url('assets/jquery.min.js')?>"></script>

    <!-- SELECT 2 -->
    <script src="<?php echo base_url()?>assets/select2/dist/js/select2.min.js"></script>
    
    <!-- Highchart -->
    <script type="text/javascript" src="<?php echo base_url('assets/highchart/code/js/highcharts.js')?>"></script>

     <!-- Typed -->
    <script type="text/javascript" src="<?php echo base_url('assets/typed/lib/typed.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/printEl/print.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/printEl/jquery.printElement.js')?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url('assets/datatables.net/js/jquery.dataTables.min.js')?>"></script>
    <script src="<?php echo base_url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/datatables.net-bs/js/jszip.min.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('assets/datatables.net-bs/js/pdfmake.min.js')?>" type="text/javascript"></script>
    <!-- <script src="<?=base_url('assets/datatables.net-bs/js/fvs_fonts.js')?>" type="text/javascript"></script> -->
    <script src="<?php echo base_url('assets/datepicker/dist/js/bootstrap-datepicker.js')?>"></script>
    <script src="<?=base_url('assets/datatables.net-bs/js/dataTables.buttons.min.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('assets/datatables.net-bs/js/buttons.flash.min.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('assets/datatables.net-bs/js/buttons.html5.min.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('assets/datatables.net-bs/js/buttons.print.min.js')?>"></script>
    <!-- <script src="<?php echo base_url('assets/sweetalert2/js/sweetalert2.all.min.js')?>"></script> -->
    <!-- <script src="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script> -->
</head>

<body class="<?php echo $konf->tema?>">
    <!-- Page Loader -->
    <!-- <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader pl-size-xl ">
                <div class="spinner-layer pl-<?php //echo substr($konf->tema,6,100)?>">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Tunggu...</p>
        </div>
    </div> -->
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->