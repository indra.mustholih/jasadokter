<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <!-- <div class="user-info">
                <div class="image">
                    <img src="images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
                    <div class="email">john.doe@example.com</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div> -->
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="color:green">SELAMAT DATANG <?php echo strtoupper($this->session->userdata('username'))?></li>
                    <li class="header">NAVIGASI UTAMA</li>
                    <li class="<?=isset($main_menu) && $main_menu == 'beranda' ?'active':''?>">
                        <a href="<?php echo base_url()?>"><i class="material-icons">dashboard</i><span>Beranda</span></a>
                    </li>
                    <li class="<?=isset($main_menu) && $main_menu == 'biljasa' ?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">bar_chart</i>
                            <span>Biling Jasa</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?=isset($submenu1) && $submenu1 == 'jsdoc' ?'active':''?>">
                                <a href="javascript:void(0);" class="menu-toggle"><span>Jasa Dokter</span></a>
                                <ul class="ml-menu">
                                    <li class="<?=isset($submenu1) && $submenu1 == 'umum' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Biling/Jasa-Dokter/Umum')?>">
                                            <!-- <i class="material-icons">settings</i> -->
                                            <span>Pasien Umum</span>
                                        </a>
                                    </li>
                                    <li class="<?=isset($submenu1) && $submenu1 == 'umum' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Biling_lab_pd/rawat')?>">
                                            <!-- <i class="material-icons">settings</i> -->
                                            <span>Laboratorium Umum</span>
                                        </a>
                                    </li>
                                    <li class="<?=isset($submenu1) && $submenu1 == 'umum' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Biling_dokter/hemo_umum')?>">
                                            <!-- <i class="material-icons">settings</i> -->
                                            <span>Hemodialisa Umum</span>
                                        </a>
                                    </li>
                                    <li class="<?=isset($submenu1) && $submenu1 == 'hondok' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Biling_dokter/honor_dokter')?>">
                                            <!-- <i class="material-icons">settings</i> -->
                                            <span>Honor Dokter</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?=isset($submenu1) && $submenu1 == 'jasper' ?'active':''?>">
                                <a href="javascript:void(0);" class="menu-toggle"><span>Jasa Perawat</span></a>
                                <ul class="ml-menu">
                                    <li class="<?=isset($submenu1) && $submenu1 == 'umum' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Biling_perawat/rawat')?>">
                                            <!-- <i class="material-icons">settings</i> -->
                                            <span>Pasien Umum</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=isset($main_menu) && $main_menu == 'byjasa' ?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">bar_chart</i>
                            <span>Pembayaran Jasa</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?=isset($submenu1) && $submenu1 == 'sisajasadoc' ?'active':''?>">
                                <a href="javascript:void(0);" class="menu-toggle"><span>Sisa Pembayaran Jasa Dokter</span></a>
                                <ul class="ml-menu">
                                    <li class="<?=isset($submenu2) && $submenu2 == 'umum' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Biling/Sisa-Bayar/Jasa-Dokter/Umum')?>">
                                            <!-- <i class="material-icons">settings</i> -->
                                            <span>Pasien Umum</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <li class="<?=isset($submenu1) && $submenu1 == 'jsdoc' ?'active':''?>">
                                <a href="javascript:void(0);" class="menu-toggle"><span>Jasa Dokter</span></a>
                                <ul class="ml-menu">
                                    <li class="<?=isset($submenu2) && $submenu2 == 'umum' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Biling/Bayar/Jasa-Dokter/Umum')?>">
                                            <!-- <i class="material-icons">settings</i> -->
                                            <span>Pasien Umum</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!--                    -->
                    <?php if($this->session->userdata('akses_level') != ""):?>
                        <li class="<?=isset($main_menu) && $main_menu == 'master_data' ?'active':''?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">settings</i>
                                <span>Tarif Layanan BPJS</span>
                            </a>
                            <ul class="ml-menu">
                                <li class="<?=isset($submenu1) && $submenu1 == 'tarif_bpjs_irna' ? 'active' : '';?>">
                                    <a href="<?php echo base_url('Master-Data/Tarif-Pelayanan-BPJS-Irna')?>">
                                        <!-- <i class="material-icons">settings</i> -->
                                        <span>Rawat Inap</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php endif;?>
                    <?php if($this->session->userdata('akses_level') != ""):?>
                        <li class="<?=isset($main_menu) && $main_menu == 'byjasa' ?'active':''?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">settings</i>
                                <span>Distribusi</span>
                            </a>
                            <ul class="ml-menu">
                                <li class="<?=isset($submenu1) && $submenu1 == 'sisajasadoc' ?'active':''?>">
                                    <a href="javascript:void(0);" class="menu-toggle"><span>Rawat Jalan</span></a>
                                    <ul class="ml-menu">
                                        <li class="<?=isset($submenu1) && $submenu1 == 'komponenbiaya' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('komponenBiaya')?>">

                                                <span>Komponen Biaya </span>
                                            </a>
                                        </li>
                                        <li class="<?=isset($submenu1) && $submenu1 == 'jasapelayanan' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('jasaPelayanan')?>">

                                                <span>Jasa Pelayanan </span>
                                            </a>
                                        </li>
                                        <li class="<?=isset($submenu1) && $submenu1 == 'biayaperkomponen' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('biayaPerkomponen')?>">
                                                <span>Biaya PerKomponen </span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                                <li class="<?=isset($submenu1) && $submenu1 == 'sisajasadoc' ?'active':''?>">
                                    <a href="javascript:void(0);" class="menu-toggle"><span>Laboratorium Umum</span></a>
                                    <ul class="ml-menu">
                                        <li class="<?=isset($submenu1) && $submenu1 == 'komponenbiaya' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('komponenBiayaLab')?>">
                                                <span>Komponen Biaya</span>
                                            </a>
                                        </li>
                                        <li class="<?=isset($submenu1) && $submenu1 == 'jasapelayanan' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('jasaPelayananLab')?>">

                                                <span>Jasa Pelayanan </span>
                                            </a>
                                        </li>
                                        <li class="<?=isset($submenu1) && $submenu1 == 'biayaperkomponen' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('biayaPerkomponenLab')?>">
                                                <span>Biaya PerKomponen </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="<?=isset($submenu1) && $submenu1 == 'sisajasadoc' ?'active':''?>">
                                    <a href="javascript:void(0);" class="menu-toggle"><span>Hemodialisa Umum</span></a>
                                    <ul class="ml-menu">
                                        <li class="<?=isset($submenu1) && $submenu1 == 'komponenbiaya' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('komponenBiayaHemo')?>">
                                                <span>Komponen Biaya</span>
                                            </a>
                                        </li>
                                        <li class="<?=isset($submenu1) && $submenu1 == 'jasapelayanan' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('jasaPelayananHemo')?>">

                                                <span>Jasa Pelayanan </span>
                                            </a>
                                        </li>
                                        <li class="<?=isset($submenu1) && $submenu1 == 'biayaperkomponen' ? 'active' : '';?>">
                                            <a href="<?php echo base_url('biayaPerkomponenHemo')?>">
                                                <span>Biaya PerKomponen </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                
                            </ul>
                        </li>
                    <?php endif;?>
                    <!--<?php if($this->session->userdata('akses_level') != ""):?>
                        <li class="<?=isset($main_menu) && $main_menu == 'distribusi' ?'active':''?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">settings</i>
                                <span>Distribusi</span>
                            </a>
                            <ul class="ml-menu">
                                <li class="<?=isset($submenu1) && $submenu1 == 'komponenbiaya' ? 'active' : '';?>">
                                    <a href="<?php echo base_url('komponenBiaya')?>">

                                        <span>Komponen Biaya </span>
                                    </a>
                                </li>
                                <li class="<?=isset($submenu1) && $submenu1 == 'jasapelayanan' ? 'active' : '';?>">
                                    <a href="<?php echo base_url('jasaPelayanan')?>">

                                        <span>Jasa Pelayanan </span>
                                    </a>
                                </li>
                                <li class="<?=isset($submenu1) && $submenu1 == 'biayaperkomponen' ? 'active' : '';?>">
                                    <a href="<?php echo base_url('biayaPerkomponen')?>">
                                        <span>Biaya PerKomponen </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php endif;?> -->
                    <!-- Auth -->
                    <?php if($this->session->userdata('akses_level') == 'admin'):?>
                        <li class="<?=isset($main_menu) && $main_menu == 'konfigurasi' ?'active':''?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">settings</i>
                                <span>Setting</span>
                            </a>
                            <ul class="ml-menu">
                                <?php if($this->session->userdata('akses_level') == "admin"):?>
                                    <li class="<?=isset($submenu1) && $submenu1 == 'konf' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Administratif/Konfigurasi')?>">
                                            <!-- <i class="material-icons">settings</i> -->
                                            <span>Konfigurasi</span>
                                        </a>
                                    </li>
                                    <li class="<?=isset($submenu1) && $submenu1 == 'user' ? 'active' : '';?>">
                                        <a href="<?php echo base_url('Administratif/Data-User')?>">
                                            <!-- <i class="material-icons">update</i> -->
                                            <span>User Management</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <li class="<?=isset($submenu1) && $submenu1 == 'pwd' ? 'active' : '';?>">
                                    <a href="<?php echo base_url('Administratif/Data-User/Ubah-Password/'.$this->cl->enkrip($this->session->userdata('id_user')))?>">
                                        <!-- <i class="material-icons">update</i> -->
                                        <span>Ubah Password</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php endif;?>
                    <!-- End Auth  -->
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; <?php $build= "2018";$date = date('Y'); if($build == $date){echo "2018";}else{echo "2018". "-". $date;}?> <a href="#">RSPAU dr. S. HARDJOLUKITO</a><!--  - <a href="<?php echo base_url('User-Login')?>">Masuk</a> -->
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
</section>