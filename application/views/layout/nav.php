<style>
/*     .navbar-brand {
        padding: 0px !important;
        display: inline-block;
    }
      .navbar-brand>img {
        height: 100% !important;
        padding-left: 20px !important;
        width: auto !important;
        } */
/* body {
  margin: 20px;
} */

/* Custom Navbar Dengan Logo */
.logo {
  padding: 5px;

}

.logo img {
        height: auto ;
        padding-left: 20px ;
        width: 60px ;
}

.name {
  font-size: 18px;
  font-weight: bold;
  color: #fff;
  padding-top: 13px;
  padding-left: 5px;
}
.belum{
    visibility: hidden;
    position: absolute;
}
@media (max-width: 767px) {
    .logo {
      /* padding: 0px; */
      padding-left: 30px;
    }
    .ada {
      visibility: hidden;
      position: absolute;
    }
    .belum{
        visibility: visible;
        position: relative;
    }

}
</style>
  <?php $config = $this->konfigurasi_model->listing();?>
  <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="logo pull-left" href="<?php echo base_url()?>"><img src="<?php echo base_url('assets/images/thumb/'.$config->logo)?>" ></a>
                <a href="<?php echo base_url()?>"><span class="name ada pull-left"><?php echo $config->nama_web?> | DASHBOARD</span><span class="name belum pull-left"><?php echo $config->nama_web?></span></a>
               <!--  <a href="<?php //echo base_url()?>" class="navbar-brand">
                    <img src="<?php //echo base_url('assets/images/thumb/'.$config->logo)?>" width="30" height="30" class="img"/>
                </a> -->
                <!-- <a class="navbar-brand" style="padding:15px;font-size: 20px" href="<?php //echo base_url()?>"> &nbsp<?php //echo $config->nama_web?> | DASHBOARD</a> -->
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <?php if($this->session->userdata('username') != ''):?>
                        <li><a href="<?php echo base_url('beranda/logout')?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </nav>