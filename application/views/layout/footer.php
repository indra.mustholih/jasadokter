   <!-- Jquery Core Js -->
    <!-- <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery/jquery.min.js"></script> -->
    

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/node-waves/waves.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/momentjs/moment.js"></script>

     <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/flot-charts/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/plugins/flot-charts/jquery.flot.time.js"></script>

     <!-- Jquery DataTable Plugin Js -->
   <!--  <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php //echo base_url()?>assets/adminsb/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
 -->

    

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/jquery-sparkline/jquery.sparkline.js"></script>

     <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/jquery-validation/jquery.validate.js"></script>

     <!-- JQuery Steps Plugin Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/jquery-steps/jquery.steps.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url()?>assets/adminsb/js/admin.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/js/pages/index.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/js/pages/forms/basic-form-elements.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/js/pages/forms/form-validation.js"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url()?>/assets/adminsb/js/demo.js"></script>
</body>

</html>