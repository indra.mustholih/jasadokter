<div class="row clearfix">
    <div class="card">

        <div class="body">
            <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
            <h2 class="card-inside-title">Edit Komponen Biaya</h2>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <?php echo form_open(base_url('komponenBiaya/edit/'.$this->cl->enkrip($komp_biayaid)));?>
                    <div class="form-group">
                        <label for="">Komponen Biaya</label>
                        <div class="form-line">
                          <select name="komp_biaya" class="form-control">
                            <option value=""> - Pilih Komponen Biaya - </option>
                            <option value="1" <?php if($komponenbiaya['komp_biaya'] == '1'){echo "selected";}?>>Rawat Jalan</option>
                            <option value="2" <?php if($komponenbiaya['komp_biaya'] == '2'){echo "selected";}?>>Rawat Inap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Komponen</label>
                        <div class="form-line">
                            <input type="text" name="nama_komp" class="form-control" placeholder="Masukan Nama Komponen" value="<?php echo $komponenbiaya['nama_komp']?>" required>
                        </div>
                    </div>
                    
                    <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg">
                        <i class="fa fa-save"></i> &nbsp Simpan Data
                    </button> 
                    <a href="<?php echo base_url('komponenBiaya')?>" class="btn btn-default btn-lg">Batal</a>     
                    </div>
                    
                  <?php echo form_close();?>
                </div>
            </div>

        </div>
    </div>
 </div>