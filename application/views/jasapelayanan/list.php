<div class="row">
<div class="card">
    <ul class="nav nav-tabs tab-nav-right" role="tablist">
        <!-- <li role="presentation" class="active"><a href="#graf" data-toggle="tab"> <i class="material-icons">show_chart</i> GRAFIK</a></li> -->
        <li role="presentation" class="active"><a href="#all" data-toggle="tab"><i class="material-icons">view_list</i> DAFTAR JASA PELAYANAN</a></li>
    </ul>
    <div class="body">
        <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
     <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="all">
            <div class="row clearfix">
                <div class="header bg-<?php echo substr($this->konfigurasi_model->listing()->tema,6,100)?>">
                    <h2>
                        <i class="fa fa-users"></i><span> DAFTAR JASA PELAYANAN</span>
                    </h2>
                </div>
                <div class="body">
                    <p>
                        <a href="<?php echo base_url('JasaPelayanan/add')?>" title="" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Jasa Pelayanan</a>
                    </p>
                    
                    <div class="table-responsive">
                    <table id="table" class="table table-bordered table-striped" cellspacing="0" width="100%" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Komponen Biaya </th>
                                <th>Nama Jasa </th>
                                <th>Keterangan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    </div>
                </div>
                
                    <!-- /.box-body -->
                  <!-- /.box -->
           
            </div>
        </div>
    </div>   
    </div>
    
</div>
</div>

  

<!-- page script -->
<script type="text/javascript">

function confirmDialog() {
 return confirm('Apakah anda yakin akan menghapus data jasa pelayanan ini?')
}

var table;

$(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "language": {
            "processing": '<i class="fa fa-refresh fa-spin fa-3x fa-fw" style="color:#00A65A"></i><span>Mohon Tunggu...</span>'
        },
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('jasapelayanan/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });
    

});

</script>