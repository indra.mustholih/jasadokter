    <div class="row clearfix">
     	<div class="card">
           
            <div class="body">
                 <?php if($this->session->flashdata('sukses')):?>
                    <div class="alert alert-success"><i class="fa fa-check"></i> <?php echo $this->session->flashdata('sukses')?></div>
                <?php endif;?>
            	<div class="alert bg-indigo" align="center">Konfigurasi WEB</div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                    	<?php echo form_open_multipart(base_url('admin/konfigurasi'));?>
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1 class="card-inside-title"></h1>
                         <div class="form-group form-float">
                            <div class="form-line">
                                <label class="fosrm-label">Nama Website</label>
                                <input type="text" class="form-control" name="nama_web" value="<?php echo $konfigurasi->nama_web;?>"/>
                            </div>
                        </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group form-float">
                            <div class="form-line">
                                <label style="color:black">Tema Website</label>
                                <select name="tema" class="form-control">
                                    <option value="theme-deep-orange" style="background: #FF5722; color: #fff;" <?php if($konfigurasi->tema == "theme-deep-orange"){echo "selected";}?>>Deep Orange</option>

                                    <option value="theme-red" style="background: #F44336; color: #fff;" <?php if($konfigurasi->tema == "theme-red"){echo "selected";}?>>Red</option>

                                    <option value="theme-pink" style="background: #E91E63; color: #fff;" <?php if($konfigurasi->tema == "theme-pink"){echo "selected";}?>>Pink</option>

                                    <option value="theme-purple" style="background: #9C27B0; color: #fff;" <?php if($konfigurasi->tema == "theme-purple"){echo "selected";}?>>Purple</option>

                                    <option value="theme-deep-purple" style="background: #673AB7; color: #fff;" <?php if($konfigurasi->tema == "theme-deep-purple"){echo "selected";}?>>Deep Purple</option>

                                    <option value="theme-indigo" style="background: #3F51B5; color: #fff;" <?php if($konfigurasi->tema == "theme-indigo"){echo "selected";}?>>Indigo</option>

                                    <option value="theme-blue" style="background: #2196F3; color: #fff;" <?php if($konfigurasi->tema == "theme-blue"){echo "selected";}?>>Blue</option>

                                    <option value="theme-light-blue" style="background: #03A9F4; color: #fff;" <?php if($konfigurasi->tema == "theme-light-blue"){echo "selected";}?>>Light Blue</option>

                                    <option value="theme-cyan" style="background: #00BCD4; color: #fff;" <?php if($konfigurasi->tema == "theme-cyan"){echo "selected";}?>>Cyan</option>

                                    <option value="theme-teal" style="background: #009688; color: #fff;" <?php if($konfigurasi->tema == "theme-teal"){echo "selected";}?>>Teal</option>

                                    <option value="theme-green" style="background: #4CAF50; color: #fff;" <?php if($konfigurasi->tema == "theme-green"){echo "selected";}?>>Green</option>

                                    <option value="theme-light-green" style="background: #8BC34A; color: #fff;" <?php if($konfigurasi->tema == "theme-light-green"){echo "selected";}?>>Light Green</option>

                                    <option value="theme-lime" style="background: #CDDC39; color: #fff;" <?php if($konfigurasi->tema == "theme-lime"){echo "selected";}?>>Lime</option>

                                    <option value="theme-yellow" style="background: #FFE821; color: #fff;" <?php if($konfigurasi->tema == "theme-yellow"){echo "selected";}?>>Yellow</option>

                                    <option value="theme-amber" style="background: #FFC107; color: #fff;" <?php if($konfigurasi->tema == "theme-amber"){echo "selected";}?>>Amber</option>

                                    <option value="theme-orange" style="background: #FF9800; color: #fff;" <?php if($konfigurasi->tema == "theme-orange"){echo "selected";}?>>Orange</option>

                                    <option value="theme-brown" style="background: #795548; color: #fff;" <?php if($konfigurasi->tema == "theme-brown"){echo "selected";}?>>Brown</option>

                                    <option value="theme-grey" style="background: #9E9E9E; color: #fff;" <?php if($konfigurasi->tema == "theme-grey"){echo "selected";}?>>Grey</option>

                                    <option value="theme-blue-grey" style="background: #607D8B; color: #fff;" <?php if($konfigurasi->tema == "theme-blue-grey"){echo "selected";}?>>Blue Grey</option>

                                    <option value="theme-black" style="background: #000000; color: #fff;" <?php if($konfigurasi->tema == "theme-black"){echo "selected";}?>>Black</option>
                                </select>
                               
                            </div>
                        </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group form-float">
                                <div class="">
                                   <label>Logo</label>
                                   <input type="file" name="logo" class="form-control" placeholder="Logo" value="<?php echo set_value('logo')?>">

                                </div>
                            </div>
                            <div class="form-group">
                            <div class="">
                                <?php if($konfigurasi->logo != ""):?>
                                    <img src="<?php echo base_url('assets/images/thumb/'.$konfigurasi->logo)?>" class="img img-responsive img-thumbnail" width=10%>
                                <?php else:?>
                                     <span class="text-danger"><small>Belum ada foto yang diupload</small></span>
                                <?php endif;?>
                            </div>
                            </div>
                            <div class="form-group">
                            <button type="submit" class="btn btn-success btn-lg">
                                <i class="fa fa-check-circle"></i> &nbsp Terapkan
                            </button>    
                            </div>
                        </div>

                        
                        
                      <?php echo form_close();?>
                    </div>
                </div>

            </div>
     	</div>
     </div>