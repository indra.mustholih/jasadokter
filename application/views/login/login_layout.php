
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>LOGIN </title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url()?>assets/adminsb/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url()?>assets/adminsb/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url()?>assets/adminsb/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css">

    <!-- Custom Css -->
    <link href="<?php echo base_url()?>assets/adminsb/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><b>RSPAU dr. S. HARDJOLUKITO</b> LOGIN</a>
            <!-- <small>Admin BootStrap Based - Material Design</small> -->
        </div>
        <div class="card">
             
            <div class="body">
            <?php
              echo validation_errors('<div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-warning"></i> ','</div>');

              if($this->session->flashdata('gagal')){
                echo '<div class="alert alert-danger">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('gagal');
                echo "</div>";
              }elseif($this->session->flashdata('sukses')){
                echo '<div class="alert alert-success">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo $this->session->flashdata('sukses');
                echo '</div>';
              }
            ?>
                <form action="<?php echo base_url('login')?>" method="post">
                    <div class="msg">Silahkan Log In</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                             <input type="text" name="username" class="form-control" placeholder="Username">
                            <!-- <input type="text" class="form-control" name="username" placeholder="Username" required autofocus> -->
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            <!-- <input type="password" class="form-control" name="password" placeholder="Password" required> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-block bg-pink waves-effect"><i class="fa fa-sign-in"></i> SIGN IN</button>
                        </div>
                        <div class="col-xs-8">
                           <a href="<?php echo base_url()?>" class="btn btn-block bg-indigo waves-effect"><i class="fa fa-home"></i> PORTAL</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url()?>assets/adminsb/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src=".<?php echo base_url()?>assets/adminsb/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src=".<?php echo base_url()?>assets/adminsb/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url()?>assets/adminsb/js/admin.js"></script>
    <script src="<?php echo base_url()?>assets/adminsb/js/pages/examples/sign-in.js"></script>
</body>

</html>