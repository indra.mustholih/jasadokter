<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	private $db2;
	public function __construct(){
		parent::__construct();
		$this->load->library('cl');
	}
	public function index()
	{
		$data = array
		(
			'title_atas' => 'Dashboard',
			'title_panel'=> 'Dashboard',
			'main_menu'	 => 'beranda',
			'isi'        => 'beranda/index'  
		);
		$this->load->view('layout/wrapper', $data, FALSE);
	}

	public function form()
	{
		$data = array
		(
			'isi'  => 'beranda/form',
		);
		$this->load->view('layout/wrapper',$data, FALSE);
	}
	public function logout(){
		$userdata = ['username','akses_level','id_user'];
		$this->session->unset_userdata($userdata);
		$this->session->set_flashdata('sukses','<i class="fa fa-check"></i> Anda Berhasil Logout');
		redirect(base_url(),'refresh');
	}

}

/* End of file Beranda.php */
/* Location: ./application/controllers/Beranda.php */