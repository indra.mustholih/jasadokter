<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('username') == ""){
			$this->session->set_flashdata('gagal','<i class="fa fa-warning"></i> Anda Harus Login Dahulu');
			redirect(base_url('login'),'refresh');
		}
		$this->load->model('user_model','users');
		$this->load->library('cl');
	}

	public function index()
	{
		$user = $this->users->listing();
		$data = array(
			'title_atas' => 'User Management',
			'title_panel'=> 'Pengguna',
			'user'		 => $user,
			'main_menu'   	=> 'konfigurasi',
			'submenu1'   	=> 'user',
			'isi'		 => 'user/list'
		);
		$this->load->view('layout/wrapper',$data, FALSE);
	}

	public function ajax_list()
	{
		$list = $this->users->get_datatables();
		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $datas){
			$no++;
			$row  	= array();
			$row[]	= $no;
			$row[]	= $datas->username;
			$row[]	= $datas->akses_level;
			if(strtolower($datas->username) == strtolower($this->session->userdata('username'))){
				$row[] 	= anchor('Administratif/Data-User/'.$this->cl->enkrip($datas->id_user), '<i class="fa fa-pencil"></i> edit', array('class' => 'btn btn-warning btn-sm'));
			}else{

				$row[] 	= anchor('Administratif/Data-User/'.$this->cl->enkrip($datas->id_user), '<i class="fa fa-pencil"></i> edit', array('class' => 'btn btn-warning btn-sm'))."&nbsp".
		 			 anchor('Administratif/Data-User/Hapus/'.$this->cl->enkrip($datas->id_user), '<i class="fa fa-trash"></i> Hapus', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirmDialog();"));
			}
		 	$data[] = $row;
		}

		$output 	= array(
						"draw"				=> $_POST['draw'],
						"recordsTotal"		=> $this->users->count_all(),
						"recordsFiltered"	=> $this->users->count_filtered(),
						"data"				=> $data
					);
		echo json_encode($output);
	}

	public function tambah()
	{
		$valid 	= $this->form_validation;

		$valid->set_rules('username','Username','required|is_unique[user.username]',array(
			'required'		=> '%s Harus Diisi',
			'is_unique'		=> '%s Sudah ada Silahkan mengganti dengan username lain'
		));

		$valid->set_rules('password','Password','required|min_length[6]',array(
			'required'		=> '%s Harus Diisi',
			'min_length'	=> '%s Minimal 6 Karakter'
		));

		if($valid->run() == FALSE){
			$data = array(
				'title_atas'	=> 'Halaman Tambah User',
				'title_panel'	=> 'Tambah Pengguna',
				'isi'			=> 'user/tambah',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'user',
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i   	= $this->input;
			$data 	= array(
				'username'		=> $i->post('username'),
				'password'		=> password_hash($i->post('password'),PASSWORD_BCRYPT),
				'akses_level'	=> $i->post('akses_level'),
			);

			$this->users->tambah($data);
			$this->session->set_flashdata('sukses','User Berhasil Dibuat');
			redirect(base_url('Administratif/Data-User'),'refresh');
		}
	}

	public function edit($id_user)
	{
		$id_user = $this->cl->dekrip($id_user);
		$valid 	= $this->form_validation;

		$valid->set_rules('password','Password','min_length[6]',array(
			'min_length'	=> '%s Minimal 6 Karakter'
		));

		$user 	= $this->users->detail($id_user);

		if($valid->run() == FALSE)
		{
			$data 	= array(
				'title_atas' 	=> 'Halaman Ubah Data Pengguna',
				'user'			=> $user,
				'title_panel'	=> 'Edit Pengguna',
				'isi'			=> 'user/edit',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'user',
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i 		= $this->input;
			$pass 	= $i->post('password');
			if($pass == "")
			{
				$data 	= array(
					'id_user'		=> $id_user,
					'username'		=> $i->post('username'),
					'akses_level'	=> $i->post('akses_level')
				);
				$this->users->edit($data);
				$this->session->set_flashdata('sukses','Data Berhasil Di Ubah');
				redirect(base_url('Administratif/Data-User'),'refresh');
			}else{
				$data  = array(
					'id_user'		=> $id_user,
					'username'		=> $i->post('username'),
					'password'		=> password_hash($i->post('password'),PASSWORD_BCRYPT),
					'akses_level'	=> $i->post('akses_level')
				);
				$this->users->edit($data);
				$this->session->set_flashdata('sukses','Data Berhasil Di ubah');
				redirect(base_url('Administratif/Data-User'),'refresh');
			}
		}	
	}

	public function delete($id_user){
		$id_user = $this->cl->dekrip($id_user);
		$data = array('id_user' => $id_user);
		$this->users->delete($data);
		$this->session->set_flashdata('sukses','Data Telah Dihapus');
		redirect(base_url('admin/user'),'refresh');
	}

	public function ubah_password($id_user){
		$id_user = $this->cl->dekrip($id_user);
		$valid		= $this->form_validation;

		$valid->set_rules('password','Password','min_length[6]',array(
			'min_length'	=> '%s Minimal 6 karakter'));
		
		$user 	= $this->users->detail($id_user);
		
		if($valid->run()==FALSE){
			
			$data = array(
				'title_atas'	=> 'Halaman Pengguna',
				'title_panel'	=> 'Pengguna ',
				'user'			=> $user,
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'user',
				'isi'			=> 'user/ubah_password'
			);
			$this->load->view('layout/wrapper',$data, FALSE);
			
		}else{
				$i 		= $this->input;
				$pass   = $i->post('password');
				$pass2  = $i->post('password2');
				if($pass != $pass2){
					$this->session->set_flashdata('gagal','Password ulang tidak sama');
					redirect(base_url('Administratif/Data-User/Ubah-Passord/'.$this->cl->enkrip($this->session->userdata('id_user'))),'refresh');	
				}else{
					$data 	= array(
						'id_user'		=> $id_user,
						'password'		=> password_hash($pass,PASSWORD_BCRYPT),
					);
					$this->users->edit($data);
					
				}
				$this->session->set_flashdata('sukses','Data Berhasil Diubah');
				redirect(base_url('Administratif/Data-User'),'refresh');
		}
			
			
	}

}

/* End of file User.php */
/* Location: ./application/controllers/admin/User.php */