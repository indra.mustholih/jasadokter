<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigurasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username') == ""){
			$this->session->set_flashdata('gagal','<i class="fa fa-warning"></i> Anda Harus Login Dahulu');
			redirect(base_url('login'),'refresh');
		}
		$this->load->model('konfigurasi_model','konfigurasi');
		$this->load->library('cl');
	}
	public function index()
	{
		$konfigurasi = $this->konfigurasi->listing();

		$this->form_validation->set_rules('nama_web','Nama Website','required',array('required' => '%s Harus Diisi'));

		if($this->form_validation->run())
		{
			if(!empty($_FILES['logo']['name']))
			{
				$config 	['upload_path']			= './assets/images';
				$config 	['allowed_types']		= 'png|jpg|jpeg|gif';
				$config 	['max_size']			= '9000'; // Dalam 1an Kb
				$this->upload->initialize($config);

				if(! $this->upload->do_upload('logo'))
				{
					$data = array
					(
						'title_atas'    => $konfigurasi->nama_web.' |Konfigurasi',
						'title_panel'   => 'Konfigurasi',
						'konfigurasi'	=>  $konfigurasi,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'konfigurasi/form',
						'main_menu'   	=> 'konfigurasi',
						'submenu1'   	=> 'konf',
					);
					$this->load->view('layout/wrapper', $data, FALSE);
				}
				else
				{
					$upload_data				= array('uploads'	=> $this->upload->data());
						//Image Editor

					$config['image_library']	= 'gd2';
					$config['source_image']		= './assets/images/'.$upload_data['uploads']['file_name'];
					$config['new_image']		= './assets/images/thumb/';
					$config['create_thumb']		= TRUE;
					$config['quality']			= "100%";
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 64 ; //PIXEL
					$config['height']			= 64 ;//PIXEL
					$config['x_axis']			= '0';
					$config['y_axis']			= '0';
					$config['thumb_marker']		= '';
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					// Hapus data jika ingin upload foto lagi
					if($konfigurasi->logo != "" ){
						unlink('./assets/images/'.$konfigurasi->logo);
						unlink('./assets/images/thumb/'.$konfigurasi->logo);
					}// END DELETE	

					$i = $this->input;
					$data = array
					(	
						'id_konf'		=> $konfigurasi->id_konf,
						'logo'			=> $upload_data['uploads']['file_name'],
						'nama_web'		=> $i->post('nama_web'),
						'tema'			=> $i->post('tema'),
					);
					$this->konfigurasi->edit($data);
					$this->session->set_flashdata('sukses','Data Berhasil Diubah');
					redirect(base_url('admin/konfigurasi'),'refresh');
				}
			}
			else
			{
				$i = $this->input;

				$data = array
					(	
						'id_konf'		=> $konfigurasi->id_konf,
						'nama_web'		=> $i->post('nama_web'),
						'tema'			=> $i->post('tema'),
					);
					$this->konfigurasi->edit($data);
					$this->session->set_flashdata('sukses','Data Berhasil Diubah');
					redirect(base_url('admin/konfigurasi'),'refresh');
			}
		}	
		$data = array
		(
			'title_atas'    => $konfigurasi->nama_web.' |Konfigurasi',
			'title_panel'   => 'Konfigurasi',
			'konfigurasi'	=>  $konfigurasi,
			'main_menu'   	=> 'konfigurasi',
			'submenu1'   	=> 'konfig',
			'isi'			=> 'konfigurasi/form'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
	}

}

/* End of file Konfigurasi.php */
/* Location: ./application/controllers/admin/Konfigurasi.php */