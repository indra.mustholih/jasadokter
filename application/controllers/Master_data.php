<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_data extends CZ_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('master_data_model','mdm');
		$this->load->library('cl');
	}
	public function index()
	{
		echo "There is Nothing!";
	}
	public function tarif_bpjs_irna()
	{
		$data = array
		(
			'title_atas' => 'Master Data',
			'title_panel'=> 'Jasa Pelayanan Kesehatan BPJS Rawat Inap',
			'isi'        => 'master_data/jasa_klaim_bpjs_irna/index'
		);
		$this->load->view('layout/wrapper', $data);
	}

	public function tarif_bpjs_irna_form()
    {
        $resp['stat'] = false;
        $way = $this->req('way');
        if($way == 'POST'){
            $send = array();
            $send['Inst']                           = $this->req('Inst');
            $send['asgiz']                          = $this->req('asgiz');
            $send['askep_0']                        = $this->req('askep_0');
            $send['askep_1']                        = $this->req('askep_1');
            $send['askep_2']                        = $this->req('askep_2');
            $send['askep_3']                        = $this->req('askep_3');
            $send['askep_4']                        = $this->req('askep_4');
            $send['ranap_dari_igd_dokter']          = $this->req('ranap_dari_igd_dokter');
            $send['ranap_dari_igd_perawat']         = $this->req('ranap_dari_igd_perawat');
            $send['visite_spesialis_konsul_telpon'] = $this->req('visite_spesialis_konsul_telpon');
            $send['visite_spesialis_visite_0']      = $this->req('visite_spesialis_visite_0');
            $send['visite_spesialis_visite_1']      = $this->req('visite_spesialis_visite_1');
            $send['visite_spesialis_visite_2']      = $this->req('visite_spesialis_visite_2');
            $send['visite_spesialis_visite_3']      = $this->req('visite_spesialis_visite_3');
            $send['visite_umum_jaga']               = $this->req('visite_umum_jaga');
            $send['visite_umum_visite']             = $this->req('visite_umum_visite');
            $send['CreateAt']                       = date('Y-m-d H:i:s');
            $up = $this->mdm->up_tarif_bpjs_irna('',$send);
            if($up){
                $resp['stat'] = true;
            }
        }else if($way == 'PUT'){
            $tarif = $this->mdm->get_tarif_bpjs_irna();
            $data = array
            (
                'tarif' => $tarif,
            );
            $this->load->view('master_data/jasa_klaim_bpjs_irna/form', $data);
            return false;
        }else if($way == 'DEL'){

        }
//        else{
//            $tarif = $this->mdm->get_tarif_bpjs_irna();
//            $data = array
//            (
//                'tarif' => $tarif,
//            );
//            $this->load->view('master_data/jasa_klaim_bpjs_irna/form', $data);
//            return false;
//        }

        echo json_encode($resp);
    }


}

/* End of file biling.php */
/* Location: ./application/controllers/biling.php */