<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// komp biaya
// 1. rajal 
// 2. ranap

class KomponenBiaya extends CZ_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('komponenBiaya_model','kbm');
		$this->load->library('cl');
  }
  public function index(){
    $kbm = $this->kbm->listing();
    
		$data = array(
			'title_atas' => 'Komponen Biaya',
			'title_panel'=> 'Komponen Biaya',
			'kbm'		 => $kbm,
			'main_menu'   	=> 'konfigurasi',
			'submenu1'   	=> 'komponenbiaya',
			'isi'		 => 'komponenbiaya/list'
		);
		$this->load->view('layout/wrapper',$data, FALSE);
  }

  public function ajax_list(){
    $list = $this->kbm->get_datatables();
    $data = array();
		$no   = $_POST['start'];
		foreach ($list as $datas){
			$no++;
			$row  	= array();
			$row[]	= $no;
			$row[]	= $datas->komp_biaya;
      $row[]	= $datas->nama_komp;
		
			$row[] 	= anchor('komponenbiaya/edit/'.$this->cl->enkrip($datas->komp_biayaid), '<i class="fa fa-pencil"></i> edit', array('class' => 'btn btn-warning btn-sm'))."&nbsp".
		 			 anchor('komponenbiaya/delete/'.$this->cl->enkrip($datas->komp_biayaid), '<i class="fa fa-trash"></i> Hapus', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirmDialog();"));
			
		 	$data[] = $row;
		}

		$output 	= array(
						"draw"				=> $_POST['draw'],
						"recordsTotal"		=> $this->kbm->count_all(),
						"recordsFiltered"	=> $this->kbm->count_filtered(),
						"data"				=> $data
					);
		echo json_encode($output);
	}

	// function tambah komponen biaya
	public function add(){
		$valid 	= $this->form_validation;

		// $valid->set_rules('mst_groupjasaid','Group Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
		// ));

		$valid->set_rules('komp_biaya','Komponen Biaya','required',array(
			'required'		=> '%s Harus Diisi',
    ));
		
    if($valid->run() == FALSE){
			$data = array(
				'title_atas'	=> 'Halaman Tambah Komponen Biaya',
				'title_panel'	=> 'Tambah Komponen Biaya',
				'isi'			=> 'komponenbiaya/tambah',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'komponenbiaya',
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i   	= $this->input;
			$data 	= array(
				'komp_biaya'		=> $i->post('komp_biaya'),
        'nama_komp'	=> $i->post('nama_komp'),
			);

			$this->kbm->add($data);
			$this->session->set_flashdata('sukses','Komponen Biaya Berhasil Dibuat');
			redirect(base_url('komponenBiaya'),'refresh');
    }
	}

	//function edit komponen biaya
	public function edit($komp_biayaid){
    
		$komp_biayaid = $this->cl->dekrip($komp_biayaid);

		$valid 	= $this->form_validation;

		$valid->set_rules('komp_biaya','Komponen Biaya','required',array(
			'required'		=> '%s Harus Diisi',
		));

		// $valid->set_rules('nama_jasa','Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
    // ));

		$komp_biaya 	= $this->kbm->edit($komp_biayaid);
    
		if($valid->run() == FALSE)
		{
			$data 	= array(
				'title_atas' 	=> 'Halaman Ubah Komponen Biaya',
        'komponenbiaya'			=> $komp_biaya,
        'komp_biayaid'			=> $komp_biayaid,
				'title_panel'	=> 'Edit Komponen Biaya',
				'isi'			=> 'komponenbiaya/edit',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'komponenbiaya',
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i 		= $this->input;
		
      $data  = array(
        'komp_biaya' => $i->post('komp_biaya'),
        'nama_komp'		  => $i->post('nama_komp'),
      );
      $this->kbm->editproses($komp_biayaid,$data);
      $this->session->set_flashdata('sukses','Data Berhasil di Ubah');
      redirect(base_url('komponenBiaya'),'refresh');
			
		}	
	}

	public function delete($komp_biayaid){
		$komp_biayaid = $this->cl->dekrip($komp_biayaid);
		$data = array('komp_biayaid' => $komp_biayaid);
		$this->kbm->delete($data);
		$this->session->set_flashdata('sukses','Data Telah Dihapus');
		redirect(base_url('komponenBiaya'),'refresh');
	}

  
}