<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spesialis extends CZ_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('spesialis_model','spes');
		$this->load->library('cl');
  }

  public function index(){
    $spes = $this->spes->listing();
    
		$data = array(
			'title_atas' => 'Klinik Spesialis',
			'title_panel'=> 'Klinik Spesialis',
			'spes'		 => $spes,
			'main_menu'   	=> 'konfigurasi',
			'submenu1'   	=> 'spesialis',
			'isi'		 => 'spesialis/list'
		);
		$this->load->view('layout/wrapper',$data, FALSE);
  }

  public function ajax_list(){
    $list = $this->spes->get_datatables();
    $data = array();
		$no   = $_POST['start'];
		foreach ($list as $datas){
			$no++;
			$row  	= array();
			$row[]	= $no;
			$row[]	= $datas->nama_jasa;
      $row[]	= money_to_view($datas->jumlah);
      $row[]	= $datas->keterangan;
		
			$row[] 	= anchor('spesialis/edit/'.$this->cl->enkrip($datas->trans_spesialisid), '<i class="fa fa-pencil"></i> edit', array('class' => 'btn btn-warning btn-sm'))."&nbsp".
		 			 anchor('spesialis/delete/'.$this->cl->enkrip($datas->trans_spesialisid), '<i class="fa fa-trash"></i> Hapus', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirmDialog();"));
			
		 	$data[] = $row;
		}

		$output 	= array(
						"draw"				=> $_POST['draw'],
						"recordsTotal"		=> $this->spes->count_all(),
						"recordsFiltered"	=> $this->spes->count_filtered(),
						"data"				=> $data
					);
		echo json_encode($output);
  }

  public function tambah(){
    
    $valid 	= $this->form_validation;

		$valid->set_rules('mst_jasapelayananid','Jasa Pelayanan','required',array(
			'required'		=> '%s Harus Diisi',
		));

		// $valid->set_rules('nama_jasa','Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
    // ));
    $jasapelayananGetAll = $this->spes->jasapelayananGetAll();
    if($valid->run() == FALSE){
			$data = array(
				'title_atas'	=> 'Halaman Klinik Spesialis',
				'title_panel'	=> 'Tambah Klinik Spesialis',
				'isi'			=> 'spesialis/tambah',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'spesialis',
				'jasapelayananGetAll'   	=> $jasapelayananGetAll,
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i   	= $this->input;
			$data 	= array(
				'mst_jasapelayananid'		=> $i->post('mst_jasapelayananid'),
        'jumlah'	=> $i->post('jumlah'),
        'keterangan'	=> $i->post('keterangan'),
			);

			$this->spes->tambah($data);
			$this->session->set_flashdata('sukses','Klinik Spesialis Berhasil Dibuat');
			redirect(base_url('spesialis'),'refresh');
    }
  }
  public function edit($trans_spesialisid){
    
		$trans_spesialisid = $this->cl->dekrip($trans_spesialisid);

		$valid 	= $this->form_validation;

		$valid->set_rules('mst_jasapelayananid','Jasa Pelayanan','required',array(
			'required'		=> '%s Harus Diisi',
		));

		// $valid->set_rules('nama_jasa','Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
    // ));

		$spesialis 	= $this->spes->edit($trans_spesialisid);
		$jasapelayananGetAll = $this->spes->jasapelayananGetAll();
    
		if($valid->run() == FALSE)
		{
			$data 	= array(
				'title_atas' 	=> 'Halaman Ubah Data Klinik Spesialis',
        'spesialis'			=> $spesialis,
        'trans_spesialisid'			=> $trans_spesialisid,
				'title_panel'	=> 'Edit Jasa Pelayanan',
				'isi'			=> 'spesialis/edit',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'spesialis',
				'jasapelayananGetAll'   	=> $jasapelayananGetAll,
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i 		= $this->input;
		
      $data  = array(
        'mst_jasapelayananid' => $i->post('mst_jasapelayananid'),
        'jumlah'		          => $i->post('jumlah'),
        'keterangan'		      => $i->post('keterangan')
      );

      $this->spes->editproses($trans_spesialisid,$data);
      $this->session->set_flashdata('sukses','Data Berhasil di Ubah');
      redirect(base_url('spesialis'),'refresh');
			
		}	
  }
	public function delete($trans_spesialisid){
		$trans_spesialisid = $this->cl->dekrip($trans_spesialisid);
		$data = array('trans_spesialisid' => $trans_spesialisid);
		$this->spes->delete($data);
		$this->session->set_flashdata('sukses','Data Telah Dihapus');
		redirect(base_url('spesialis'),'refresh');
	}

}