<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_ranap extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('biling_ranap_model','bm');
		$this->load->library('cl');
		$this->load->library('pdf');
	}
	public function index()
	{
		echo "There is Nothing!";
	}
	public function biling_irna()
	{
		$data = array
		(
			'title_atas' => 'RAWAT INAP',
			'title_panel'=> 'Biling Transaksi Rawat Jalan',
			'main_menu'	 => 'bilirna',
			'isi'        => 'bil_irna/main_page'
		);
		$this->load->view('layout/wrapper', $data);
	}
	public function biling_irna_table($regno='',$format='',$dt='')
	{
		if($regno == ''){
			$regno = $this->input->post('regno');
		}
		if($format == ''){
			$format = $this->input->post('format');
		}
		$ps = $this->bm->get_pasien($regno);
		$irna = $this->bm->get_irna($regno);
		$ibs = $this->bm->get_ibs($regno);
		$lab = $this->bm->get_lab($regno);
		$fis = $this->bm->get_fis($regno);
		$ugd = $this->bm->get_ugd($regno);
		$rad = $this->bm->get_rad($regno);
		$irj = $this->bm->get_irj($regno);
		$apt = $this->bm->get_apotek($regno);
		$umuk = $this->bm->get_umuk($regno);
		$hemo = $this->bm->get_hd($regno);
		// echo "<pre>";print_r($hemo);echo "</pre>"; die();
		if ($format == 'rkp') {
			$data = array
			(
				'ps'	=> $ps,	 'irna'	=> $irna,
				'lab'	=> $lab, 'ibs'	=> $ibs,
				'fis'	=> $fis, 'ugd'	=> $ugd,
				'rad'	=> $rad, 'irj'	=> $irj,
				'apt'	=> $apt, 'hemo'	=> $hemo,
				'umuk'	=> $umuk, 'bln'	=> $this->cl->arrBulan(),
				'set'	=> '',
			);
			$this->load->view('bil_irna/main_page_table_rkp', $data);	
		}elseif($format == 'dtl'){
			$data = array
			(
				'ps'	=> $ps,	 'irna'	=> $irna,
				'lab'	=> $lab, 'ibs'	=> $ibs,
				'fis'	=> $fis, 'ugd'	=> $ugd,
				'rad'	=> $rad, 'irj'	=> $irj,
				'apt'	=> $apt, 'hemo'	=> $hemo,
				'umuk'	=> $umuk, 'bln'	=> $this->cl->arrBulan(),
				'set'	=> '',
			);
			$this->load->view('bil_irna/main_page_table_dtl', $data);	
		}elseif($format == 'pdf'){
			$data = array
			(
				'ps'	=> $ps,	 'irna'	=> $irna,
				'lab'	=> $lab, 'ibs'	=> $ibs,
				'fis'	=> $fis, 'ugd'	=> $ugd,
				'rad'	=> $rad, 'irj'	=> $irj,
				'apt'	=> $apt, 'hemo'	=> $hemo,
				'umuk'	=> $umuk, 'bln'	=> $this->cl->arrBulan(),
				'set'	=> 'pdf',
			);
			if($dt=='rkp'){
				$html = $this->load->view('bil_irna/main_page_table_rkp', $data);
			}elseif($dt=='dtl'){
				$html = $this->load->view('bil_irna/main_page_table_dtl', $data, true);
			}
			$this->pdf->render($html,'Biling_'.$regno,'Legal');
		}
	}


}

/* End of file biling.php */
/* Location: ./application/controllers/biling.php */