<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_ugd extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('biling_ugd_model','um');
		$this->load->model('master_model','mm');
		$this->load->library('cl');
	}
	public function index()
	{
		echo "<h2 style='text-align:center'>RSPAU dr. S. HARDJOLUKITO<h2>";
		die();
	}
	public function rawat()
	{
		$data = array
		(
			'title_atas' => 'RAWAT INAP',
			'title_panel'=> 'Biling Jasa UGD',
			'main_menu'	 => 'biljasa',
			'submenu1'	 => 'bilugd',
			'isi'        => 'bil_ugd/main_page',
		);
		$this->load->view('layout/wrapper', $data);
	}
	public function rawat_umum_table()
	{
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$ugd = $this->um->get_ugd($tgl_awal,$tgl_akhir);
		$data = array
		(
			'ugd'	=> $ugd,
			'tgl_awal'	=> $tgl_awal,
			'tgl_akhir'	=> $tgl_akhir,
		);
		// echo "<pre>";print_r($data);echo "</pre>";
		// die();
		$this->load->view('bil_ugd/umum_table', $data);
	}
	
}