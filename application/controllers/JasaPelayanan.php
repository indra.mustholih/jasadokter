<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// mst_groupjasaid
// 1 Klinik Spesialis
// 2 IGD
// 3 Klinik Gigi dan mulut spesialis
// 4 laboratorium
// 5 Radiologi
// 6 Fisioterapi
// 7 Hemodialisa
// 8 Farmasi

class JasaPelayanan extends CZ_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('jasaPelayanan_model','jpm');
		$this->load->library('cl');
  }
  
  public function index(){
    $jpm = $this->jpm->listing();
    
		$data = array(
			'title_atas' => 'Jasa Pelayanan',
			'title_panel'=> 'Jasa Pelayanan',
			'jpm'		 => $jpm,
			'main_menu'   	=> 'konfigurasi',
			'submenu1'   	=> 'jasapelayanan',
			'isi'		 => 'jasapelayanan/list'
		);
		$this->load->view('layout/wrapper',$data, FALSE);
  }

  public function ajax_list(){
    $list = $this->jpm->get_datatables();
    $data = array();
		$no   = $_POST['start'];
		foreach ($list as $datas){
			$no++;
			$row  	= array();
			$row[]	= $no;
			$row[]	= $datas->nama_komp;
      $row[]	= $datas->nama_jasa;
      $row[]	= $datas->keterangan;
		
			$row[] 	= anchor('JasaPelayanan/edit/'.$this->cl->enkrip($datas->komp_jasaid), '<i class="fa fa-pencil"></i> edit', array('class' => 'btn btn-warning btn-sm'))."&nbsp".
		 			 anchor('JasaPelayanan/delete/'.$this->cl->enkrip($datas->komp_jasaid), '<i class="fa fa-trash"></i> Hapus', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirmDialog();"));
			
		 	$data[] = $row;
		}

		$output 	= array(
						"draw"				=> $_POST['draw'],
						"recordsTotal"		=> $this->jpm->count_all(),
						"recordsFiltered"	=> $this->jpm->count_filtered(),
						"data"				=> $data
					);
		echo json_encode($output);
	}
	
  public function add(){

    $valid 	= $this->form_validation;

		// $valid->set_rules('mst_groupjasaid','Group Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
		// ));

		$valid->set_rules('nama_jasa','Jasa Pelayanan','required',array(
			'required'		=> '%s Harus Diisi',
		));
		$komponenbiaya = $this->jpm->showAllKompBiaya();
    if($valid->run() == FALSE){
			$data = array(
				'title_atas'	=> 'Halaman Tambah Jasa Pelayanan',
				'title_panel'	=> 'Tambah Jasa Pelayanan',
				'isi'			=> 'jasapelayanan/tambah',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'jasapelayanan',
				'komponenbiaya'			=> $komponenbiaya,
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i   	= $this->input;
			$data 	= array(
				'komp_biayaid'		=> $i->post('komp_biayaid'),
        'nama_jasa'	=> $i->post('nama_jasa'),
        'keterangan'	=> $i->post('keterangan'),
			);

			$this->jpm->add($data);
			$this->session->set_flashdata('sukses','Jasa Pelayanan Berhasil Dibuat');
			redirect(base_url('JasaPelayanan'),'refresh');
    }
    
  }

  public function edit($komp_jasaid){
    
		$komp_jasaid = $this->cl->dekrip($komp_jasaid);

		$valid 	= $this->form_validation;

		// $valid->set_rules('mst_groupjasaid','Group Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
		// ));

		$valid->set_rules('nama_jasa','Jasa Pelayanan','required',array(
			'required'		=> '%s Harus Diisi',
    ));

		$jasapelayanan 	= $this->jpm->edit($komp_jasaid);
		$komponenbiaya = $this->jpm->showAllKompBiaya();
		if($valid->run() == FALSE)
		{
			$data 	= array(
				'title_atas' 	=> 'Halaman Ubah Data Jasa Pelayanan',
        'jasapelayanan'			=> $jasapelayanan,
        'komp_jasaid'			=> $komp_jasaid,
				'title_panel'	=> 'Edit Jasa Pelayanan',
				'isi'			=> 'jasapelayanan/edit',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'jasapelayanan',
				'komponenbiaya'			=> $komponenbiaya,
				
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i 		= $this->input;
		
      $data  = array(
        'komp_biayaid' => $i->post('komp_biayaid'),
        'nama_jasa'		  => $i->post('nama_jasa'),
        'keterangan'		          => $i->post('keterangan')
      );
      $this->jpm->editproses($komp_jasaid,$data);
      $this->session->set_flashdata('sukses','Data Berhasil di Ubah');
      redirect(base_url('jasaPelayanan'),'refresh');
			
		}	
	}

	
	public function delete($komp_jasaid){
		$komp_jasaid = $this->cl->dekrip($komp_jasaid);
		$data = array('komp_jasaid' => $komp_jasaid);
		$this->jpm->delete($data);
		$this->session->set_flashdata('sukses','Data Telah Dihapus');
		redirect(base_url('jasaPelayanan'),'refresh');

	}
  

}
