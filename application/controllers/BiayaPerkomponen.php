<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// jenis jasa
// 1. medis
// 2. dokter

// TYPE
// 1. amount
// 2. persen

class BiayaPerkomponen extends CZ_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('biayaPerkomponen_model','bpk');
		$this->load->library('cl');
  }
  public function index(){
    $bpk = $this->bpk->listing();
    
		$data = array(
			'title_atas' => 'Biaya Perkomponen',
			'title_panel'=> 'Biaya Perkomponen',
			'bpk'		 => $bpk,
			'main_menu'   	=> 'konfigurasi',
			'submenu1'   	=> 'biayaperkomponen',
			'isi'		 => 'biayaperkomponen/list'
		);
		$this->load->view('layout/wrapper',$data, FALSE);
  }

  public function ajax_list(){
    $list = $this->bpk->get_datatables();
    $data = array();
		$no   = $_POST['start'];
		foreach ($list as $datas){
			$no++;
			$row  	= array();
			$row[]	= $no;
      $row[]	= $datas->nama_komp;
      $row[]	= $datas->nama_jasa;
      $row[]	= $datas->jenis_jasa;
      $row[]	= money_to_view($datas->biaya_jasa);
			$row[]	= $datas->persen;
			$row[]	= $datas->kd_tarif;
		
			$row[] 	= anchor('biayaPerkomponen/tarif/'.$this->cl->enkrip($datas->komp_perbiayaid), '<i class="fa fa-gear"></i> Tarif', array('class' => 'btn btn-info btn-sm'))."&nbsp".anchor('biayaPerkomponen/edit/'.$this->cl->enkrip($datas->komp_perbiayaid), '<i class="fa fa-pencil"></i> edit', array('class' => 'btn btn-warning btn-sm'))."&nbsp".
		 			 anchor('biayaPerkomponen/delete/'.$this->cl->enkrip($datas->komp_perbiayaid), '<i class="fa fa-trash"></i> Hapus', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirmDialog();"));
			
		 	$data[] = $row;
		}

		$output 	= array(
						"draw"				=> $_POST['draw'],
						"recordsTotal"		=> $this->bpk->count_all(),
						"recordsFiltered"	=> $this->bpk->count_filtered(),
						"data"				=> $data
					);
		echo json_encode($output);
  }


  // function tambah komponen biaya
	public function add(){
		$valid 	= $this->form_validation;

		// $valid->set_rules('mst_groupjasaid','Group Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
		// ));

		$valid->set_rules('komp_jasaid','Jasa Pelayanan','required',array(
			'required'		=> '%s Harus Diisi',
    ));

    $komponenbiaya = $this->bpk->showAllKompBiaya();
    $komponenjasa = $this->bpk->showAllKompJasa();
    if($valid->run() == FALSE){
			$data = array(
				'title_atas'	=> 'Halaman Tambah Biaya Perkomponen',
				'title_panel'	=> 'Tambah Biaya Perkomponen',
				'isi'			=> 'biayaperkomponen/tambah',
				'main_menu'   	=> 'konfigurasi',
        'submenu1'   	=> 'biayaperkomponen',
        'komponenbiaya'   	=> $komponenbiaya,
        'komponenjasa'   	=> $komponenjasa,
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i   	= $this->input;
			$data 	= array(
				'komp_biayaid'		=> $i->post('komp_biayaid'),
        'komp_jasaid'	=> $i->post('komp_jasaid'),
        'jenis_jasa'	=> $i->post('jenis_jasa'),
        'biaya_jasa'	=> $i->post('biaya_jasa'),
				'persen'	=> $i->post('persen'),
				'tipe'	=> $i->post('tipe'),
			);

			$this->bpk->add($data);
			$this->session->set_flashdata('sukses','Biaya Perkomponen Berhasil Dibuat');
			redirect(base_url('biayaPerkomponen'),'refresh');
    }
  }
  
  //function edit
  public function edit($komp_perbiayaid){
    
		$komp_perbiayaid = $this->cl->dekrip($komp_perbiayaid);

		$valid 	= $this->form_validation;

		// $valid->set_rules('mst_groupjasaid','Group Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
		// ));

		$valid->set_rules('komp_biayaid','Jasa Pelayanan','required',array(
			'required'		=> '%s Harus Diisi',
    ));

		$biayaperkomponen 	= $this->bpk->edit($komp_perbiayaid);
    $komponenbiaya = $this->bpk->showAllKompBiaya();
    $komponenjasa = $this->bpk->showAllKompJasa();
		if($valid->run() == FALSE)
		{
			$data 	= array(
				'title_atas' 	      => 'Halaman Ubah Data Biaya Perkomponen',
        'biayaperkomponen'  => $biayaperkomponen,
        'komp_perbiayaid'		=> $komp_perbiayaid,
				'title_panel'	=> 'Edit Biaya Perkomponen',
				'isi'			=> 'biayaperkomponen/edit',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'biayaperkomponen',
        'komponenbiaya'			=> $komponenbiaya,
        'komponenjasa'			=> $komponenjasa,
				
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i 		= $this->input;
		
      $data  = array(
        'komp_biayaid'  => $i->post('komp_biayaid'),
        'komp_jasaid'		  => $i->post('komp_jasaid'),
        'jenis_jasa'		=> $i->post('jenis_jasa'),
        'biaya_jasa'		=> $i->post('biaya_jasa'),
				'persen'		=> $i->post('persen'),
				'tipe'	=> $i->post('tipe')
      );

      $this->bpk->editproses($komp_perbiayaid,$data);
      $this->session->set_flashdata('sukses','Data Berhasil di Ubah');
      redirect(base_url('biayaPerkomponen'),'refresh');
			
		}	
	}
	
	public function tarif($komp_perbiayaid){
    
		$komp_perbiayaid = $this->cl->dekrip($komp_perbiayaid);

		$valid 	= $this->form_validation;

		// $valid->set_rules('mst_groupjasaid','Group Jasa Pelayanan','required',array(
		// 	'required'		=> '%s Harus Diisi',
		// ));

		$valid->set_rules('komp_biayaid','Jasa Pelayanan','required',array(
			'required'		=> '%s Harus Diisi',
    ));

		$biayaperkomponen 	= $this->bpk->edit($komp_perbiayaid);
    $komponenbiaya = $this->bpk->showAllKompBiaya();
		$komponenjasa = $this->bpk->showAllKompJasa();
		$komptarif = $this->bpk->showAllKompTarifRawatJalan();
		if($valid->run() == FALSE)
		{
			$data 	= array(
				'title_atas' 	      => 'Halaman Setting Tarif Biaya Perkomponen',
        'biayaperkomponen'  => $biayaperkomponen,
        'komp_perbiayaid'		=> $komp_perbiayaid,
				'title_panel'	=> 'Setting Tarif Biaya Perkomponen',
				'isi'			=> 'biayaperkomponen/tarif',
				'main_menu'   	=> 'konfigurasi',
				'submenu1'   	=> 'biayaperkomponen',
        'komponenbiaya'			=> $komponenbiaya,
				'komponenjasa'			=> $komponenjasa,
        'komptarif'			=> $komptarif,
				
			);
			$this->load->view('layout/wrapper',$data, FALSE);
		}else{
			$i 		= $this->input;
		
      $data  = array(
        'komp_biayaid'  => $i->post('komp_biayaid'),
        'komp_jasaid'		  => $i->post('komp_jasaid'),
        'jenis_jasa'		=> $i->post('jenis_jasa'),
        // 'biaya_jasa'		=> $i->post('biaya_jasa'),
				// 'persen'		=> $i->post('persen'),
				// 'tipe'	=> $i->post('tipe'),
				'kd_tarif' => $i->post('kd_tarif')
      );

      $this->bpk->editproses($komp_perbiayaid,$data);
      $this->session->set_flashdata('sukses','Data Berhasil di Ubah');
      redirect(base_url('biayaPerkomponen'),'refresh');
			
		}	
  }
  
  public function delete($komp_perbiayaid){
		$komp_perbiayaid = $this->cl->dekrip($komp_perbiayaid);
		$data = array('komp_perbiayaid' => $komp_perbiayaid);
		$this->bpk->delete($data);
		$this->session->set_flashdata('sukses','Data Telah Dihapus');
		redirect(base_url('biayaPerkomponen'),'refresh');

	}

}