<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_dokter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('Biling_dokter_umum_model','bdum');
		$this->load->model('Biling_dokter_bpjs_model','bdbm');
        $this->load->model('biling_lab_model','lm');
        $this->load->model('biling_hemo','bh');
        $this->load->model('Biling_rajal_model','birjm');
		$this->load->model('master_data_model','mdm');
		$this->load->model('master_model','mm');
		$this->load->library('cl');
		$this->load->library('pdf');
	}
	public function index()
	{
		echo "<h2 style='text-align:center'>RSPAU dr. S. HARDJOLUKITO<h2>";
		die();
	}
	public function rawat_umum()
	{
        $dokter = $this->mm->get_dokter();
        $kategoribayar = $this->mm->get_kategoribayar();
        $kategoripasien = $this->mm->get_ktg_pasien();
		$data = array
		(
			'title_atas' => 'Jasa Dokter',
			'title_panel'=> 'Biling Jasa Dokter Umum',
			'main_menu'	 => 'biljasa',
			'submenu1'	 => 'jsdoc',
			'isi'        => 'bil_dokter/umum/main_page',
            'dokter'	 => $dokter,
            'kategoribayar'	 => $kategoribayar,
            'kategoripasien'	 => $kategoripasien,
		);
		$this->load->view('layout/wrapper', $data);
	}
	public function rawat_umum_table()
	{
        $KdDoc = $this->input->post('KdDoc');
        $NmDoc = $this->input->post('NmDoc');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $txtSrc = $this->input->post('txtSrc');
        $get_data_irj = $this->bdum->get_data_irj('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $get_data_ugd = $this->bdum->get_data_ugd('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $get_data_irna = $this->bdum->get_data_irna('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $get_data_ok = $this->bdum->get_data_ok('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $data = array
        (
            'rajal'		=> $get_data_irj,
            'ugd'		=> $get_data_ugd,
            'irna'      => $get_data_irna,
            'opr'       => $get_data_ok,
            'NmDoc'		=> $NmDoc,
            'tgl_awal'	=> $tgl_awal,
            'tgl_akhir'	=> $tgl_akhir,
        );
        $this->load->view('bil_dokter/umum/table', $data);
	}

    public function rawat_umum_pdf($KdDoc='',$tgl_awal='',$tgl_akhir='')
    {
        $get_data_irj = $this->bdum->get_data_irj('01',$KdDoc,$tgl_awal,$tgl_akhir);
        $get_data_ugd = $this->bdum->get_data_ugd('01',$KdDoc,$tgl_awal,$tgl_akhir);
        $get_data_irna = $this->bdum->get_data_irna('01',$KdDoc,$tgl_awal,$tgl_akhir);
        $get_data_ok = $this->bdum->get_data_ok('01',$KdDoc,$tgl_awal,$tgl_akhir);
        $data = array
        (
            'rajal'		=> $get_data_irj,
            'ugd'		=> $get_data_ugd,
            'irna'      => $get_data_irna,
            'opr'       => $get_data_ok,
            'tgl_awal'	=> $tgl_awal,
            'tgl_akhir'	=> $tgl_akhir,
        );
//        echo "<pre>";print_r($data);echo "</pre>";die();
//        $this->load->view('bil_dokter/umum/table', $data, true);
        $html = $this->load->view('bil_dokter/umum/table', $data, true);
        $filename = 'Jasa Dokter '.$tgl_awal.' - '.$tgl_akhir;
        $this->pdf->render($html,$filename,'A4','landscape');
    }

    public function sisa_bayar_umum()
    {
        $data = array
        (
            'title_atas' => 'Jasa Dokter',
            'title_panel'=> 'Sisa Pembayaran Jasa Dokter',
            'main_menu'	 => 'byjasa',
            'submenu1'	 => 'sisajasadoc',
            'submenu2'   => 'umum',
            'isi'        => 'bayar_js_doc/umum/sisa_bayar',
        );
        $this->load->view('layout/wrapper', $data);
    }
    public function sisa_bayar_form()
    {
        $resp['stat']=false;
        $way = $this->input->post('way');
        $id = $this->input->post('id');
        if($way == 'POST'){
            $send = array(
                'kd_doc'    => $this->input->post('kd_doc'),
                'sisa_bayar'=> $this->input->post('sisa_bayar'),
            );
            $up = $this->mdm->up_sisa_bayar($id,$send);
            if($up){
                $resp['stat'] = true;
            }
        }elseif($way == 'PUT'){
            $dokter = $this->mm->get_dokter();
            if($id!=''){
                $edit = $this->mdm->get_sisa_bayar('id',$id);
                $data = array
                (
                    'edit'   => isset($edit[0])?$edit[0]:$edit,
                    'dokter' => $dokter,
                );
            }else{
                $data = array
                (
                    'dokter' => $dokter,
                );
            }
            $this->load->view('bayar_js_doc/umum/sisa_bayar_form', $data);
            return false;
        }elseif($way == 'DEL'){
            $del = $this->mdm->del_sisa_bayar($id);
            if($del){
                $resp['stat'] = true;
            }
        }
        echo json_encode($resp);
    }
    public function sisa_bayar_table()
    {
        $list = $this->mdm->get_sisa_bayar();
        $data = array
        (
            'list'  => $list,
        );
        $this->load->view('bayar_js_doc/umum/sisa_bayar_table', $data);
    }

    public function bayar_umum()
    {
        $dokter = $this->mm->get_dokter();
        $data = array
        (
            'title_atas' => 'Pembayaran Jasa Dokter',
            'title_panel'=> 'Bayar Jasa Dokter Umum',
            'main_menu'	 => 'byjasa',
            'submenu1'	 => 'jsdoc',
            'submenu2'	 => 'umum',
            'isi'        => 'bayar_js_doc/umum/bayar',
            'dokter'	 => $dokter,
        );
        $this->load->view('layout/wrapper', $data);
    }
    public function bayar_umum_table()
    {
        $KdDoc = $this->input->post('KdDoc');
        $NmDoc = $this->input->post('NmDoc');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $txtSrc = $this->input->post('txtSrc');
        $KDCbayar = $this->input->post('KDCbayar');
        
        $get_data_irj = $this->bdum->get_data_irj($KDCbayar, $KdDoc, $tgl_awal, $tgl_akhir, $txtSrc);
        $get_data_ugd = $this->bdum->get_data_ugd('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $get_data_irna = $this->bdum->get_data_irna('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $get_data_ok = $this->bdum->get_data_ok('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $rad = $this->bdum->get_rad('1',$KdDoc,$tgl_awal,$tgl_akhir);
        // $hd = $this->bh->get_hdtes('2','073',$tgl_awal,$tgl_akhir);
        // $lab = $this->lm->get_lab('1',$KdDoc,$tgl_awal,$tgl_akhir);
        // $lab = $this->bdum->get_lab('1',$KdDoc,$tgl_awal,$tgl_akhir);
        
        $data = array
        (
            'rajal'		=> $get_data_irj,
            'ugd'		=> $get_data_ugd,
            'irna'      => $get_data_irna,
            'opr'       => $get_data_ok,
            'rad'       => $rad,
            // 'lab'       => $lab,
            // 'hd'        => $hd,
            'NmDoc'		=> $NmDoc,
            'tgl_awal'	=> $tgl_awal,
            'tgl_akhir'	=> $tgl_akhir,
        );
        // echo "<pre>";print_r($hd);echo "</pre>";die();
        $this->load->view('bayar_js_doc/umum/bayar_table', $data);
    }

    public function hemo_umum()
    {
        $data = array
        (
            'title_atas' => 'Hemodialisa',
            'title_panel'=> 'Hemodialisa',
            'main_menu'  => 'byjasa',
            'submenu1'   => 'jsdoc',
            'submenu2'   => 'umum',
            'isi'        => 'bil_hd/main_page',
        );
        $this->load->view('layout/wrapper', $data);
    }

    public function hemo_umum_table()
    {
        $KdDoc = $this->input->post('KdDoc');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $hd = $this->bh->get_hdtes('1','',$tgl_awal,$tgl_akhir);
        // $lab = $this->lm->get_lab('1',$KdDoc,$tgl_awal,$tgl_akhir);
        // $lab = $this->bdum->get_lab('1',$KdDoc,$tgl_awal,$tgl_akhir);
        
        // echo "<pre>";print_r($hd);echo "</pre>";die();
        $data = array
        (
            'hd'       => $hd,
            'tgl_awal'  => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
        );
        $this->load->view('bil_hd/umum_table', $data);
    }

    public function rawat_bpjs()
    {
        $dokter = $this->mm->get_dokter();
        $data = array
        (
            'title_atas' => 'Jasa Dokter',
            'title_panel'=> 'Biling Jasa Dokter BPJS',
            'main_menu'	 => 'biljasa',
            'submenu1'	 => 'jsdoc',
            'isi'        => 'bil_dokter/bpjs/main_page',
            'dokter'	 => $dokter,
        );
        $this->load->view('layout/wrapper', $data);
    }
	public function rawat_bpjs_table()
	{
		$KdDoc = $this->input->post('KdDoc');
		$NmDoc = $this->input->post('NmDoc');
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$rajal = $this->bdbm->get_irj($KdDoc,$tgl_awal,$tgl_akhir);
		$visite_dpjp = $this->bdbm->get_irna($KdDoc,$tgl_awal,$tgl_akhir);
		$visite_umum = $this->bdbm->get_umum($KdDoc,$tgl_awal,$tgl_akhir);
//		$ibs = $this->bdbm->get_ibs($KdDoc,$tgl_awal,$tgl_akhir);
		$hd = $this->bdbm->get_hd($KdDoc,$tgl_awal,$tgl_akhir);
		$lab = $this->bdbm->get_lab($KdDoc,$tgl_awal,$tgl_akhir);
		$rad = $this->bdbm->get_rad($KdDoc,$tgl_awal,$tgl_akhir);
        $ugd = $this->bdbm->get_ugd($KdDoc,$tgl_awal,$tgl_akhir);
        $data = array
		(
			'rajal'		 => $rajal,
			'visite_dpjp'=> $visite_dpjp,
			'visite_umum'=> $visite_umum,
//			'ibs'		 => $ibs,
			'hd'		 => $hd,
			'lab'		 => $lab,
			'rad'		 => $rad,
			'ugd'		 => $ugd,
			'NmDoc'		 => $NmDoc,
			'tgl_awal'	 => $tgl_awal,
			'tgl_akhir'	 => $tgl_akhir,
		);
//        echo "<pre>";print_r($data);echo "</pre>";die();
        $this->load->view('bil_dokter/bpjs/bpjs_table', $data);
    }
	public function rawat_jmlain_table()
	{
		$KdDoc = $this->input->post('KdDoc');
		$NmDoc = $this->input->post('NmDoc');
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$rajal = $this->bm->get_rajal('3',$KdDoc,$tgl_awal,$tgl_akhir);
		$visite = $this->bm->get_visite('3',$KdDoc,$tgl_awal,$tgl_akhir);
		$ibs = $this->bm->get_ibs('3',$KdDoc,$tgl_awal,$tgl_akhir);
		$hd = $this->bm->get_hd('3',$KdDoc,$tgl_awal,$tgl_akhir);
		$lab = $this->bm->get_lab('3',$KdDoc,$tgl_awal,$tgl_akhir);
		$rad = $this->bm->get_rad('3',$KdDoc,$tgl_awal,$tgl_akhir);
		$ugd = $this->bm->get_ugd('3',$KdDoc,$tgl_awal,$tgl_akhir);
		// echo "<pre>";print_r($_POST);echo "</pre>";
		// die();
		$data = array
		(
			'rajal'		=> $rajal,
			'visite'	=> $visite,
			'ibs'		=> $ibs,
			'hd'		=> $hd,
			'lab'		=> $lab,
			'rad'		=> $rad,
			'ugd'		=> $ugd,
			'NmDoc'		=> $NmDoc,
			'tgl_awal'	=> $tgl_awal,
			'tgl_akhir'	=> $tgl_akhir,
		);
		$this->load->view('bil_dokter/jmlain_table', $data);
    }
    
    public function cetak_billing($format,$way,$regno){
        
        if($format == 'pdf') {

            if($way == 'rajal') 
            {
                $ps = $this->birjm->get_pasien_irj($regno);
                $irj = $this->birjm->get_irj($regno);
                $apt = $this->birjm->get_apotek($regno);
                $umuk = $this->birjm->get_umuk($regno);

                $data = array
                (
                    'ps'	=> $ps,  'irj'	=> $irj,
                    'umuk'  => $umuk, 'apt' => $apt, 
                    'bln'	=> $this->cl->arrBulan(),
                    'set'	=> 'pdf',
                    'way'	=> $way,
                );
                // echo "<pre>"; print_r($data); exit();
                $html = $this->load->view('pdf/rajal/bill-rajal', $data, true);
                $this->pdf->render($html,'Biling_'.$regno,'Legal');
            }
        }
    }
    public function honor_dokter(){
        $dokter = $this->mm->get_dokter();
        
        $data = array(
            'title_atas' => 'Jasa Dokter',
			'title_panel'=> 'Honor Dokter',
			'main_menu'	 => 'biljasa',
			'submenu1'	 => 'jsdoc',
			'isi'        => 'bil_dokter/honor_dokter/main_page',
			'dokter'	 => $dokter,
        );
        $this->load->view('layout/wrapper', $data);
    }

    public function honor_dokter_table()
    {
        $KdDoc = $this->input->post('KdDoc');
        $NmDoc = $this->input->post('NmDoc');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $txtSrc = $this->input->post('txtSrc');
        $get_data_irj = $this->bdum->get_data_irj('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $get_data_ugd = $this->bdum->get_data_ugd('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $get_data_irna = $this->bdum->get_data_irna('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $get_data_ok = $this->bdum->get_data_ok('01',$KdDoc,$tgl_awal,$tgl_akhir,$txtSrc);
        $rad = $this->bdum->get_rad('1',$KdDoc,$tgl_awal,$tgl_akhir);
        // $hd = $this->bh->get_hdtes('2','073',$tgl_awal,$tgl_akhir);
        // $lab = $this->lm->get_lab('1',$KdDoc,$tgl_awal,$tgl_akhir);
        // $lab = $this->bdum->get_lab('1',$KdDoc,$tgl_awal,$tgl_akhir);
        
        $data = array
        (
            'rajal'		=> $get_data_irj,
            'ugd'		=> $get_data_ugd,
            'irna'      => $get_data_irna,
            'opr'       => $get_data_ok,
            'rad'       => $rad,
            // 'lab'       => $lab,
            // 'hd'        => $hd,
            'NmDoc'		=> $NmDoc,
            'tgl_awal'	=> $tgl_awal,
            'tgl_akhir'	=> $tgl_akhir,
        );
        // echo "<pre>";print_r($hd);echo "</pre>";die();
        $this->load->view('bil_dokter/honor_dokter/umum_table', $data);
    }
    public function honor_dokter_pdf($KdDoc='',$tgl_awal='',$tgl_akhir='')
    {
        $get_data_irj = $this->bdum->get_data_irj('01',$KdDoc,$tgl_awal,$tgl_akhir);
        $get_data_ugd = $this->bdum->get_data_ugd('01',$KdDoc,$tgl_awal,$tgl_akhir);
        $get_data_irna = $this->bdum->get_data_irna('01',$KdDoc,$tgl_awal,$tgl_akhir);
        $get_data_ok = $this->bdum->get_data_ok('01',$KdDoc,$tgl_awal,$tgl_akhir);
        $data = array
        (
            'rajal'		=> $get_data_irj,
            'ugd'		=> $get_data_ugd,
            'irna'      => $get_data_irna,
            'opr'       => $get_data_ok,
            'tgl_awal'	=> $tgl_awal,
            'tgl_akhir'	=> $tgl_akhir,
        );
//        echo "<pre>";print_r($data);echo "</pre>";die();
//        $this->load->view('bil_dokter/umum/table', $data, true);
        $html = $this->load->view('bil_dokter/honor_dokter/table', $data, true);
        $filename = 'Jasa Dokter '.$tgl_awal.' - '.$tgl_akhir;
        $this->pdf->render($html,$filename,'A4','landscape');
    }
    
  
}