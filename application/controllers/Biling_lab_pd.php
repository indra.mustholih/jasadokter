<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling_lab_pd extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('biling_lab_model','lm');
		$this->load->model('master_model','mm');
		$this->load->library('cl');
	}
	public function index()
	{
		echo "<h2 style='text-align:center'>RSPAU dr. S. HARDJOLUKITO<h2>";
		die();
	}
	public function rawat()
	{
		$data = array
		(
			'title_atas' => 'RAWAT INAP',
			'title_panel'=> 'Biling Jasa Lab Untuk Perawat dan Dokter',
			'main_menu'	 => 'biljasa',
			'submenu1'	 => 'billabpd',
			'isi'        => 'bil_lab_pd/main_page',
		);
		$this->load->view('layout/wrapper', $data);
	}
	public function rawat_umum_table()
	{
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$lab = $this->lm->get_lab('1',$tgl_awal,$tgl_akhir);
		$data = array
		(
			'lab'	=> $lab,
			'tgl_awal'	=> $tgl_awal,
			'tgl_akhir'	=> $tgl_akhir,
		);
		// echo "<pre>";print_r($data);echo "</pre>";
		// die();
		$this->load->view('bil_lab_pd/umum_table', $data);
	}
	
}