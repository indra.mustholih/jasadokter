<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		// Untuk load model user
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('user_model');
	}

	public function index()
	{
		$valid = $this->form_validation;

		$valid->set_rules('username','Username','required',array(
						'required'		=> '%s harus Diisi'));

		$valid->set_rules('password','Password','required',array(
						'required'		=> '%s harus Diisi'));

		if($valid->run() == FALSE){
			// ini digunakan untuk, jika webapp sudah punya session dia tidak akan bisa balik ke halaman login

			if($this->session->userdata('username') != "")
			{
				redirect(base_url(),'refresh');
			}
			else
			{
				$data = array (
					'title_atas'		=> 'Halaman Login',
					'title_panel'		=> 'Halaman Login'
					);
				$this->load->view('login/login_layout', $data, FALSE);
			}
		}else{
			$i 				= $this->input;
			$username 		= $i->post('username');
			$password 		= $i->post('password');

			// CHECK DI DATABASE MENGGUNAKAN FUNCTION CHECK PADA USER MODEL
			$check_login  	= $this->user_model->check_login($username);
			// Yang pertama dia mengecek ke dalam function di dalam model apakah username sama maka di itung lebih dari 0 dan lanjut memproses.
			if(count($check_login) > 0){
				// dapatkan hash password pada database
				$hash = $check_login['password'];
				// jika password hash ada dan terverify pada function password_verify maka diproses kode selanjutnya
				if(password_verify($password,$hash)){
					
					$userdata 	= array(
						'username'		=> $username,
						'akses_level'	=> $check_login['akses_level'],
						'id_user'		=> $check_login['id_user'],
					);
					$this->session->set_userdata($userdata);
					$this->session->set_flashdata('sukses','<i class="fa fa-check"></i> Selamat Datang !! '. $username);
					redirect(base_url(),'refresh');
				}else{
					// RECORD USERNAME BENAR TAPI PASSWORD SALAH KEMBALI KE HALAMAN LOGIN
					$this->session->set_flashdata('gagal','<i class="fa fa-warning"></i> Username atau Password tidak cocok');
					redirect(base_url('login'),'refresh');
				}
				// RECORD 22NYA SALAH KEMBALI KE HALAMAN LOGIN
			}else{
				$this->session->set_flashdata('gagal','<i class="fa fa-warning"></i> Username atau Password tidak cocok');
				redirect(base_url('login'),'refresh');
			}
		}	
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/admin/Login.php */