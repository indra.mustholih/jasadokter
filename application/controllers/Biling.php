<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biling extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('akses_level')) redirect('/');
		$this->load->model('biling_model','bm');
		$this->load->library('cl');
	}
	public function index()
	{
		echo "There is Nothing!";
	}
	public function biling_irna()
	{
		$data = array
		(
			'title_atas' => 'RAWAT INAP',
			'title_panel'=> 'Biling Transaksi Rawat Jalan',
			'isi'        => 'bil_irna/main_page'
		);
		$this->load->view('layout/wrapper', $data);
	}
	public function biling_irna_table()
	{
		$regno = $this->input->post('regno');
		$format = $this->input->post('format');
		$ps = $this->bm->get_pasien($regno);
		$apt = $this->bm->get_apotek($regno);
		$ibs = $this->bm->get_ibs($regno);
		$fis = $this->bm->get_fis($regno);
		$hd = $this->bm->get_hd($regno);
		$irj = $this->bm->get_irj($regno);
		$irna = $this->bm->get_irna($regno);
		$lab = $this->bm->get_lab($regno);
		$rad = $this->bm->get_rad($regno);
		$ugd = $this->bm->get_ugd($regno);
		$data = array
		(
			'ps'	=> $ps,
			'apt'	=> $apt,
			'ibs'	=> $ibs,
			'lab'	=> $lab,
			'rad'	=> $rad,
			'hd'	=> $hd,
			'ugd'	=> $ugd,
			'irj'	=> $irj,
			'fis'	=> $fis,
			'irna'	=> $irna,
		);
		if ($format == 'rkp') {
			$this->load->view('bil_irna/main_page_table_rkp', $data);	
		}elseif($format == 'dtl'){
			$this->load->view('bil_irna/main_page_table_dtl', $data);	
		}
	}


}

/* End of file biling.php */
/* Location: ./application/controllers/biling.php */