<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'beranda';
$route['404_override'] = 'error';
$route['translate_uri_dashes'] = FALSE;

$route['User-Login'] = 'login/index';
$route['User-Logout'] = 'beranda/logout';

$route['Master-Data/Tarif-Pelayanan-BPJS-Irna'] = 'Master_data/tarif_bpjs_irna';



$route['Administratif/Konfigurasi'] = 'admin/konfigurasi';
$route['Administratif/Data-User'] = 'admin/user';
$route['Administratif/Data-User/Tambah'] = 'admin/user/tambah';
$route['Administratif/Data-User/(:any)'] = 'admin/user/edit/$1';
$route['Administratif/Data-User/Hapus/(:any)'] = 'admin/user/delete/$1';
$route['Administratif/Data-User/Ubah-Password/(:any)'] = 'admin/user/ubah_password/$1';

$route['Biling/Rawat-Inap'] = 'biling_ranap/biling_irna';

$route['Biling/Jasa-Dokter/Umum'] = 'biling_dokter/rawat_umum';
$route['Biling/Jasa-Dokter/Umum/PDF/(:any)/(:any)/(:any)'] = 'biling_dokter/rawat_umum_pdf/$1/$2/$3';

$route['Biling/Sisa-Bayar/Jasa-Dokter/Umum'] = 'biling_dokter/sisa_bayar_umum';

$route['Biling/Bayar/Jasa-Dokter/Umum'] = 'biling_dokter/bayar_umum';

$route['Biling/Jasa-Dokter/BPJS'] = 'biling_dokter/rawat_bpjs';

$route['Biling/Jasa-Perawat'] = 'biling_perawat/rawat';
$route['Biling/Jasa-Lab-Perawat-dan-Dokter'] = 'biling_lab_pd/rawat';
$route['Biling/Jasa-UGD'] = 'biling_ugd/rawat';
$route['Biling/Jasa-OK'] = 'biling_ok/rawat';
$route['Biling/Jasa-Radiologi-Perawat-dan-Dokter'] = 'biling_rad_pd/rawat';